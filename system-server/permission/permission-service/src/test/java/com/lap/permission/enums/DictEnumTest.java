package com.lap.permission.enums;

import com.lap.framework.tools.JsonUtil;
import com.lap.permission.dto.entity.DictDetail;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

@Slf4j
@DisplayName("数字字典单元测试")
class DictEnumTest {

    @DisplayName("备注拆分单元测试")
    @ParameterizedTest
    @CsvSource({"'：0-test;1-aa，3-bbb；4-ccc，5-啊啊啊'"})
//"'菜单类型:0-模块,1-菜单,2-功能'",
    void getDictList(String remark) {
        List<DictDetail> list = DictEnum.getDictList(remark);
        Assertions.assertNotNull(list);
        log.info("{}", JsonUtil.prettyPrint(list));
    }

}