package com.lap.permission.api;

import com.lap.junit.entity.ArgEntity;
import com.lap.junit.entity.ArgMap;
import com.lap.junit.tools.ArgUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("登录日志")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LoginLogControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("登录日志新增")
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = {"/api/login-logs/saveLoginLog.csv"}, numLinesToSkip = 1)
    void testSaveLoginLog(@AggregateWith(SaveLoginLog.class) ArgMap map) throws Exception {
        mockMvc.perform(post("/api/login-logs").content(map.getJson()).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(map.getInteger("resultCode")));// just
    }

    static class SaveLoginLog implements ArgumentsAggregator {

        @Override
        public ArgMap aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
            ArgEntity[] _entityList = new ArgEntity[11];
            _entityList[0] = new ArgEntity("id", Integer.class);
            _entityList[1] = new ArgEntity("userId", Integer.class);
            _entityList[2] = new ArgEntity("username", String.class);
            _entityList[3] = new ArgEntity("fullName", String.class);
            _entityList[4] = new ArgEntity("browser", String.class);
            _entityList[5] = new ArgEntity("sysInfo", String.class);
            _entityList[6] = new ArgEntity("loginIp", Long.class);
            _entityList[7] = new ArgEntity("loginAddress", String.class);
            _entityList[8] = new ArgEntity("successFlag", Integer.class);
            _entityList[9] = new ArgEntity("remark", String.class);
            _entityList[10] = new ArgEntity("resultCode", Integer.class);
            return ArgUtil.arg2Map(accessor, _entityList);
        }

    }

    @DisplayName("登录日志列表")
    @Order(2)
    @ParameterizedTest
    @CsvSource(value = {", , , "})
    void testQueryPage(String username, String fullName, String successFlag, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/login-logs").param("username", username).param("fullName", fullName).param("successFlag", successFlag)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("登录日志删除")
    @Order(3)
    @ParameterizedTest
    @CsvSource(value = {", "})
    void testDeleteById(Integer loginLogId, Integer resultCode) throws Exception {
        mockMvc.perform(delete("/api/login-logs/{loginLogId}", loginLogId))
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
