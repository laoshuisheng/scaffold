package com.lap.permission.api;

import com.lap.framework.tools.JsonUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("列表头")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class HeaderControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("列表头保存")
    @Order(1)
    @ParameterizedTest
    @CsvSource(value = {", , 450","test,'code,id,createTime',200"})
    void testSaveHeader(String code, String content, Integer resultCode) throws Exception {
        Map<String, Object> _paraMap = new HashMap<>(2);
        _paraMap.put("code", code);
        _paraMap.put("content", content);
        String _content = JsonUtil.toJson(_paraMap);

        mockMvc.perform(put("/api/headers").content(_content).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("根据编码获取列表头")
    @Order(2)
    @ParameterizedTest
    @CsvSource(value = {", 450","test,200"})
    void testQueryByCode(String code, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/headers").param("code", code)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
