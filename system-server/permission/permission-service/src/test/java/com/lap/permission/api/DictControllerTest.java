package com.lap.permission.api;

import com.lap.junit.entity.ArgEntity;
import com.lap.junit.entity.ArgMap;
import com.lap.junit.tools.ArgUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("数字字典")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DictControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("数字字典更新")
    @Order(2)
    @ParameterizedTest
    @CsvFileSource(resources = {"/api/dicts/updateDict.csv"}, numLinesToSkip = 1)
    void testUpdateDict(@AggregateWith(UpdateDict.class) ArgMap map) throws Exception {
        mockMvc.perform(put("/api/dicts").content(map.getJson()).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(map.getInteger("resultCode")));// just
    }

    static class UpdateDict implements ArgumentsAggregator {

        @Override
        public ArgMap aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
            ArgEntity[] _entityList = new ArgEntity[6];
            _entityList[0] = new ArgEntity("id", Integer.class);
            _entityList[1] = new ArgEntity("moduleFlag", Integer.class);
            _entityList[2] = new ArgEntity("code", String.class);
            _entityList[3] = new ArgEntity("name", String.class);
            _entityList[4] = new ArgEntity("remark", String.class);
            _entityList[5] = new ArgEntity("resultCode", Integer.class);
            return ArgUtil.arg2Map(accessor, _entityList);
        }

    }

    @DisplayName("数字字典新增")
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = {"/api/dicts/saveDict.csv"}, numLinesToSkip = 1)
    void testSaveDict(@AggregateWith(SaveDict.class) ArgMap map) throws Exception {
        mockMvc.perform(post("/api/dicts").content(map.getJson()).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(map.getInteger("resultCode")));// just
    }

    static class SaveDict implements ArgumentsAggregator {

        @Override
        public ArgMap aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
            ArgEntity[] _entityList = new ArgEntity[6];
            _entityList[0] = new ArgEntity("id", Integer.class);
            _entityList[1] = new ArgEntity("moduleFlag", Integer.class);
            _entityList[2] = new ArgEntity("code", String.class);
            _entityList[3] = new ArgEntity("name", String.class);
            _entityList[4] = new ArgEntity("remark", String.class);
            _entityList[5] = new ArgEntity("resultCode", Integer.class);
            return ArgUtil.arg2Map(accessor, _entityList);
        }

    }

    @DisplayName("数字字典列表")
    @Order(3)
    @ParameterizedTest
    @CsvSource(value = {", , , 200", "10,dict.name,测试,200", ",,test, 200"})
    void testQueryPage(String moduleFlag, String code, String name, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/dicts").param("moduleFlag", moduleFlag).param("code", code).param("name", name)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("数字字典删除")
    @Order(4)
    @ParameterizedTest
    @CsvSource(value = {"10, 200", "1,200"})
    void testDeleteById(Integer dictId, Integer resultCode) throws Exception {
        mockMvc.perform(delete("/api/dicts/{dictId}", dictId))
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
