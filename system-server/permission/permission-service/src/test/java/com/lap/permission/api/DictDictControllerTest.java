package com.lap.permission.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.lap.framework.tools.JsonUtil;
import com.lap.junit.tools.StringConverter;
import com.lap.permission.dto.request.DictDetailRequest;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("数字字典明细")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DictDictControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("数字字典明细新增")
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "/api/dict/details/saveDictDetail.csv", numLinesToSkip = 1)
    void testSaveDictDetail(String dictId, String dictCode, String dataList, Integer resultCode) throws Exception {
        Map<String, Object> _paraMap = new HashMap<>(3);
        _paraMap.put("dictId", dictId);
        _paraMap.put("dictCode", dictCode);
        _paraMap.put("dataList", parserList(dataList));
        String _content = JsonUtil.toJson(_paraMap);

        mockMvc.perform(post("/api/dict-details").content(_content).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    /**
     * @param parameter 入参
     * @return 集合
     */
    private List<DictDetailRequest.DetailRequest> parserList(String parameter) {
        if (StringUtils.isBlank(parameter)) {
            return null;
        }
        return JsonUtil.toObject(parameter, new TypeReference<>() {
        });
    }

    @DisplayName("根据ID查询数字字典明细")
    @Order(2)
    @ParameterizedTest
    @CsvSource(value = {", 500", "1,200"})
    void testQueryByDictId(Integer dictId, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/dict-details/{dictId}", dictId)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("根据编码数字字典明细")
    @Order(3)
    @ParameterizedTest
    @CsvSource(value = {"'[dict.permission.typeFlag,dict.commons.yesNo]',200"})
    void testQueryByDictCodes(@ConvertWith(StringConverter.class) List<String> dataList, Integer resultCode) throws Exception {
        String content = JsonUtil.toJson(dataList);
        mockMvc.perform(post("/api/dict-details/list").content(content).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
