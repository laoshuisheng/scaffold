package com.lap.permission.component;

import com.lap.framework.tools.JsonUtil;
import com.lap.framework.tools.Uid;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.crypto.SecretKey;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Slf4j
@SpringBootTest
class JwtTokenHelperTest {

    @Resource
    private JwtTokenHelper jwtTokenHelper;

//    @Test
//    void generateToken() {
//        User user = new User();
//        user.setId(1);
//        user.setUsername("laoshuisheng");
//        user.setFullName("劳水生");
//        String token = jwtTokenHelper.generateToken(user);
//        Assertions.assertNotNull(token);
//        log.info("{}", token);
//    }
//
//    @Test
//    void testGenerateToken() {
//        String token = "peyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJsYW9zaHVpc2hlbmciLCJmdWxsTmFtZSI6IuWKs-awtOeUnyIsInN1YiI6Imxhb3NodWlzaGVuZyIsImlhdCI6MTcyMDA1NTE1OCwiZXhwIjoxNzIwMDU4NzU4fQ.UwVx9OrJvthYe4THuBt4-4m271KhfLAd0RV6NPDtS1I";
//        boolean flag = jwtTokenHelper.isTokenValid(token, "laoshuisheng");
//        Assertions.assertTrue(flag);
//        log.info("{}", flag);
//    }

    static SecretKey key;

    @BeforeAll
    static void init() {
        byte[] keyBytes = Decoders.BASE64.decode("3cfa76ef14937c1c0ea519f8fc057a80fcd04a7420f8e8bcd0a7567c272e007b");
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    @Test
    void testJwt() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 10);

        String token = Jwts.builder().id(Uid.getId())
                .issuer("me")//签发人
                .subject("sub")//主题
                .claims(Map.of("id", 1, "fullName", "test", "username", "Laoshuisheng"))
                .issuedAt(new Date())//签发时间
                .expiration(calendar.getTime())//过期时间
                .signWith(key).compact();
        Assertions.assertNotNull(token);
        log.info("{}", token);

        Claims claims = Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token)
                .getPayload();
        log.info("{}", JsonUtil.prettyPrint(claims));
    }

    @Test
    void parserToken() {
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJqdGkiOiI3MDAyZWRiNzk3NjM0OTFiODc3NDc0ZTYwNGMwY2M4MyIsImlzcyI6Im1lIiwic3ViIjoic3ViIiwiaWQiOjEsImZ1bGxOYW1lIjoidGVzdCIsInVzZXJuYW1lIjoibGFvc2h1aXNoZW5nIiwiaWF0IjoxNzIwMDcyMDEyLCJleHAiOjE3MjAwNzIwMjJ9.igJjp6AiE2DZN4VtXXjxT2chB0IsUzX1n_rfiqGh2SIUbR2DyVBhe7IkLst1h5zr";
        try {
            Claims claims = Jwts.parser().verifyWith(key).build().parseSignedClaims(token).getPayload();
            log.info("{}", JsonUtil.prettyPrint(claims));
        } catch (Exception e) {
            throwException(e);
        }
    }

    void throwException(Exception e) {
        if (e instanceof ExpiredJwtException) {
            log.info("Token 过期");
        } else if (e instanceof UnsupportedJwtException
                || e instanceof JwtException
                || e instanceof MalformedJwtException) {
            log.info("jwt 非法");
        }
    }

    @Test
    void isTokenValid() {
    }

}