package com.lap.permission.test.file;

import com.lap.analysis.processor.FileProcessor;
import com.lap.analysis.source.entity.FileInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

@DisplayName("单元测试文件生成")
@Slf4j
class FileProcessorTest {

    @DisplayName("查询单元测试")
    @Test
    void testQueryFile() {
        FileProcessor processor = new FileProcessor();
        List<FileInfo> fileList = processor.getFiles("config.xml");
        Assertions.assertNotNull(fileList);
        log.info("{}", fileList);
    }

    @DisplayName("执行单元测试")
    //@Disabled
    @Test
    void testExecute() {
        FileProcessor processor = new FileProcessor();
        processor.execute("config.xml");
        Assertions.assertNotNull(processor);
    }

}