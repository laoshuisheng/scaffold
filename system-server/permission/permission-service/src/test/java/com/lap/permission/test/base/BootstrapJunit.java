package com.lap.permission.test.base;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalancerAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaDiscoveryClientConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@ActiveProfiles("junit")
@SpringBootApplication(scanBasePackages = {"com.lap.permission"},
        exclude = {EurekaClientAutoConfiguration.class,
                EurekaDiscoveryClientConfiguration.class,
                LoadBalancerAutoConfiguration.class})
@MapperScan("com.lap.permission.dal")
@EnableTransactionManagement
public class BootstrapJunit {
    //not thing
    static {
        log.info("junit test start bootstrap..");
    }

}