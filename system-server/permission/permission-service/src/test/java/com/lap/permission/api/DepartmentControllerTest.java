package com.lap.permission.api;

import com.lap.junit.entity.ArgEntity;
import com.lap.junit.entity.ArgMap;
import com.lap.junit.tools.ArgUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("部门")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepartmentControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("部门更新")
    @Order(2)
    @ParameterizedTest
    @CsvFileSource(resources = {"/api/departments/updateDepartment.csv"}, numLinesToSkip = 1)
    void testUpdateDepartment(@AggregateWith(UpdateDepartment.class) ArgMap map) throws Exception {
        mockMvc.perform(put("/api/departments").content(map.getJson()).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(map.getInteger("resultCode")));// just
    }

    static class UpdateDepartment implements ArgumentsAggregator {

        @Override
        public ArgMap aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
            ArgEntity[] _entityList = new ArgEntity[8];
            _entityList[0] = new ArgEntity("id", Integer.class);
            _entityList[1] = new ArgEntity("parentId", Integer.class);
            _entityList[2] = new ArgEntity("code", String.class);
            _entityList[3] = new ArgEntity("name", String.class);
            _entityList[4] = new ArgEntity("statusFlag", Integer.class);
            _entityList[5] = new ArgEntity("remark", String.class);
            _entityList[6] = new ArgEntity("orders", Integer.class);
            _entityList[7] = new ArgEntity("resultCode", Integer.class);
            return ArgUtil.arg2Map(accessor, _entityList);
        }

    }

    @DisplayName("部门新增")
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = {"/api/departments/saveDepartment.csv"}, numLinesToSkip = 1)
    void testSaveDepartment(@AggregateWith(SaveDepartment.class) ArgMap map) throws Exception {
        mockMvc.perform(post("/api/departments").content(map.getJson()).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(map.getInteger("resultCode")));// just
    }

    static class SaveDepartment implements ArgumentsAggregator {

        @Override
        public ArgMap aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
            ArgEntity[] _entityList = new ArgEntity[8];
            _entityList[0] = new ArgEntity("id", Integer.class);
            _entityList[1] = new ArgEntity("parentId", Integer.class);
            _entityList[2] = new ArgEntity("code", String.class);
            _entityList[3] = new ArgEntity("name", String.class);
            _entityList[4] = new ArgEntity("statusFlag", Integer.class);
            _entityList[5] = new ArgEntity("remark", String.class);
            _entityList[6] = new ArgEntity("orders", Integer.class);
            _entityList[7] = new ArgEntity("resultCode", Integer.class);
            return ArgUtil.arg2Map(accessor, _entityList);
        }

    }

    @DisplayName("部门列表")
    @Order(3)
    @ParameterizedTest
    @CsvSource(value = {", , , 200", ",,1,200", ",test,1,200", "test,test,1,200"})
    void testQueryPage(String code, String name, String statusFlag, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/departments").param("code", code).param("name", name).param("statusFlag", statusFlag)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("部门删除")
    @Order(4)
    @ParameterizedTest
    @CsvSource(value = {"1, 200", "100, 451"})
    void testDeleteById(Integer departmentId, Integer resultCode) throws Exception {
        mockMvc.perform(delete("/api/departments/{departmentId}", departmentId))
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
