package com.lap.permission.test.base;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * 单元测试抽象类
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@ActiveProfiles("junit")
@Slf4j
@SpringBootTest(classes = {BootstrapJunit.class})
public abstract class AbstractBaseTest {

    @Resource
    private WebApplicationContext context;

    protected MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        log.info("Initializer mockMvc..");
    }

}