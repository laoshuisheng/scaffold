package com.lap.permission.api;

import com.lap.framework.tools.JsonUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("权限访问")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthenticationControllerTest extends com.lap.permission.test.base.AbstractBaseTest {

    @DisplayName("验证")
    @Order(2)
    @ParameterizedTest
    @CsvFileSource(resources = "/api/authentication/validate.csv", numLinesToSkip = 1)
    void testValidate(String token, Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/auth/validate").param("token", token)) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("登出")
    @Order(3)
    @ParameterizedTest
    @CsvSource(value = {"200"})
    void testLogout(Integer resultCode) throws Exception {
        mockMvc.perform(get("/api/auth/signOut")) //perform
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

    @DisplayName("登录")
    @Order(1)
    @ParameterizedTest
    @CsvSource(value = {", , 450", "admin,test,451", "admin,123456,200"})
    void testLogin(String username, String password, Integer resultCode) throws Exception {
        Map<String, Object> _paraMap = new HashMap<>(2);
        _paraMap.put("username", username);
        _paraMap.put("password", password);
        String _content = JsonUtil.toJson(_paraMap);

        mockMvc.perform(post("/api/auth/signIn").content(_content).contentType(MediaType.APPLICATION_JSON))// api
                .andExpect(status().isOk()).andDo(print())// status
                .andExpect(jsonPath("$.code").value(resultCode));// just
    }

}
