INSERT INTO sys_dict (id, module_flag, code, name, remark, created_by, create_time, updated_by, update_time)
VALUES (30, 0, 'dict.permission.typeFlag', '菜单类型', '菜单类型:0-模块,1-菜单,2-功能', 'admin', '2021-05-01 21:46:06',
        '系统管理员', '2023-03-14 20:37:22'),
       (36, 0, 'dict.permission.module.typeFlag', '所属模块', '所属模块:0-通用模块,1-权限模块,2-客户管理', 'admin',
        '2021-05-01 22:47:18', '系统管理员', '2023-03-14 20:30:53'),
       (37, 0, 'dict.permission.yesNo', '通用是否', '通用是否:0-否,1-是', 'admin', '2021-05-03 20:37:30', '系统管理员',
        '2023-03-05 22:26:31'),
       (38, 0, 'dict.permission.sexFlag', '通用性别', '通用性别:0-保密,1-男,2-女', 'admin', '2021-05-03 20:38:42',
        '系统管理员', '2023-04-12 21:17:43'),
       (39, 0, 'dict.permission.statusFlag', '通用状态', '通用状态:0-禁用,1-启用', 'admin', '2021-05-03 20:41:02',
        '系统管理员', '2023-03-05 22:26:34'),
       (41, 1, 'dict.permission.successFlag', '登录状态', '登录状态:0-失败,1-成功', 'admin', '2021-09-29 22:12:56',
        '系统管理员', '2023-04-12 21:17:40');

INSERT INTO sys_dict_detail (id, dict_id, dict_code, detail_value, detail_title, status_flag, local, orders)
VALUES (20, 30, 'dict.permission.typeFlag', 0, '模块', 1, 'zh_CN', 0),
       (21, 30, 'dict.permission.typeFlag', 1, '菜单', 1, 'zh_CN', 1),
       (22, 30, 'dict.permission.typeFlag', 2, '功能', 1, 'zh_CN', 2),
       (35, 36, 'dict.permission.module.typeFlag', 0, '通用模块', 1, 'zh_CN', 1),
       (37, 37, 'dict.permission.yesNo', 0, '否', 1, 'zh_CN', 0),
       (38, 37, 'dict.permission.yesNo', 1, '是', 1, 'zh_CN', 1),
       (39, 38, 'dict.permission.sexFlag', 0, '保密', 1, 'zh_CN', 0),
       (40, 38, 'dict.permission.sexFlag', 2, '女', 1, 'zh_CN', 2),
       (41, 39, 'dict.permission.statusFlag', 0, '禁用', 1, 'zh_CN', 0),
       (42, 39, 'dict.permission.statusFlag', 1, '启用', 1, 'zh_CN', 1),
       (43, 38, 'dict.permission.sexFlag', 1, '男', 1, 'zh_CN', 1),
       (45, 36, 'dict.permission.module.typeFlag', 1, '权限模块', 1, 'zh_CN', 2),
       (46, 36, 'dict.permission.module.typeFlag', 2, '客户管理', 1, 'zh_CN', 3),
       (47, 41, 'dict.permission.successFlag', 0, '失败', 1, 'zh_CN', 0),
       (48, 41, 'dict.permission.successFlag', 1, '成功', 1, 'zh_CN', 1);

INSERT INTO `sys_user` (`username`, `password`, `full_name`, `email`, `department_id`, `department_id_path`,
                        `position_id`, `status_flag`, `status_remark`, `term_validity`, `type_flag`, `created_by`,
                        `create_time`, `updated_by`, `update_time`)
VALUES ('admin', '123456', '系统管理员', 'laoshuisheng@163.com', 20, '10,20', 1, 1, '启动管理员', '2024-09-20 23:59:59',
        1, 'admin', '2020-03-20 23:13:41', '系统管理员', '2024-07-04 20:52:58');
