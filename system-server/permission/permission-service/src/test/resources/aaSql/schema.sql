DROP TABLE IF EXISTS sys_access_log;

CREATE TABLE sys_access_log
(
    id             int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    user_id        int         NOT NULL COMMENT '管理员ID',
    username       varchar(32) NOT NULL COMMENT '登录账号',
    full_name      varchar(16)          DEFAULT NULL COMMENT '姓名',
    access_uri     varchar(256)         DEFAULT NULL COMMENT '访问地址',
    access_method  varchar(16)          DEFAULT NULL COMMENT '访问方法',
    device_info    varchar(256)         DEFAULT NULL COMMENT '访问浏览器',
    sys_info       varchar(256)         DEFAULT NULL COMMENT '访问系统',
    access_sign    varchar(256)         DEFAULT NULL COMMENT '访问来源地址',
    access_address varchar(128)         DEFAULT NULL COMMENT '访问来源地点',
    access_param   varchar(2048)        DEFAULT NULL COMMENT '访问入参',
    access_result  text COMMENT '访问返回值',
    times          bigint               DEFAULT '0' COMMENT '访问耗时',
    create_time    timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '访问时间',
    PRIMARY KEY (id)
) COMMENT='访问日志';

DROP TABLE IF EXISTS sys_department;

CREATE TABLE sys_department
(
    id          int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    parent_id   int         NOT NULL COMMENT '上级部门ID',
    code        varchar(64)          DEFAULT NULL COMMENT '编码',
    name        varchar(32) NOT NULL COMMENT '名称',
    status_flag tinyint     NOT NULL COMMENT '状态:0-禁用,1-启用',
    leaf        tinyint              DEFAULT NULL COMMENT '是否叶子:0-否,1-是',
    remark      varchar(64)          DEFAULT NULL COMMENT '备注',
    orders      int         NOT NULL DEFAULT '0' COMMENT '排序',
    created_by  varchar(32) NOT NULL COMMENT '创建人',
    create_time timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by  varchar(32) NOT NULL COMMENT '修改人',
    update_time timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id, parent_id)
) COMMENT='部门';

DROP TABLE IF EXISTS sys_dict;

CREATE TABLE sys_dict
(
    id          int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    module_flag int                  DEFAULT '0' COMMENT '模块标识',
    code        varchar(64) NOT NULL COMMENT '编码',
    name        varchar(32)          DEFAULT NULL COMMENT '字典名称',
    remark      varchar(512)         DEFAULT NULL COMMENT '备注',
    created_by  varchar(32) NOT NULL COMMENT '创建人',
    create_time datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by  varchar(32) NOT NULL COMMENT '修改人',
    update_time datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='数字字典';

DROP TABLE IF EXISTS sys_dict_detail;

CREATE TABLE sys_dict_detail
(
    id           int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    dict_id      int         NOT NULL COMMENT '字典ID',
    dict_code    varchar(64) NOT NULL COMMENT '字典编码',
    detail_value int         NOT NULL COMMENT '明细值',
    detail_title varchar(32) NOT NULL COMMENT '明细标题',
    status_flag  tinyint     NOT NULL DEFAULT '1' COMMENT '状态',
    local        varchar(8)  NOT NULL DEFAULT 'zh_CN' COMMENT '语言',
    orders       tinyint     NOT NULL DEFAULT '1' COMMENT '排序',
    PRIMARY KEY (id)
) COMMENT='数字字典明细';

DROP TABLE IF EXISTS sys_email_template;

CREATE TABLE sys_email_template
(
    id          int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    module_flag tinyint      NOT NULL DEFAULT '0' COMMENT '模板类型,使用数字字典',
    code        varchar(128) NOT NULL COMMENT '编码,不能修改',
    name        varchar(64)  NOT NULL COMMENT '名称',
    subject     varchar(256) NOT NULL COMMENT '邮件主题',
    content     longtext     NOT NULL COMMENT '模版内容',
    created_by  varchar(32)  NOT NULL COMMENT '创建人',
    create_time datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by  varchar(32)  NOT NULL COMMENT '修改人',
    update_time datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='邮件模版';

DROP TABLE IF EXISTS sys_header;

CREATE TABLE sys_header
(
    user_id int          NOT NULL COMMENT '管理员ID',
    code    varchar(128) NOT NULL COMMENT '头编码',
    content varchar(2047) DEFAULT NULL COMMENT '头内容',
    PRIMARY KEY (user_id, code)
) COMMENT='列表头';

DROP TABLE IF EXISTS sys_login_log;

CREATE TABLE sys_login_log
(
    id            int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    user_id       int         NOT NULL COMMENT '管理员ID',
    username      varchar(32) NOT NULL COMMENT '登录账号',
    full_name     varchar(16) NOT NULL COMMENT '姓名',
    browser       varchar(256)         DEFAULT NULL COMMENT '浏览器',
    sys_info      varchar(256)         DEFAULT NULL COMMENT '系统信息',
    login_ip      bigint      NOT NULL COMMENT '登录地址',
    login_address varchar(256)         DEFAULT NULL COMMENT '登录地点',
    success_flag  tinyint     NOT NULL COMMENT '状态:0-失败,1-成功',
    remark        varchar(128)         DEFAULT NULL COMMENT '备注',
    times         int         not null default 0 comment '耗时',
    create_time   datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (id)
) COMMENT='登录日志';

DROP TABLE IF EXISTS sys_menu;

CREATE TABLE sys_menu
(
    id              int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    pid             int          NOT NULL DEFAULT '0' COMMENT '父ID',
    type_flag       tinyint      NOT NULL DEFAULT '0' COMMENT '菜单类型:0-目录,1-菜单,2-功能',
    path            varchar(128) NOT NULL COMMENT '菜单路径',
    name            varchar(64)  NOT NULL COMMENT '菜单名称',
    name_cn         varchar(64)           DEFAULT NULL COMMENT '菜单中文名',
    component       varchar(128)          DEFAULT NULL COMMENT '菜单组件路径',
    title           varchar(128)          DEFAULT NULL COMMENT '国际化编码',
    redirect        varchar(256)          DEFAULT NULL COMMENT '重新定向路径',
    icon            varchar(256)          DEFAULT NULL COMMENT '图标',
    hide_flag       tinyint      NOT NULL DEFAULT '0' COMMENT '是否隐藏',
    keep_alive_flag tinyint      NOT NULL DEFAULT '0' COMMENT '是否缓存',
    affix_flag      tinyint      NOT NULL DEFAULT '0' COMMENT '是否固定tag',
    iframe_flag     tinyint      NOT NULL DEFAULT '0' COMMENT '是否外链',
    link            varchar(128)          DEFAULT NULL COMMENT '外链',
    perm_code       varchar(128)          DEFAULT NULL COMMENT '权限码',
    orders          tinyint      NOT NULL DEFAULT '0' COMMENT '排序',
    created_by      varchar(32)  NOT NULL COMMENT '创建人',
    create_time     datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by      varchar(32)  NOT NULL COMMENT '修改人',
    update_time     datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='菜单';

DROP TABLE IF EXISTS sys_menu_resource;

CREATE TABLE sys_menu_resource
(
    id        int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    menu_id   int          NOT NULL COMMENT '菜单ID',
    uri_code  varchar(128) NOT NULL COMMENT '资源编码',
    type_flag tinyint      NOT NULL COMMENT '类型:0-半选,1-全选',
    PRIMARY KEY (id)
) COMMENT='菜单和资源关系';

DROP TABLE IF EXISTS sys_position;

CREATE TABLE sys_position
(
    id          int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    name        varchar(32) NOT NULL COMMENT '名称',
    status_flag tinyint     NOT NULL COMMENT '状态:0-禁用,1-启用',
    remark      varchar(64)          DEFAULT NULL COMMENT '备注',
    created_by  varchar(32) NOT NULL COMMENT '创建人',
    create_time timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by  varchar(32) NOT NULL COMMENT '修改人',
    update_time timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='岗位';

DROP TABLE IF EXISTS sys_project;

CREATE TABLE sys_project
(
    id           int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    code         varchar(64)  NOT NULL COMMENT '项目编码',
    name         varchar(64)  NOT NULL COMMENT '项目名称',
    uri          varchar(256) NOT NULL COMMENT '资源地址',
    service_flag tinyint      NOT NULL DEFAULT '0' COMMENT '服务类型:0-对内,1-对外',
    created_by   varchar(32)  NOT NULL COMMENT '创建人',
    create_time  datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by   varchar(32)  NOT NULL COMMENT '修改人',
    update_time  datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='项目配置';

DROP TABLE IF EXISTS sys_resource;

CREATE TABLE sys_resource
(
    id               int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    module           varchar(32)  NOT NULL COMMENT '资源模块',
    parent_code      varchar(256)          DEFAULT NULL COMMENT '父资源编码,提供建立关系',
    code             varchar(256) NOT NULL COMMENT '资源编码,必须唯一',
    name             varchar(64)  NOT NULL COMMENT '资源名称',
    uri              varchar(256) NOT NULL COMMENT '资源地址',
    method           varchar(8)            DEFAULT NULL COMMENT '方法类型:POST,PUT,GET,DELETE',
    function_flag    tinyint      NOT NULL DEFAULT '1' COMMENT '功能权限:0-否,1-是',
    data_flag        tinyint      NOT NULL DEFAULT '0' COMMENT '数据权限:0-否,1-是',
    desensitize_flag tinyint               DEFAULT '0' COMMENT '数据脱敏:0-否,1-是',
    desensitize_json varchar(1024)         DEFAULT NULL COMMENT '脱敏信息:{field:{start:1,end:100}}',
    leaf_flag        tinyint      NOT NULL DEFAULT '1' COMMENT '是否叶子:0-否,1-是',
    type_flag        tinyint      NOT NULL DEFAULT '0' COMMENT '资源类型:0-系统,1-类,2-方法',
    service_flag     tinyint               DEFAULT '0' COMMENT '服务类型:0-对内,1-对外',
    update_flag      tinyint      NOT NULL DEFAULT '0' COMMENT '是否更新:0-否,1-是',
    created_by       varchar(32)  NOT NULL COMMENT '创建人',
    create_time      datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by       varchar(32)  NOT NULL COMMENT '修改人',
    update_time      datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='资源';

DROP TABLE IF EXISTS sys_role;

CREATE TABLE sys_role
(
    id          int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    name        varchar(32) NOT NULL COMMENT '名字',
    module_flag tinyint     NOT NULL DEFAULT '0' COMMENT '模块类型:0-权限模块,数字字典',
    remark      varchar(64)          DEFAULT NULL COMMENT '备注',
    state_flag  tinyint     NOT NULL DEFAULT '1' COMMENT '状态:字典dict.permission.statusFlag',
    created_by  varchar(32) NOT NULL COMMENT '创建人',
    create_time datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by  varchar(32) NOT NULL COMMENT '修改人',
    update_time datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='角色';


DROP TABLE IF EXISTS sys_role_menu;

CREATE TABLE sys_role_menu
(
    id        int     NOT NULL AUTO_INCREMENT COMMENT '主键',
    role_id   int     NOT NULL COMMENT '角色ID',
    menu_id   int     NOT NULL COMMENT '菜单ID',
    type_flag tinyint NOT NULL COMMENT '类型:0-半选,1-全选',
    PRIMARY KEY (id)
) COMMENT='角色和菜单关系';

DROP TABLE IF EXISTS sys_service_log;

CREATE TABLE sys_service_log
(
    id              int          NOT NULL AUTO_INCREMENT COMMENT '主键',
    user_id         int          NOT NULL COMMENT '管理员ID',
    username        varchar(32)  NOT NULL COMMENT '用户名',
    full_name       varchar(16)  NOT NULL COMMENT '姓名',
    target_server   varchar(64)           DEFAULT NULL COMMENT '访问实例',
    schema          varchar(64)           DEFAULT NULL COMMENT '访问协议',
    request_path    varchar(128) NOT NULL COMMENT '访问地址',
    request_method  varchar(32)  NOT NULL COMMENT '访问方法',
    browser         varchar(64)           DEFAULT NULL COMMENT '访问者浏览器',
    client_os       varchar(32)  NOT NULL COMMENT '访问者操作系统',
    request_ip      mediumtext   NOT NULL COMMENT '登录地址',
    request_address varchar(32)  NOT NULL COMMENT '访问来源地点',
    request_body    longtext     NOT NULL COMMENT '访问入参',
    request_time    datetime              DEFAULT NULL COMMENT '请求开始时间',
    response_time   datetime              DEFAULT NULL COMMENT '请求结束时间',
    response_data   longtext     NOT NULL COMMENT '访问返回值',
    times           bigint       NOT NULL DEFAULT '0' COMMENT '访问耗时',
    create_time     datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (id)
) COMMENT='服务访问';

DROP TABLE IF EXISTS sys_user;

CREATE TABLE sys_user
(
    id                 int         NOT NULL AUTO_INCREMENT COMMENT '主键',
    username           varchar(32) NOT NULL COMMENT '用户名',
    password           varchar(64) NOT NULL COMMENT '密码',
    full_name          varchar(16) NOT NULL COMMENT '姓名',
    email              varchar(64) NOT NULL COMMENT '邮箱',
    department_id      int         NOT NULL COMMENT '部门ID,数据权限',
    department_id_path varchar(256) NULL comment '部门路径',
    position_id        int                  DEFAULT NULL COMMENT '岗位ID',
    status_flag        tinyint     NOT NULL COMMENT '状态:0-禁用,1-启用',
    status_remark      varchar(128)         DEFAULT NULL COMMENT '状态描述',
    term_validity      datetime             DEFAULT NULL COMMENT '有效期,格式:yyyy-MM-dd HH:mm:ss',
    type_flag          tinyint     NOT NULL COMMENT '管理员类型:0-普通管理员,1-系统创建',
    created_by         varchar(32) NOT NULL COMMENT '创建人',
    create_time        datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    updated_by         varchar(32) NOT NULL COMMENT '修改人',
    update_time        datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (id)
) COMMENT='系统管理员';

DROP TABLE IF EXISTS sys_user_role;

CREATE TABLE sys_user_role
(
    id      int NOT NULL AUTO_INCREMENT COMMENT '主键',
    user_id int NOT NULL COMMENT '管理员ID',
    role_id int NOT NULL COMMENT '角色ID',
    PRIMARY KEY (id)
) COMMENT='管理员和角色关系';
