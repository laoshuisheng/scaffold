package com.lap.permission.validator.login;

import com.lap.framework.exception.ExceptionHandler;
import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.request.LoginRequest;
import com.lap.permission.validator.LoginValidator;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Date;

@Order(5)
@Component
public class TermValidityValidator implements LoginValidator {

    @Override
    public void validate(LoginRequest request, User user) {
        if (new Date().after(user.getTermValidity()))
            ExceptionHandler.execInterrupt("10105");
    }

}
