/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.Department;
import com.lap.permission.dto.query.DepartmentQuery;
import com.lap.permission.dto.response.DepartmentDto;

import java.util.List;

/**
 * 部门业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface DepartmentService {
    /**
     * 分页查询部门
     *
     * @param query 查询条件,继承分页信息
     * @return 部门集合
     */
    PageDTO<List<Department>> queryPage(DepartmentQuery query);

    /**
     * 生成树形
     *
     * @return 部门树形
     */
    List<DepartmentDto> queryTree();

    /**
     * 根据ID查询部门
     *
     * @param departmentId 部门Id
     * @return 部门
     */
    Department queryById(Integer departmentId);

    /**
     * 根据父ID查询部门信息
     *
     * @param pid 父ID
     * @return 部门
     */
    List<Department> queryByPid(Integer pid);

    /**
     * 根据编码查询部门
     *
     * @param code 部门编码
     * @return 部门
     */
    Department queryByCode(String code);

    /**
     * 根据id集合查询部门
     *
     * @param list id集合
     * @return 部门记录集合
     */
    List<Department> queryByIds(List<Integer> list);

    /**
     * 保存部门
     *
     * @param department 部门
     * @return 部门Id
     */
    Integer insertDepartment(Department department);

    /**
     * 根据ID更新部门
     *
     * @param department 部门
     * @return 更新数量
     */
    int updateDepartment(Department department);

    /**
     * 根据ID删除部门
     *
     * @param departmentId 部门Id
     * @return 删除数量
     */
    int deleteById(Integer departmentId);

}
