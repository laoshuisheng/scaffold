/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.ServiceLog;
import com.lap.permission.dto.query.ServiceLogQuery;

import java.util.List;

/**
 * 服务访问业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface ServiceLogService {
    /**
     * 分页查询服务访问
     *
     * @param query 查询条件,继承分页信息
     * @return 服务访问集合
     */
    PageDTO<List<ServiceLog>> queryPage(ServiceLogQuery query);

    /**
     * 根据ID查询服务访问
     *
     * @param serviceLogId 服务访问Id
     * @return 服务访问
     */
    ServiceLog queryById(Integer serviceLogId);

    /**
     * 保存服务访问
     *
     * @param serviceLog 服务访问
     * @return 服务访问Id
     */
    Integer insertServiceLog(ServiceLog serviceLog);

}
