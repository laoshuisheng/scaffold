/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.Header;

/**
 * 列表头业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface HeaderService {
    /**
     * 保存列表头
     *
     * @param header 列表头
     * @return 列表头Id
     */
    String insertHeader(Header header);

    /**
     * 根据ID查询列表头
     *
     * @param userId 用户ID
     * @param code   编码
     * @return 列表头
     */
    Header queryByUserIdAndCode(Integer userId, String code);

    /**
     * 根据ID更新列表头
     *
     * @param header 列表头
     * @return 更新数量
     */
    int updateHeader(Header header);

}
