package com.lap.permission.cachekey;

public interface DictKey {
    /**
     * 字典明细编码
     */
    String DETAIL_CODE = "dict:detail:code";
    /**
     * 字典明细编码集合
     */
    String DETAIL_CODES = "dict:detail:codes";
}
