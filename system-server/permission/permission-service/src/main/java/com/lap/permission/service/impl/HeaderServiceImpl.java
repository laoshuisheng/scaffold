/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.permission.cachekey.HeaderKey;
import com.lap.permission.dal.HeaderDAO;
import com.lap.permission.dto.entity.Header;
import com.lap.permission.service.HeaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class HeaderServiceImpl implements HeaderService {

    private final HeaderDAO headerDAO;

    @CacheEvict(value = HeaderKey.VALUE, key = "#header.userId+':'+#header.code")
    @Override
    public String insertHeader(Header header) {
        headerDAO.insertHeader(header);
        return header.getCode();
    }

    @Cacheable(value = HeaderKey.VALUE, key = "#userId+':'+#code", unless = "#result == null")
    @Override
    public Header queryByUserIdAndCode(Integer userId, String code) {
        return headerDAO.queryByUserIdAndCode(userId, code);
    }

    @CacheEvict(value = HeaderKey.VALUE, key = "#header.userId+':'+#header.code")
    @Override
    public int updateHeader(Header header) {
        return headerDAO.updateHeader(header);
    }

}
