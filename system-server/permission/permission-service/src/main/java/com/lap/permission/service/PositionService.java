/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.Position;
import com.lap.permission.dto.query.PositionQuery;

import java.util.List;

/**
 * 岗位业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface PositionService {
    /**
     * 分页查询岗位
     *
     * @param query 查询条件,继承分页信息
     * @return 岗位集合
     */
    PageDTO<List<Position>> queryPage(PositionQuery query);

    /**
     * 根据ID查询岗位
     *
     * @param positionId 岗位Id
     * @return 岗位
     */
    Position queryById(Integer positionId);

    /**
     * 根据名称查询岗位信息
     *
     * @param name 岗位名称
     * @return 岗位信息
     */
    Position queryByName(String name);

    /**
     * 保存岗位
     *
     * @param position 岗位
     * @return 岗位Id
     */
    Integer insertPosition(Position position);

    /**
     * 根据ID更新岗位
     *
     * @param position 岗位
     * @return 更新数量
     */
    int updatePosition(Position position);

    /**
     * 根据ID删除岗位
     *
     * @param positionId 岗位Id
     * @return 删除数量
     */
    int deleteById(Integer positionId);
}
