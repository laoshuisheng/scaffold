package com.lap.permission.validator;

import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.request.LoginRequest;

/**
 * <pre>
 * 登录验证接口，可以实现该类实现验证逻辑
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface LoginValidator {

    void validate(LoginRequest request, User user);

}
