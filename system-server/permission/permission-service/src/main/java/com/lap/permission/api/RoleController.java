/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.dto.response.PageDTO;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.framework.validator.Update;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.Role;
import com.lap.permission.dto.query.RoleQuery;
import com.lap.permission.dto.request.RoleRequest;
import com.lap.permission.service.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <pre>
 *  角色 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see RoleService
 * @see RoleRequest
 * @see Role
 */
@Tag(name = "角色")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/roles")
public class RoleController extends BaseController {

    private final RoleService roleService;

    private final Convert convert;

    @Operation(summary = "角色列表")
    @GetMapping
    public PageDTO<List<Role>> queryPage(RoleQuery query) {
        return roleService.queryPage(query);
    }

    @Operation(summary = "角色新增")
    @Repeat
    @PostMapping
    public Integer saveRole(@RequestBody @Valid RoleRequest request) {
        Role query = roleService.queryByName(request.getName());
        if (Objects.nonNull(query))
            ExceptionHandler.execInterrupt("10081");

        return roleService.insertRole(convert.toRole(request));
    }

    @Operation(summary = "角色更新")
    @Repeat
    @PutMapping
    public Integer updateRole(@RequestBody @Validated(Update.class) RoleRequest request) {
        Role query = roleService.queryByName(request.getName());
        if (Objects.nonNull(query) && !Objects.equals(query.getId(), request.getId()))
            ExceptionHandler.execInterrupt("10081");

        return roleService.updateRole(convert.toRole(request));
    }

    @Operation(summary = "角色删除")
    @Repeat
    @DeleteMapping("{roleId}")
    public Integer deleteById(@PathVariable Integer roleId) {
        return roleService.deleteById(roleId);
    }

}
