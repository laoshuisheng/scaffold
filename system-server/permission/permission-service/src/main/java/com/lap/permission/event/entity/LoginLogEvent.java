package com.lap.permission.event.entity;

import com.lap.framework.entity.Device;
import lombok.Data;

/**
 * <pre>
 * extends ApplicationEvent
 * </pre>
 */
@Data
public class LoginLogEvent {
    /**
     * 设备信息
     */
    private Device device;
    /**
     * 登录账号
     */
    private String username;
    /**
     * ip
     */
    private String ip;
    /**
     * 登录状态
     */
    private Integer statusFlag;
    /**
     * 登录备注
     */
    private String remark;
    /**
     * 登录耗时
     */
    private Long times;

}
