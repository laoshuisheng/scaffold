package com.lap;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 权限启动入口
 *
 * @author Shuisheng Lao(劳水生)
 * @version 1.0.0
 */
@MapperScan("com.lap.permission.dal")
@EnableDiscoveryClient
@EnableTransactionManagement
@SpringBootApplication
public class PermissionBootstrap {

    public static void main(String[] args) {
        var app = new SpringApplication(PermissionBootstrap.class);
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }

}
