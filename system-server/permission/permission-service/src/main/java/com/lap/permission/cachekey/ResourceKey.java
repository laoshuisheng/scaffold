package com.lap.permission.cachekey;

/**
 * 资源缓存key
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface ResourceKey {
    /**
     * 用户资源
     */
    String RESOURCE_USER = "resource:user";
    /**
     * 资源
     */
    String RESOURCE_TREE = "resource:tree";
}
