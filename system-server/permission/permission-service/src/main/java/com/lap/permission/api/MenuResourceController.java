/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.enums.YesNo;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.dto.entity.MenuResource;
import com.lap.permission.dto.request.MenuResourceRequest;
import com.lap.permission.service.MenuResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <pre>
 *  菜单和资源关系 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see MenuResourceService
 * @see MenuResourceRequest
 * @see MenuResource
 */
@Tag(name = "菜单和资源关系")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/menu-resources")
public class MenuResourceController extends BaseController {

    private final MenuResourceService menuResourceService;

    @Operation(summary = "菜单和资源关系列表")
    @GetMapping("{menuId}")
    public List<String> queryByMenuId(@PathVariable Integer menuId) {
        return menuResourceService.queryByMenuId(menuId);
    }

    @Operation(summary = "菜单和资源关系新增")
    @Repeat
    @PostMapping
    public Integer saveMenuResource(@RequestBody @Valid MenuResourceRequest request) {
        Integer menuId = request.getMenuId();
        List<MenuResource> dataList = new ArrayList<>();
        if (Objects.nonNull(request.getCheckList())) {
            dataList.addAll(request.getCheckList().stream()
                    .map(e -> new MenuResource(menuId, e, YesNo.YES.getValue()))
                    .toList());
        }
        if (Objects.nonNull(request.getHalfList())) {
            dataList.addAll(request.getHalfList().stream()
                    .map(e -> new MenuResource(menuId, e, YesNo.NO.getValue()))
                    .toList());
        }
        return menuResourceService.insertMenuResource(menuId, dataList);
    }

}
