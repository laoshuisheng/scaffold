/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.permission.cachekey.MenuKey;
import com.lap.permission.cachekey.ResourceKey;
import com.lap.permission.cachekey.UserKey;
import com.lap.permission.dal.RoleMenuDAO;
import com.lap.permission.dto.entity.RoleMenu;
import com.lap.permission.manager.RoleMenuManager;
import com.lap.permission.service.RoleMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    private final RoleMenuManager roleMenuManager;

    private final RoleMenuDAO roleMenuDAO;

    @Override
    public List<Integer> queryByRoleId(Integer roleId) {
        return roleMenuDAO.queryByRoleId(roleId);
    }

    @CacheEvict(value = {UserKey.USER_INFO, MenuKey.MENU_TREE_USER, ResourceKey.RESOURCE_USER}, allEntries = true)
    @Override
    public Integer insertRoleMenu(Integer roleId, List<RoleMenu> dataList) {
        return roleMenuManager.insertRoleMenu(roleId, dataList);
    }

}
