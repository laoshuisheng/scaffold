/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.Dict;
import com.lap.permission.dto.entity.DictDetail;
import com.lap.permission.dto.query.DictQuery;

import java.util.List;

/**
 * 数字字典业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface DictService {
    /**
     * 分页查询数字字典
     *
     * @param query 查询条件,继承分页信息
     * @return 数字字典集合
     */
    PageDTO<List<Dict>> queryPage(DictQuery query);

    /**
     * 根据ID查询数字字典
     *
     * @param dictId 数字字典Id
     * @return 数字字典
     */
    Dict queryById(Integer dictId);

    /**
     * 根据编码查询数字字典
     *
     * @param code 数字字典编码
     * @return 数字字典
     */
    Dict queryByCode(String code);

    /**
     * 保存数字字典
     *
     * @param dict     数字字典
     * @param dataList 数字字典明细
     * @return 数字字典Id
     */
    Integer insertDict(Dict dict, List<DictDetail> dataList);

    /**
     * 根据ID更新数字字典
     *
     * @param dict 数字字典
     * @return 更新数量
     */
    int updateDict(Dict dict);

    /**
     * 根据ID删除数字字典
     *
     * @param dictId 数字字典Id
     * @return 删除数量
     */
    int deleteById(Integer dictId);

}
