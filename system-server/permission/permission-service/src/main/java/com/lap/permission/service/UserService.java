/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.pojo.UserState;
import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.query.UserQuery;
import com.lap.permission.dto.response.UserDto;
import com.lap.permission.dto.response.UserInfoDto;

import java.util.List;

/**
 * 系统管理员业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface UserService {
    /**
     * 分页查询系统管理员
     *
     * @param query 查询条件,继承分页信息
     * @return 系统管理员集合
     */
    PageDTO<List<UserDto>> queryPage(UserQuery query);

    /**
     * 分页查询简易管理员
     *
     * @param query 查询条件,继承分页信息
     * @return 系统管理员集合
     */
    PageDTO<List<User>> querySelect(UserQuery query);

    /**
     * 根据ID查询系统管理员
     *
     * @param userId 系统管理员Id
     * @return 系统管理员
     */
    User queryById(Integer userId);

    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户吗
     * @return 用户信息
     */
    User queryByUsername(String username);

    /**
     * 根据用户ID查询用户登录信息
     *
     * @param userId 系统管理员Id
     * @return 用户登录信息
     */
    UserInfoDto queryUserInfo(Integer userId);

    /**
     * 根据用户id集合查询用户
     *
     * @param ids 用户id集合
     * @return 用户集合
     */
    List<User> queryByIds(List<Integer> ids);

    /**
     * 保存系统管理员
     *
     * @param user 系统管理员
     * @return 系统管理员Id
     */
    Integer insertUser(User user);

    /**
     * 根据ID更新系统管理员
     *
     * @param user 系统管理员
     * @return 更新数量
     */
    int updateUser(User user);

    /**
     * 根据id更新系统管理状态
     *
     * @param userState 状态对象
     * @return 更新数量
     */
    int updateState(UserState userState);

    /**
     * 根据ID删除系统管理员
     *
     * @param userId 系统管理员Id
     * @return 删除数量
     */
    int deleteById(Integer userId);

}
