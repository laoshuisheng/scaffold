/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.cachekey.MenuKey;
import com.lap.permission.cachekey.ResourceKey;
import com.lap.permission.cachekey.UserKey;
import com.lap.permission.dal.RoleDAO;
import com.lap.permission.dto.entity.Role;
import com.lap.permission.dto.query.RoleQuery;
import com.lap.permission.manager.RoleManager;
import com.lap.permission.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleManager roleManager;

    private final RoleDAO roleDAO;

    @Override
    public PageDTO<List<Role>> queryPage(RoleQuery query) {
        long count = roleDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(roleDAO.queryPage(query), count);
    }

    @Override
    public Role queryById(Integer roleId) {
        return roleDAO.queryById(roleId);
    }

    @Override
    public Role queryByName(String name) {
        return roleDAO.queryByName(name);
    }

    @Override
    public Integer insertRole(Role role) {
        roleDAO.insertRole(role);
        return role.getId();
    }

    @Override
    public int updateRole(Role role) {
        return roleDAO.updateRole(role);
    }

    @CacheEvict(value = {UserKey.USER_INFO, MenuKey.MENU_TREE_USER, ResourceKey.RESOURCE_USER}, allEntries = true)
    @Override
    public int deleteById(Integer roleId) {
        return roleManager.deleteById(roleId);
    }

}
