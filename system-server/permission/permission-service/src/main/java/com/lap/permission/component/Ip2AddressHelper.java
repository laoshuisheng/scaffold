package com.lap.permission.component;

import com.lap.framework.tools.JsonUtil;
import com.lap.permission.enums.IpRound;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
public class Ip2AddressHelper {
    public static final String[] NATIVE_IPS = {"localhost", "127.0.0.1"};
    public static final String NATIVE_ADDRESS = "本地网(01)";
    public static final String LOCAL_ADDRESS = "局域网(01)";
    public static final String UNKNOWN = "未知地址";

    @Qualifier("ipRestTemplate")
    private final RestTemplate restTemplate;

    @Value("${ip.url:https://whois.pconline.com.cn/ipJson.jsp?json=true&ip={ip}}")
    private String url;

    public String getAddress(String ip) {
        if (StringUtils.isBlank(ip)) {
            return null;
        }

        for (String str : NATIVE_IPS) {
            if (str.equals(ip)) {
                return NATIVE_ADDRESS;
            }
        }

        if (IpRound.validIp(ip)) {
            return LOCAL_ADDRESS;
        }

        try {
            String resultJson = restTemplate.getForObject(url, String.class, ip);
            Map<String, Object> map = JsonUtil.toObject(resultJson, Map.class);
            if (Objects.nonNull(map)) {
                return (String) map.get("addr");
            }
        } catch (Exception e) {
            log.warn("call ip is error:{}", e.getLocalizedMessage());
        }
        return UNKNOWN;
    }

}
