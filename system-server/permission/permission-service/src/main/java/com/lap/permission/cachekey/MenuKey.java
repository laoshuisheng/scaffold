package com.lap.permission.cachekey;

/**
 * 菜单缓存key
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface MenuKey {
    /**
     * 菜单
     */
    String MENU_TREE = "menu:tree";
    /**
     * 用户菜单
     */
    String MENU_TREE_USER = "menu:tree:user";
}
