/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.RoleMenu;

import java.util.List;

/**
 * 角色和菜单关系业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface RoleMenuService {
    /**
     * 根据角色ID查询 菜单ID
     *
     * @param roleId 角色ID
     * @return 角色和菜单关系集合
     */
    List<Integer> queryByRoleId(Integer roleId);

    /**
     * 保存角色和菜单关系
     *
     * @param roleId   角色id
     * @param dataList 角色和菜单关系
     * @return 角色和菜单关系Id
     */
    Integer insertRoleMenu(Integer roleId, List<RoleMenu> dataList);

}
