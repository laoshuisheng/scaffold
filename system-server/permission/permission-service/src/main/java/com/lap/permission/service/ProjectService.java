/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.Project;
import com.lap.permission.dto.query.ProjectQuery;

import java.util.List;

/**
 * 项目配置业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface ProjectService {
    /**
     * 分页查询项目配置
     *
     * @param query 查询条件,继承分页信息
     * @return 项目配置集合
     */
    PageDTO<List<Project>> queryPage(ProjectQuery query);

    /**
     * 根据ID查询项目配置
     *
     * @param projectId 项目配置Id
     * @return 项目配置
     */
    Project queryById(Integer projectId);

    /**
     * 根据编码查询项目配置
     *
     * @param code 项目编码
     * @return 项目配置
     */
    Project queryByCode(String code);

    /**
     * 保存项目配置
     *
     * @param project 项目配置
     * @return 项目配置Id
     */
    Integer insertProject(Project project);

    /**
     * 根据ID更新项目配置
     *
     * @param project 项目配置
     * @return 更新数量
     */
    int updateProject(Project project);

    /**
     * 根据ID删除项目配置
     *
     * @param projectId 项目配置Id
     * @return 删除数量
     */
    int deleteById(Integer projectId);

}
