/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.ProjectDAO;
import com.lap.permission.dto.entity.Project;
import com.lap.permission.dto.query.ProjectQuery;
import com.lap.permission.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectDAO projectDAO;

    @Override
    public PageDTO<List<Project>> queryPage(ProjectQuery query) {
        long count = projectDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(projectDAO.queryPage(query), count);
    }

    @Override
    public Project queryById(Integer projectId) {
        return projectDAO.queryById(projectId);
    }

    @Override
    public Project queryByCode(String code) {
        return projectDAO.queryByCode(code);
    }

    @Override
    public Integer insertProject(Project project) {
        projectDAO.insertProject(project);
        return project.getId();
    }

    @Override
    public int updateProject(Project project) {
        return projectDAO.updateProject(project);
    }

    @Override
    public int deleteById(Integer projectId) {
        return projectDAO.deleteById(projectId);
    }

}
