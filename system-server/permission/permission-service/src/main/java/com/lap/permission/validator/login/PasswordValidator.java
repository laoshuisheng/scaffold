package com.lap.permission.validator.login;

import com.lap.framework.exception.ExceptionHandler;
import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.request.LoginRequest;
import com.lap.permission.validator.LoginValidator;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(1)
@Component
public class PasswordValidator implements LoginValidator {

    @Override
    public void validate(LoginRequest request, User user) {
        if (!user.getPassword().equals(request.getPassword()))
            ExceptionHandler.execInterrupt("10103");
    }

}
