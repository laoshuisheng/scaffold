/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.context.holder.UserHolder;
import com.lap.framework.controller.BaseController;
import com.lap.framework.enums.YesNo;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.framework.tools.IntStrategy;
import com.lap.framework.validator.Update;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.Menu;
import com.lap.permission.dto.request.MenuRequest;
import com.lap.permission.dto.response.MenuDto;
import com.lap.permission.enums.MenuType;
import com.lap.permission.service.MenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <pre>
 *  菜单 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see MenuService
 * @see MenuRequest
 * @see Menu
 */
@Tag(name = "菜单")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/menus")
public class MenuController extends BaseController {

    private final MenuService menuService;

    private final Convert convert;

    @Operation(summary = "菜单树")
    @GetMapping
    public List<MenuDto> queryTree() {
        return menuService.queryTree();
    }

    @Operation(summary = "菜单当前用户获取")
    @GetMapping("me")
    public List<MenuDto> queryForMe() {
        return menuService.queryByUserId(UserHolder.getUserId());
    }

    @Operation(summary = "菜单新增")
    @Repeat
    @PostMapping
    public Integer saveMenu(@RequestBody @Valid MenuRequest request) {
        return menuService.insertMenu(getMenu(request));
    }

    private Menu getMenu(MenuRequest request) {
        Menu menu = convert.toMenu(request);
        menu.setPid(IntStrategy.getLast(request.getPidPaths()));
        if (MenuType.FUN.getValue().equals(menu.getTypeFlag()))
            menu.setHideFlag(YesNo.YES.getValue());

        return menu;
    }

    @Operation(summary = "菜单更新")
    @Repeat
    @PutMapping
    public Integer updateMenu(@RequestBody @Validated(Update.class) MenuRequest request) {
        return menuService.updateMenu(getMenu(request));
    }

    @Operation(summary = "菜单删除")
    @Repeat
    @DeleteMapping("{menuId}")
    public Integer deleteById(@PathVariable Integer menuId) {
        List<Menu> queryList = menuService.queryByPid(menuId);
        if (!queryList.isEmpty())
            ExceptionHandler.execInterrupt("10141", queryList.size());

        return menuService.deleteById(menuId);
    }

}
