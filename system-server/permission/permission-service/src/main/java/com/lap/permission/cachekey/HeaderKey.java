package com.lap.permission.cachekey;

/**
 * 头信息缓存
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface HeaderKey {
    /**
     * 头缓存
     */
    String VALUE = "header";
}
