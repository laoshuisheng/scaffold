package com.lap.permission.cachekey;

/**
 * 管理员缓存key
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface UserKey {
    /**
     * 管理员
     */
    String USER = "user";
    /**
     * 登录管理员
     */
    String USER_INFO = "info";
}
