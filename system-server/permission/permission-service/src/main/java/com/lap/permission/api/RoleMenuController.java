/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.enums.YesNo;
import com.lap.permission.dto.entity.RoleMenu;
import com.lap.permission.dto.request.RoleMenuRequest;
import com.lap.permission.service.RoleMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *  角色和菜单关系 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see RoleMenuService
 * @see RoleMenuRequest
 * @see RoleMenu
 */
@Tag(name = "角色和菜单关系")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/role-menus")
public class RoleMenuController extends BaseController {

    private final RoleMenuService roleMenuService;

    @Operation(summary = "角色和菜单关系获取")
    @GetMapping("{roleId}")
    public List<Integer> queryByRoleId(@PathVariable Integer roleId) {
        return roleMenuService.queryByRoleId(roleId);
    }

    @Operation(summary = "角色和菜单关系新增")
    @PostMapping
    public Integer saveRoleMenu(@RequestBody @Valid RoleMenuRequest request) {
        Integer roleId = request.getRoleId();
        List<RoleMenu> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(request.getCheckList())) {
            dataList.addAll(request.getCheckList().stream()
                    .map(e -> new RoleMenu(roleId, e, YesNo.YES.getValue()))
                    .toList());
        }
        if (CollectionUtils.isNotEmpty(request.getHalfList())) {
            dataList.addAll(request.getHalfList().stream()
                    .map(e -> new RoleMenu(roleId, e, YesNo.NO.getValue()))
                    .toList());
        }
        return roleMenuService.insertRoleMenu(roleId, dataList);
    }

}
