/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.Menu;
import com.lap.permission.dto.response.MenuDto;

import java.util.List;

/**
 * 菜单业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface MenuService {
    /**
     * 菜单树
     *
     * @return 菜单集合
     */
    List<MenuDto> queryTree();

    /**
     * 获取当前用户菜单
     *
     * @param userId 用户id
     * @return 菜单集合
     */
    List<MenuDto> queryByUserId(Integer userId);

    /**
     * 根据ID查询菜单
     *
     * @param menuId 菜单Id
     * @return 菜单
     */
    Menu queryById(Integer menuId);

    /**
     * 根据父id查询子菜单
     *
     * @param parentId 父id
     * @return 菜单集合
     */
    List<Menu> queryByPid(Integer parentId);

    /**
     * 保存菜单
     *
     * @param menu 菜单
     * @return 菜单Id
     */
    Integer insertMenu(Menu menu);

    /**
     * 根据ID更新菜单
     *
     * @param menu 菜单
     * @return 更新数量
     */
    int updateMenu(Menu menu);

    /**
     * 根据ID删除菜单
     *
     * @param menuId 菜单Id
     * @return 删除数量
     */
    int deleteById(Integer menuId);

}
