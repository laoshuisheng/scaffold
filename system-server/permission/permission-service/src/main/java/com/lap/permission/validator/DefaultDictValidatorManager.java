package com.lap.permission.validator;

import com.lap.framework.i18n.entity.LocalInfo;
import com.lap.framework.i18n.holder.LangHolder;
import com.lap.framework.spring.holder.SpringHolder;
import com.lap.framework.validate.manager.DictValidatorManager;
import com.lap.permission.dto.entity.DictDetail;
import com.lap.permission.service.DictDetailService;

public class DefaultDictValidatorManager implements DictValidatorManager {

    @Override
    public boolean validate(String code, Integer value) {
        DictDetailService detailService = SpringHolder.getBean(DictDetailService.class);
        LocalInfo localInfo = LangHolder.get();
        DictDetail dictDetail = detailService.queryByDictCode(code, value, localInfo.storeLang());
        return dictDetail != null;
    }

}
