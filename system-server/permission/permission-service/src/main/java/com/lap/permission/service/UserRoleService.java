/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.UserRole;

import java.util.List;

/**
 * 管理员和角色关系业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface UserRoleService {
    /**
     * 根据管理员ID查询管理员和角色关系
     *
     * @param userId 管理员Id
     * @return 管理员和角色关系
     */
    List<Integer> queryByUserId(Integer userId);

    /**
     * 保存管理员和角色关系
     *
     * @param userId 管理员id
     * @param list   管理员和角色关系集合
     * @return 管理员和角色关系Id
     */
    Integer insertUserRole(Integer userId, List<UserRole> list);

}
