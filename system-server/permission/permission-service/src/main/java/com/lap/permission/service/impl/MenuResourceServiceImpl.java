/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.permission.cachekey.ResourceKey;
import com.lap.permission.dal.MenuResourceDAO;
import com.lap.permission.dto.entity.MenuResource;
import com.lap.permission.manager.MenuResourceManager;
import com.lap.permission.service.MenuResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MenuResourceServiceImpl implements MenuResourceService {

    private final MenuResourceDAO menuResourceDAO;

    private final MenuResourceManager menuResourceManager;

    @Override
    public List<String> queryByMenuId(Integer menuId) {
        return menuResourceDAO.queryByMenuId(menuId);
    }

    @CacheEvict(value = {ResourceKey.RESOURCE_USER}, allEntries = true)
    @Override
    public Integer insertMenuResource(Integer menuId, List<MenuResource> dataList) {
        return menuResourceManager.insertMenuResource(menuId, dataList);
    }

}
