/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.AccessLog;
import com.lap.permission.dto.query.AccessLogQuery;

import java.util.List;

/**
 * 访问日志业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface AccessLogService {
    /**
     * 分页查询访问日志
     *
     * @param query 查询条件,继承分页信息
     * @return 访问日志集合
     */
    PageDTO<List<AccessLog>> queryPage(AccessLogQuery query);

    /**
     * 保存访问日志
     *
     * @param accessLog 访问日志
     * @return 访问日志Id
     */
    Integer insertAccessLog(AccessLog accessLog);

    /**
     * 根据ID删除访问日志
     *
     * @param accessLogId 访问日志Id
     * @return 删除数量
     */
    int deleteById(Integer accessLogId);

}
