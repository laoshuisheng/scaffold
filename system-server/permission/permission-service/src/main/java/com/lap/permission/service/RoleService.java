/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.Role;
import com.lap.permission.dto.query.RoleQuery;

import java.util.List;

/**
 * 角色业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface RoleService {
    /**
     * 分页查询角色
     *
     * @param query 查询条件,继承分页信息
     * @return 角色集合
     */
    PageDTO<List<Role>> queryPage(RoleQuery query);

    /**
     * 根据ID查询角色
     *
     * @param roleId 角色Id
     * @return 角色
     */
    Role queryById(Integer roleId);

    /**
     * 根据名称查询角色
     *
     * @param name 名称
     * @return 角色
     */
    Role queryByName(String name);

    /**
     * 保存角色
     *
     * @param role 角色
     * @return 角色Id
     */
    Integer insertRole(Role role);

    /**
     * 根据ID更新角色
     *
     * @param role 角色
     * @return 更新数量
     */
    int updateRole(Role role);

    /**
     * 根据ID删除角色
     *
     * @param roleId 角色Id
     * @return 删除数量
     */
    int deleteById(Integer roleId);

}
