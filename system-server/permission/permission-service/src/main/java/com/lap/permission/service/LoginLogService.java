/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.entity.LoginLog;
import com.lap.permission.dto.query.LoginLogQuery;

import java.util.List;

/**
 * 登录日志业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface LoginLogService {
    /**
     * 分页查询登录日志
     *
     * @param query 查询条件,继承分页信息
     * @return 登录日志集合
     */
    PageDTO<List<LoginLog>> queryPage(LoginLogQuery query);

    /**
     * 保存登录日志
     *
     * @param loginLog 登录日志
     * @return 登录日志Id
     */
    Integer insertLoginLog(LoginLog loginLog);

}
