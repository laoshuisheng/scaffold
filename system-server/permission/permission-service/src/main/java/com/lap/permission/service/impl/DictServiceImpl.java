/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.cachekey.DictKey;
import com.lap.permission.dal.DictDAO;
import com.lap.permission.dto.entity.Dict;
import com.lap.permission.dto.entity.DictDetail;
import com.lap.permission.dto.query.DictQuery;
import com.lap.permission.manager.DictManager;
import com.lap.permission.service.DictService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DictServiceImpl implements DictService {

    private final DictDAO dictDAO;

    private final DictManager dictManager;

    @Override
    public PageDTO<List<Dict>> queryPage(DictQuery query) {
        long count = dictDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(dictDAO.queryPage(query), count);
    }

    @Override
    public Dict queryById(Integer dictId) {
        return dictDAO.queryById(dictId);
    }

    @Override
    public Dict queryByCode(String code) {
        return dictDAO.queryByCode(code);
    }

    @CacheEvict(value = {DictKey.DETAIL_CODES, DictKey.DETAIL_CODE}, allEntries = true)
    @Override
    public Integer insertDict(Dict dict, List<DictDetail> dataList) {
        return dictManager.insertDict(dict, dataList);
    }

    @Override
    public int updateDict(Dict dict) {
        return dictDAO.updateDict(dict);
    }

    @CacheEvict(value = {DictKey.DETAIL_CODES, DictKey.DETAIL_CODE}, allEntries = true)
    @Override
    public int deleteById(Integer dictId) {
        return dictManager.deleteById(dictId);
    }

}
