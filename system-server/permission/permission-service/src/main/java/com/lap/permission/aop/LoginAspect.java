package com.lap.permission.aop;

import com.lap.framework.entity.Device;
import com.lap.framework.exception.BizException;
import com.lap.framework.holder.MessageHolder;
import com.lap.framework.tools.ClientIpUtil;
import com.lap.framework.tools.DeviceUtil;
import com.lap.permission.component.RequestHelper;
import com.lap.permission.dto.request.LoginRequest;
import com.lap.permission.event.entity.LoginLogEvent;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 登录日志 aop
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Slf4j
@Component
@Aspect
public class LoginAspect {

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    protected MessageHolder messageHolder;

    @Pointcut("@annotation(com.lap.permission.annotation.LoginLog)")
    public void loginPointcut() {
    }

    @Around("loginPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Object result = null;
        String remark = "success";
        Integer flag = 1;
        try {
            result = joinPoint.proceed();
            stopWatch.stop();
        } catch (Throwable e) {
            remark = getRemark(e);
            flag = 0;
            throw e;
        } finally {
            long times = stopWatch.getTime(TimeUnit.MILLISECONDS);
            String username = getUsername(joinPoint.getArgs());
            publishLoginLog(times, username, flag, remark);
        }
        return result;
    }

    private String getRemark(Throwable e) {
        return (e instanceof BizException biz)
                ? messageHolder.getMsg(biz.getCode())
                : e.getLocalizedMessage();
    }

    private String getUsername(Object[] arg) {
        if (ArrayUtils.isNotEmpty(arg)) {
            if (arg[0] instanceof LoginRequest log) {
                return log.getUsername();
            }
        }
        return null;
    }

    private void publishLoginLog(long times, String username, Integer statusFlag, String remark) {
        HttpServletRequest request = RequestHelper.getRequest();
        Device device = DeviceUtil.getDevice(request);
        String ip = ClientIpUtil.getClientIp(request);

        LoginLogEvent event = new LoginLogEvent();
        event.setIp(ip);
        event.setUsername(username);
        event.setDevice(device);
        event.setStatusFlag(statusFlag);
        event.setTimes(times);
        event.setRemark(remark);
        applicationContext.publishEvent(event);
    }

}
