/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.dto.response.PageDTO;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.framework.validator.Update;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.MsgTemplate;
import com.lap.permission.dto.query.MsgTemplateQuery;
import com.lap.permission.dto.request.MsgTemplateRequest;
import com.lap.permission.service.MsgTemplateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <pre>
 *  消息模版 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see MsgTemplateService
 * @see MsgTemplateRequest
 * @see MsgTemplate
 */
@Tag(name = "消息模版")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/msg-templates")
public class MsgTemplateController extends BaseController {

    private final MsgTemplateService msgTemplateService;

    private final Convert convert;

    @Operation(summary = "消息模版列表")
    @GetMapping
    public PageDTO<List<MsgTemplate>> queryPage(MsgTemplateQuery query) {
        return msgTemplateService.queryPage(query);
    }

    @Operation(summary = "消息模版列表")
    @GetMapping("code")
    public MsgTemplate queryByCode(String code) {
        return msgTemplateService.queryByCode(code);
    }

    @Operation(summary = "消息模版新增")
    @PostMapping
    public Integer saveMsgTemplate(@RequestBody @Valid MsgTemplateRequest request) {
        MsgTemplate query = msgTemplateService.queryByCode(request.getCode());
        if (Objects.nonNull(query))
            ExceptionHandler.execInterrupt("10121");

        return msgTemplateService.insertMsgTemplate(convert.toMsgTemplate(request));
    }

    @Operation(summary = "消息模版更新")
    @PutMapping
    public Integer updateMsgTemplate(@RequestBody @Validated(Update.class) MsgTemplateRequest request) {
        MsgTemplate query = msgTemplateService.queryByCode(request.getCode());
        if (Objects.nonNull(query) && !Objects.equals(query.getId(), request.getId()))
            ExceptionHandler.execInterrupt("10121");

        return msgTemplateService.updateMsgTemplate(convert.toMsgTemplate(request));
    }

    @Operation(summary = "消息模版删除")
    @DeleteMapping("{msgTemplateId}")
    public Integer deleteById(@PathVariable Integer msgTemplateId) {
        return msgTemplateService.deleteById(msgTemplateId);
    }

}
