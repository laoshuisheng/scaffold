package com.lap.permission.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("token")
public class TokenProperties {
    /**
     * token有效时间(天)
     */
    private int times;
    /**
     * redis有效期(分)
     */
    private int redisTimes;
    /**
     * 密钥Key
     */
    private String key;
}
