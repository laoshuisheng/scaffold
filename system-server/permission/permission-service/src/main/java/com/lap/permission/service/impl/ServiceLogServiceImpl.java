/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.ServiceLogDAO;
import com.lap.permission.dto.entity.ServiceLog;
import com.lap.permission.dto.query.ServiceLogQuery;
import com.lap.permission.service.ServiceLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ServiceLogServiceImpl implements ServiceLogService {

    private final ServiceLogDAO serviceLogDAO;

    @Override
    public PageDTO<List<ServiceLog>> queryPage(ServiceLogQuery query) {
        long count = serviceLogDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(serviceLogDAO.queryPage(query), count);
    }

    @Override
    public ServiceLog queryById(Integer serviceLogId) {
        return serviceLogDAO.queryById(serviceLogId);
    }

    @Override
    public Integer insertServiceLog(ServiceLog serviceLog) {
        serviceLogDAO.insertServiceLog(serviceLog);
        return serviceLog.getId();
    }

}
