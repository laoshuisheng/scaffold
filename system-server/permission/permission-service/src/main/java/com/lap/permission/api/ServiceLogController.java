/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.ServiceLog;
import com.lap.permission.dto.query.ServiceLogQuery;
import com.lap.permission.dto.request.ServiceLogRequest;
import com.lap.permission.service.ServiceLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <pre>
 *  服务访问 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see ServiceLogService
 * @see ServiceLogRequest
 * @see ServiceLog
 */
@Tag(name = "服务访问")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/service-logs")
public class ServiceLogController extends BaseController {

    private final ServiceLogService serviceLogService;

    private final Convert convert;

    @Operation(summary = "服务访问列表")
    @GetMapping
    public PageDTO<List<ServiceLog>> queryPage(ServiceLogQuery query) {
        return serviceLogService.queryPage(query);
    }

    @Operation(summary = "服务访问新增")
    @PostMapping
    public Integer saveServiceLog(@RequestBody @Valid ServiceLogRequest request) {
        return serviceLogService.insertServiceLog(convert.toServiceLog(request));
    }

}
