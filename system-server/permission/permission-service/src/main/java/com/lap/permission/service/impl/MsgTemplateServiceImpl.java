/*
* Copyright 2024 The JA-SIG Collaborative. All rights reserved.
* distributed with thi file and available online at
*/
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.MsgTemplateDAO;
import com.lap.permission.dto.entity.MsgTemplate;
import com.lap.permission.dto.query.MsgTemplateQuery;
import com.lap.permission.service.MsgTemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MsgTemplateServiceImpl implements MsgTemplateService {

    private final MsgTemplateDAO msgTemplateDAO;

    @Override
	public PageDTO<List<MsgTemplate>> queryPage(MsgTemplateQuery query) {
		long count = msgTemplateDAO.countPage(query);
		if (count == 0) {
			return Page.ok(count);
		}
		return Page.ok(msgTemplateDAO.queryPage(query), count);
	}

	@Override
	public MsgTemplate queryById(Integer msgTemplateId) {
		return msgTemplateDAO.queryById(msgTemplateId);
	}

	@Override
	public MsgTemplate queryByCode(String code) {
		return msgTemplateDAO.queryByCode(code);
	}

	@Override
	public Integer insertMsgTemplate(MsgTemplate msgTemplate) {
		msgTemplateDAO.insertMsgTemplate(msgTemplate);
		return msgTemplate.getId();
	}

	@Override
	public int updateMsgTemplate(MsgTemplate msgTemplate) {
		return msgTemplateDAO.updateMsgTemplate(msgTemplate);
	}

	@Override
	public int deleteById(Integer msgTemplateId) {
		return msgTemplateDAO.deleteById(msgTemplateId);
	}

}
