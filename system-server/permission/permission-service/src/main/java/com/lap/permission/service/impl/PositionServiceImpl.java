/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.PositionDAO;
import com.lap.permission.dto.entity.Position;
import com.lap.permission.dto.query.PositionQuery;
import com.lap.permission.service.PositionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PositionServiceImpl implements PositionService {

    private final PositionDAO positionDAO;

    @Override
    public PageDTO<List<Position>> queryPage(PositionQuery query) {
        long count = positionDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(positionDAO.queryPage(query), count);
    }

    @Override
    public Position queryById(Integer positionId) {
        return positionDAO.queryById(positionId);
    }

    @Override
    public Position queryByName(String name) {
        return positionDAO.queryByName(name);
    }

    @Override
    public Integer insertPosition(Position position) {
        positionDAO.insertPosition(position);
        return position.getId();
    }

    @Override
    public int updatePosition(Position position) {
        return positionDAO.updatePosition(position);
    }

    @Override
    public int deleteById(Integer positionId) {
        return positionDAO.deleteById(positionId);
    }

}
