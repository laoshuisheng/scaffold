package com.lap.permission.component;

import com.lap.framework.tools.Uid;
import com.lap.permission.dto.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JwtTokenHelper {
    public static final String USER_ID = "id";

    @Value("${security.jwt.key}")
    private String key;

    @Value("${security.jwt.ttl}")
    private int ttl;

    @Value("${security.jwt.issuer}")
    private String issuer;

    public String createJwt(User user) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, ttl);

        return Jwts.builder().id(Uid.getId())
                .issuer(issuer)//签发人
                .subject("sub")//主题
                .claims(Map.of(USER_ID, user.getId()))
                .issuedAt(new Date())//签发时间
                .expiration(calendar.getTime())//过期时间
                .signWith(getSignInKey())
                .compact();
    }

    public Integer validateJwt(String token) {
        try {
            Claims claims = Jwts.parser()
                    .verifyWith(getSignInKey())
                    .build()
                    .parseSignedClaims(token)
                    .getPayload();

            return claims.get(USER_ID, Integer.class);
        } catch (Exception e) {
            throw e;
        }
    }

    private SecretKey getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(key);
        return Keys.hmacShaKeyFor(keyBytes);
    }

}
