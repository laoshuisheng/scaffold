package com.lap.permission.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * rest 配置
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Slf4j
@Configuration
public class RestTemplateConfig {

    @Bean("normalHttpFactory")
    ClientHttpRequestFactory normalHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(15000);
        factory.setReadTimeout(5000);
        return factory;
    }

    @Bean
    RestTemplate restTemplate(@Qualifier("normalHttpFactory") ClientHttpRequestFactory factory) {
        log.info("normal factory::{}", factory);
        return new RestTemplate(factory);
    }

    @Bean("ipHttpRequestFactory")
    ClientHttpRequestFactory ipHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(3000);
        factory.setReadTimeout(3000);
        return factory;
    }

    @Bean("ipRestTemplate")
    RestTemplate ipRestTemplate(@Qualifier("ipHttpRequestFactory") ClientHttpRequestFactory factory) {
        log.info("ip factory::{}", factory);
        return new RestTemplate(factory);
    }

}
