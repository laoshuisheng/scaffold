/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.Region;
import com.lap.permission.dto.response.RegionDto;

import java.util.List;

/**
 * 区域业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface RegionService {
    /**
     * 查询区域树
     *
     * @return 区域集合
     */
    List<RegionDto> queryTree();

    /**
     * 根据ID查询区域
     *
     * @param regionId 区域Id
     * @return 区域
     */
    Region queryById(Integer regionId);

    /**
     * 根据编码查询区域
     *
     * @param name 编码
     * @return 区域
     */
    Region queryByName(String name);

    /**
     * 根据id集合查询区域
     *
     * @param list id集合
     * @return 区域集合
     */
    List<Region> queryByIds(List<Integer> list);

    /**
     * 保存区域
     *
     * @param region 区域
     * @return 区域Id
     */
    Integer insertRegion(Region region);

    /**
     * 根据ID更新区域
     *
     * @param region 区域
     * @return 更新数量
     */
    int updateRegion(Region region);

    /**
     * 根据ID删除区域
     *
     * @param regionId 区域Id
     * @return 删除数量
     */
    int deleteById(Integer regionId);
}
