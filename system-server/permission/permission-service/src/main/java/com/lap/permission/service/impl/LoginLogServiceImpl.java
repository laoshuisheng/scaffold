/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.LoginLogDAO;
import com.lap.permission.dto.entity.LoginLog;
import com.lap.permission.dto.query.LoginLogQuery;
import com.lap.permission.service.LoginLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class LoginLogServiceImpl implements LoginLogService {

    private final LoginLogDAO loginLogDAO;

    @Override
    public PageDTO<List<LoginLog>> queryPage(LoginLogQuery query) {
        long count = loginLogDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(loginLogDAO.queryPage(query), count);
    }

    @Override
    public Integer insertLoginLog(LoginLog loginLog) {
        loginLogDAO.insertLoginLog(loginLog);
        return loginLog.getId();
    }

}
