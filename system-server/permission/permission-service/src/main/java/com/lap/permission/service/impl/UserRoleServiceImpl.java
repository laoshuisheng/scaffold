/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.permission.cachekey.MenuKey;
import com.lap.permission.cachekey.ResourceKey;
import com.lap.permission.cachekey.UserKey;
import com.lap.permission.dal.UserRoleDAO;
import com.lap.permission.dto.entity.UserRole;
import com.lap.permission.manager.UserRoleManager;
import com.lap.permission.service.UserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleManager userRoleManager;

    private final UserRoleDAO userRoleDAO;

    @Override
    public List<Integer> queryByUserId(Integer userId) {
        return userRoleDAO.queryByUserId(userId);
    }

    @CacheEvict(value = {UserKey.USER_INFO, MenuKey.MENU_TREE_USER, ResourceKey.RESOURCE_USER}, allEntries = true)
    @Override
    public Integer insertUserRole(Integer userId, List<UserRole> list) {
        return userRoleManager.insertUserRole(userId, list);
    }

}
