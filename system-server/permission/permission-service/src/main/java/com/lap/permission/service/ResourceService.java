/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.entity.Resource;

import java.util.List;

/**
 * 资源业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface ResourceService {
    /**
     * 分页查询资源
     *
     * @return 资源集合
     */
    List<Resource> queryTree();

    /**
     * 根据ID查询资源
     *
     * @param resourceId 资源Id
     * @return 资源
     */
    Resource queryById(Integer resourceId);

    /**
     * 根据模块查询资源
     *
     * @param module 模块
     * @return 资源集合
     */
    List<Resource> queryByModule(String module);

    /**
     * 根据用户ID查询资源
     *
     * @param userId 用户id
     * @return 资源集合
     */
    List<Resource> queryByUserId(Integer userId);

    /**
     * 根据服务类型查询资源
     *
     * @param type 服务类型
     * @return 资源集合
     */
    List<Resource> queryByService(Integer type);

    /**
     * 根据菜单类型获取资源
     *
     * @param menuId 菜单ID
     * @return 资源集合
     */
    List<Resource> queryByMenuId(Integer menuId);

    /**
     * 批量保存资源
     *
     * @param module    模块
     * @param resources 资源集合
     * @return 资源id
     */
    Integer insertResourceBatch(String module, List<Resource> resources);

    /**
     * 根据ID更新资源
     *
     * @param resource 资源
     * @return 更新数量
     */
    int updateResource(Resource resource);

}
