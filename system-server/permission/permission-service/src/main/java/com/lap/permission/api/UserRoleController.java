/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.permission.dto.entity.UserRole;
import com.lap.permission.dto.request.UserRoleRequest;
import com.lap.permission.service.UserRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <pre>
 *  管理员和角色关系 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see UserRoleService
 * @see UserRoleRequest
 * @see UserRole
 */
@Tag(name = "管理员和角色关系")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/user-roles")
public class UserRoleController extends BaseController {

    private final UserRoleService userRoleService;

    @Operation(summary = "管理员和角色关系获取")
    @GetMapping("{userId}")
    public List<Integer> queryByUserId(@PathVariable Integer userId) {
        return userRoleService.queryByUserId(userId);
    }

    @Operation(summary = "管理员和角色关系新增")
    @PostMapping
    public Integer saveUserRole(@RequestBody @Valid UserRoleRequest request) {
        Integer userId = request.getUserId();

        List<UserRole> dataList = request.getRoleList().stream()
                .map(roleId -> new UserRole(request.getUserId(), roleId))
                .toList();

        return userRoleService.insertUserRole(userId, dataList);
    }

}
