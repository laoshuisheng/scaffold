/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dto.query.MsgTemplateQuery;
import com.lap.permission.dto.entity.MsgTemplate;

import java.util.List;

/**
 * 消息模版业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface MsgTemplateService {
    /**
     * 分页查询消息模版
     *
     * @param query 查询条件,继承分页信息
     * @return 消息模版集合
     */
    PageDTO<List<MsgTemplate>> queryPage(MsgTemplateQuery query);

    /**
     * 根据ID查询消息模版
     *
     * @param msgTemplateId 消息模版Id
     * @return 消息模版
     */
    MsgTemplate queryById(Integer msgTemplateId);

    /**
     * 根据编码查询消息模版
     *
     * @param code 编码
     * @return 消息模版
     */
    MsgTemplate queryByCode(String code);

    /**
     * 保存消息模版
     *
     * @param msgTemplate 消息模版
     * @return 消息模版Id
     */
    Integer insertMsgTemplate(MsgTemplate msgTemplate);

    /**
     * 根据ID更新消息模版
     *
     * @param msgTemplate 消息模版
     * @return 更新数量
     */
    int updateMsgTemplate(MsgTemplate msgTemplate);

    /**
     * 根据ID删除消息模版
     *
     * @param msgTemplateId 消息模版Id
     * @return 删除数量
     */
    int deleteById(Integer msgTemplateId);

}
