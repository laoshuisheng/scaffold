/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.github.promeg.pinyinhelper.Pinyin;
import com.lap.framework.controller.BaseController;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.framework.validator.Update;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.Region;
import com.lap.permission.dto.request.RegionRequest;
import com.lap.permission.dto.response.RegionDto;
import com.lap.permission.service.RegionService;
import com.lap.permission.tools.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * <pre>
 *  区域 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see RegionService
 * @see RegionRequest
 * @see Region
 */
@Tag(name = "区域")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/regions")
public class RegionController extends BaseController {

    private final RegionService regionService;

    private final Convert convert;

    @Operation(summary = "区域树")
    @GetMapping
    public List<RegionDto> queryTree() {
        return regionService.queryTree();
    }

    @Operation(summary = "区域渲染")
    @PostMapping("list")
    public List<Region> queryByIds(@RequestBody List<Integer> list) {
        if (CollectionUtils.isEmpty(list))
            return Collections.emptyList();

        List<Integer> ids = list.stream().distinct().sorted().toList();
        return regionService.queryByIds(ids);
    }

    @Operation(summary = "区域新增")
    @Repeat
    @PostMapping
    public Integer saveRegion(@RequestBody @Valid RegionRequest request) {
        Region query = regionService.queryByName(request.getName());
        if (Objects.nonNull(query))
            ExceptionHandler.execInterrupt("10181");

        Region region = convert.toRegion(request);
        region.setCode(Pinyin.toPinyin(request.getName(), Constants.BLANK));
        return regionService.insertRegion(region);
    }

    @Operation(summary = "区域更新")
    @Repeat
    @PutMapping
    public Integer updateRegion(@RequestBody @Validated(Update.class) RegionRequest request) {
        Region query = regionService.queryByName(request.getName());
        if (Objects.nonNull(query) && !Objects.equals(query.getId(), request.getId()))
            ExceptionHandler.execInterrupt("10181");

        Region region = convert.toRegion(request);
        region.setCode(Pinyin.toPinyin(request.getName(), Constants.BLANK));
        return regionService.updateRegion(region);
    }

    @Operation(summary = "区域删除")
    @Repeat
    @DeleteMapping("{regionId}")
    public Integer deleteById(@PathVariable Integer regionId) {
        return regionService.deleteById(regionId);
    }

}
