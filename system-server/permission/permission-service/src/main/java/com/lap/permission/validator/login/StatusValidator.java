package com.lap.permission.validator.login;

import com.lap.framework.enums.YesNo;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.request.LoginRequest;
import com.lap.permission.validator.LoginValidator;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(3)
@Component
public class StatusValidator implements LoginValidator {

    @Override
    public void validate(LoginRequest request, User user) {
        if (YesNo.NO.getValue().equals(user.getStatusFlag()))
            ExceptionHandler.execInterrupt("10104");
    }

}
