/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service.impl;

import com.lap.framework.dto.response.Page;
import com.lap.framework.dto.response.PageDTO;
import com.lap.permission.dal.AccessLogDAO;
import com.lap.permission.dto.entity.AccessLog;
import com.lap.permission.dto.query.AccessLogQuery;
import com.lap.permission.service.AccessLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AccessLogServiceImpl implements AccessLogService {

    private final AccessLogDAO accessLogDAO;

    @Override
    public PageDTO<List<AccessLog>> queryPage(AccessLogQuery query) {
        long count = accessLogDAO.countPage(query);
        if (count == 0) {
            return Page.ok(count);
        }
        return Page.ok(accessLogDAO.queryPage(query), count);
    }

    @Override
    public Integer insertAccessLog(AccessLog accessLog) {
        accessLogDAO.insertAccessLog(accessLog);
        return accessLog.getId();
    }

    @Override
    public int deleteById(Integer accessLogId) {
        return accessLogDAO.deleteById(accessLogId);
    }

}
