/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.api;

import com.lap.framework.controller.BaseController;
import com.lap.framework.dto.response.PageDTO;
import com.lap.framework.exception.ExceptionHandler;
import com.lap.framework.validator.Update;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.convert.Convert;
import com.lap.permission.dto.entity.Dict;
import com.lap.permission.dto.entity.DictDetail;
import com.lap.permission.dto.query.DictQuery;
import com.lap.permission.dto.request.DictRequest;
import com.lap.permission.enums.DictEnum;
import com.lap.permission.service.DictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * <pre>
 *  数字字典 Controller,控制业务流程。
 *  提供包括业务流程校验,业务流程控制。当流程非常复杂时候,请在API和Service之间添加一层Manager。
 *  Controller 对外开放Rest,同时可以做简单流程控制。Manager复杂流程控制。Service数据完整性(事务)。DAO/Mapper持久操作。
 *  流程简单: Controller->Service->DAO
 *  复杂流程: Controller->Manager->Service->DAO
 *  对外流程: Controller->Manager->Service->DAO
 * 					    |->外部服务
 *  <b>uri 规则</b>
 *   uri 名词复数.
 *   列表或分页 : GET /persons
 *   单人信息   : GET /persons/1
 *   新增      : POST /persons
 *   修改      : PUT /persons
 *   删除      : DELETE /persons/1
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 * @see DictService
 * @see DictRequest
 * @see Dict
 */
@Tag(name = "数字字典")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/dicts")
public class DictController extends BaseController {

    private final DictService dictService;

    private final Convert convert;

    @Operation(summary = "数字字典列表")
    @GetMapping
    public PageDTO<List<Dict>> queryPage(DictQuery query) {
        return dictService.queryPage(query);
    }

    @Operation(summary = "数字字典新增")
    @Repeat
    @PostMapping
    public Integer saveDict(@RequestBody @Valid DictRequest request) {
        Dict query = dictService.queryByCode(request.getCode());
        if (Objects.nonNull(query))
            ExceptionHandler.execInterrupt("10021");

        List<DictDetail> detailList = DictEnum.getDictList(request.getRemark());
        return dictService.insertDict(convert.toDict(request), detailList);
    }

    @Operation(summary = "数字字典更新")
    @Repeat
    @PutMapping
    public Integer updateDict(@RequestBody @Validated(Update.class) DictRequest request) {
        Dict query = dictService.queryByCode(request.getCode());
        if (Objects.nonNull(query) && !Objects.equals(query.getId(), request.getId()))
            ExceptionHandler.execInterrupt("10021");

        return dictService.updateDict(convert.toDict(request));
    }

    @Operation(summary = "数字字典删除")
    @Repeat
    @DeleteMapping("{dictId}")
    public Integer deleteById(@PathVariable Integer dictId) {
        return dictService.deleteById(dictId);
    }

}
