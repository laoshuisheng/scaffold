/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.MenuResource;

import java.util.List;

/**
 * 菜单和资源关系业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface MenuResourceService {
    /**
     * 根据ID查询菜单和资源关系
     *
     * @param menuId 菜单Id
     * @return 菜单和资源关系
     */
    List<String> queryByMenuId(Integer menuId);

    /**
     * 保存菜单和资源关系
     *
     * @param menuId   菜单id
     * @param dataList 菜单和资源关系集合
     * @return 菜单和资源关系Id
     */
    Integer insertMenuResource(Integer menuId, List<MenuResource> dataList);

}
