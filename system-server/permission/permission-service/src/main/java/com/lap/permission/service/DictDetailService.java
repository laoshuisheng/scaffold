/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.service;

import com.lap.permission.dto.entity.DictDetail;

import java.util.List;
import java.util.Map;

/**
 * 数字字典明细业务代码,流程控制和业务流程主要还是在Api上写.
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface DictDetailService {

    /**
     * 根据字典编码和字典明细值查询字典明细
     *
     * @param dictCode    字典编码
     * @param detailValue 字典明细值
     * @param lang        语言
     * @return 数字字典明细
     */
    DictDetail queryByDictCode(String dictCode, Integer detailValue, String lang);

    /**
     * 根据数字字典编码集合查询字典明细
     *
     * @param dictCodes 数字字典编码集合
     * @param lang      语言
     * @return 数字字典明细
     */
    Map<String, List<DictDetail>> queryByDictCodes(List<String> dictCodes, String lang);

    /**
     * 根据ID查询数字字典明细
     *
     * @param dictId 字典id
     * @return 数字字典明细
     */
    List<DictDetail> queryByDictId(Integer dictId);

    /**
     * 保存数字字典明细
     *
     * @param dictId   字典id
     * @param dataList 数字字典明细
     * @return 数字字典明细Id
     */
    Integer insertDictDetail(Integer dictId, List<DictDetail> dataList);

}
