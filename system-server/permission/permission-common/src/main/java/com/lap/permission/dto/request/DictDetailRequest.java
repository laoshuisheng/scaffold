/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 数字字典明细接收参数，提供服务接口模型
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "数字字典明细请求")
public class DictDetailRequest {

    @NotNull(message = "{DictDetail.DictId.NotNull.message}")
    @Min(value = 0, message = "{DictDetail.DictId.Min.message}")
    @Max(value = 2147483647L, message = "{DictDetail.DictId.Max.message}")
    @Schema(description = "字典ID", type = "Integer", example = "5801")
    private Integer dictId;

    @NotBlank(message = "{DictDetail.DictCode.NotNull.message}")
    @Length(max = 64, message = "{DictDetail.DictCode.Length.message}")
    @Schema(description = "字典编码", type = "String", example = "ENywsQmLwv")
    private String dictCode;

    @Valid
    @NotNull(message = "{DictDetail.DataList.NotNull.message}")
    @Size(min = 1, message = "{DictDetail.DataList.Size.message}")
    @Schema(description = "通用字段")
    private List<DetailRequest> dataList;

    @Data
    @ToString
    public static class DetailRequest {

        @NotNull(message = "{DictDetail.DetailValue.NotNull.message}")
        @Min(value = 0, message = "{DictDetail.DetailValue.Min.message}")
        @Max(value = 2147483647L, message = "{DictDetail.DetailValue.Max.message}")
        @Schema(description = "明细值", type = "Integer", example = "1")
        private Integer detailValue;

        @NotBlank(message = "{DictDetail.DetailTitle.NotNull.message}")
        @Length(max = 32, message = "{DictDetail.DetailTitle.Length.message}")
        @Schema(description = "明细标题", type = "String", example = "是")
        private String detailTitle;

        @NotNull(message = "{DictDetail.StatusFlag.NotNull.message}")
        @Min(value = -128, message = "{DictDetail.StatusFlag.Min.message}")
        @Max(value = 127, message = "{DictDetail.StatusFlag.Max.message}")
        @Schema(description = "状态", type = "Integer", example = "1")
        private Integer statusFlag;

        @NotBlank(message = "{DictDetail.Local.NotNull.message}")
        @Length(max = 8, message = "{DictDetail.Local.Length.message}")
        @Schema(description = "语言", type = "String", example = "zh-cn")
        private String local;

        @NotNull(message = "{DictDetail.Orders.NotNull.message}")
        @Min(value = -128, message = "{DictDetail.Orders.Min.message}")
        @Max(value = 127, message = "{DictDetail.Orders.Max.message}")
        @Schema(description = "排序", type = "Integer", example = "1")
        private Integer orders;

    }

}
