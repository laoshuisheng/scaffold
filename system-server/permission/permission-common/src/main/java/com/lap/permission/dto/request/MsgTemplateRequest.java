/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  消息模版接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "消息模版请求")
public class MsgTemplateRequest {

    @NotNull(message = "{MsgTemplate.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{MsgTemplate.Id.Min.message}")
    @Max(value = 2147483647L, message = "{MsgTemplate.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "1")
    private Integer id;

    @NotBlank(message = "{MsgTemplate.Code.NotNull.message}")
    @Length(max = 128, message = "{MsgTemplate.Code.Length.message}")
    @Schema(description = "模版编码", type = "String", example = "c001")
    private String code;

    @NotBlank(message = "{MsgTemplate.Name.NotNull.message}")
    @Length(max = 64, message = "{MsgTemplate.Name.Length.message}")
    @Schema(description = "模版名称", type = "String", example = "测试模版")
    private String name;

    @NotBlank(message = "{MsgTemplate.Title.NotNull.message}")
    @Length(max = 256, message = "{MsgTemplate.Title.Length.message}")
    @Schema(description = "模版主题", type = "String", example = "测试模版")
    private String title;

    @NotBlank(message = "{MsgTemplate.Content.NotNull.message}")
    @Length(max = 429496729, message = "{MsgTemplate.Content.Length.message}")
    @Schema(description = "模版内容", type = "String", example = "测试模版")
    private String content;

    @NotNull(message = "{MsgTemplate.ModuleFlag.NotNull.message}")
    @Min(value = -128, message = "{MsgTemplate.ModuleFlag.Min.message}")
    @Max(value = 127, message = "{MsgTemplate.ModuleFlag.Max.message}")
    @Schema(description = "模板类型", type = "Integer", example = "1")
    private Integer moduleFlag;

}
