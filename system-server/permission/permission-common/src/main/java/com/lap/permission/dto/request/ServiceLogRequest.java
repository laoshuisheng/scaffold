/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * <pre>
 *  服务访问接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "服务访问请求")
public class ServiceLogRequest {

    @NotNull(message = "{ServiceLog.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{ServiceLog.Id.Min.message}")
    @Max(value = 2147483647L, message = "{ServiceLog.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotNull(message = "{ServiceLog.UserId.NotNull.message}")
    @Min(value = 0, message = "{ServiceLog.UserId.Min.message}")
    @Max(value = 2147483647L, message = "{ServiceLog.UserId.Max.message}")
    @Schema(description = "管理员ID", type = "Integer", example = "5801")
    private Integer userId;

    @NotBlank(message = "{ServiceLog.Username.NotNull.message}")
    @Length(max = 32, message = "{ServiceLog.Username.Length.message}")
    @Schema(description = "用户名", type = "String", example = "ENywsQmLwv")
    private String username;

    @NotBlank(message = "{ServiceLog.FullName.NotNull.message}")
    @Length(max = 16, message = "{ServiceLog.FullName.Length.message}")
    @Schema(description = "姓名", type = "String", example = "ENywsQmLwv")
    private String fullName;

    @Length(max = 64, message = "{ServiceLog.TargetServer.Length.message}")
    @Schema(description = "访问实例", type = "String", example = "ENywsQmLwv")
    private String targetServer;

    @Length(max = 64, message = "{ServiceLog.Schema.Length.message}")
    @Schema(description = "访问协议", type = "String", example = "ENywsQmLwv")
    private String schema;

    @NotBlank(message = "{ServiceLog.RequestPath.NotNull.message}")
    @Length(max = 128, message = "{ServiceLog.RequestPath.Length.message}")
    @Schema(description = "访问地址", type = "String", example = "ENywsQmLwv")
    private String requestPath;

    @NotBlank(message = "{ServiceLog.RequestMethod.NotNull.message}")
    @Length(max = 32, message = "{ServiceLog.RequestMethod.Length.message}")
    @Schema(description = "访问方法", type = "String", example = "ENywsQmLwv")
    private String requestMethod;

    @Length(max = 64, message = "{ServiceLog.Browser.Length.message}")
    @Schema(description = "访问者浏览器", type = "String", example = "ENywsQmLwv")
    private String browser;

    @NotBlank(message = "{ServiceLog.ClientOs.NotNull.message}")
    @Length(max = 32, message = "{ServiceLog.ClientOs.Length.message}")
    @Schema(description = "访问者操作系统", type = "String", example = "ENywsQmLwv")
    private String clientOs;

    @NotBlank(message = "{ServiceLog.RequestIp.NotNull.message}")
    @Length(max = 16777215, message = "{ServiceLog.RequestIp.Length.message}")
    @Schema(description = "登录地址", type = "String", example = "ENywsQmLwv")
    private String requestIp;

    @NotBlank(message = "{ServiceLog.RequestAddress.NotNull.message}")
    @Length(max = 32, message = "{ServiceLog.RequestAddress.Length.message}")
    @Schema(description = "访问来源地点", type = "String", example = "ENywsQmLwv")
    private String requestAddress;

    @NotBlank(message = "{ServiceLog.RequestBody.NotNull.message}")
    @Length(max = 294967295, message = "{ServiceLog.RequestBody.Length.message}")
    @Schema(description = "访问入参", type = "String", example = "ENywsQmLwv")
    private String requestBody;

    @Schema(description = "请求开始时间", type = "Date", example = "2024-06-12")
    private Date requestTime;

    @Schema(description = "请求结束时间", type = "Date", example = "2024-06-12")
    private Date responseTime;

    @NotBlank(message = "{ServiceLog.ResponseData.NotNull.message}")
    @Length(max = 294967295, message = "{ServiceLog.ResponseData.Length.message}")
    @Schema(description = "访问返回值", type = "String", example = "ENywsQmLwv")
    private String responseData;

    @NotNull(message = "{ServiceLog.Times.NotNull.message}")
    @Schema(description = "访问耗时", type = "Long", example = "6165")
    private Long times;

}
