/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * <pre>
 *  角色和菜单关系接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "角色和菜单关系请求")
public class RoleMenuRequest {

    @NotNull(message = "{RoleMenu.RoleId.NotNull.message}")
    @Min(value = 0, message = "{RoleMenu.RoleId.Min.message}")
    @Max(value = 2147483647L, message = "{RoleMenu.RoleId.Max.message}")
    @Schema(description = "角色ID", type = "Integer", example = "1")
    private Integer roleId;

    @Schema(description = "半选集合")
    private List<Integer> halfList;

    @Schema(description = "全选集合")
    private List<Integer> checkList;

}
