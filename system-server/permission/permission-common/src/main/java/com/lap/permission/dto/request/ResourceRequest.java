/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  资源接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "资源请求")
public class ResourceRequest {

    @NotNull(message = "{Resource.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Resource.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Resource.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotBlank(message = "{Resource.Module.NotNull.message}")
    @Length(max = 32, message = "{Resource.Module.Length.message}")
    @Schema(description = "资源模块", type = "String", example = "ENywsQmLwv")
    private String module;

    @Length(max = 256, message = "{Resource.ParentCode.Length.message}")
    @Schema(description = "父资源编码", type = "String", example = "ENywsQmLwv")
    private String parentCode;

    @NotBlank(message = "{Resource.Code.NotNull.message}")
    @Length(max = 256, message = "{Resource.Code.Length.message}")
    @Schema(description = "资源编码", type = "String", example = "ENywsQmLwv")
    private String code;

    @NotBlank(message = "{Resource.Name.NotNull.message}")
    @Length(max = 64, message = "{Resource.Name.Length.message}")
    @Schema(description = "资源名称", type = "String", example = "ENywsQmLwv")
    private String name;

    @NotBlank(message = "{Resource.Uri.NotNull.message}")
    @Length(max = 256, message = "{Resource.Uri.Length.message}")
    @Schema(description = "资源地址", type = "String", example = "ENywsQmLwv")
    private String uri;

    @Length(max = 8, message = "{Resource.Method.Length.message}")
    @Schema(description = "方法类型", type = "String", example = "ENywsQmLwv")
    private String method;

    @NotNull(message = "{Resource.FunctionFlag.NotNull.message}")
    @Min(value = -128, message = "{Resource.FunctionFlag.Min.message}")
    @Max(value = 127, message = "{Resource.FunctionFlag.Max.message}")
    @Schema(description = "功能权限", type = "Integer", example = "00")
    private Integer functionFlag;

    @NotNull(message = "{Resource.DataFlag.NotNull.message}")
    @Min(value = -128, message = "{Resource.DataFlag.Min.message}")
    @Max(value = 127, message = "{Resource.DataFlag.Max.message}")
    @Schema(description = "数据权限", type = "Integer", example = "00")
    private Integer dataFlag;

    @Min(value = -128, message = "{Resource.DesensitizeFlag.Min.message}")
    @Max(value = 127, message = "{Resource.DesensitizeFlag.Max.message}")
    @Schema(description = "数据脱敏", type = "Integer", example = "00")
    private Integer desensitizeFlag;

    @Length(max = 1024, message = "{Resource.DesensitizeJson.Length.message}")
    @Schema(description = "脱敏信息", type = "String", example = "ENywsQmLwv")
    private String desensitizeJson;

    @NotNull(message = "{Resource.TypeFlag.NotNull.message}")
    @Min(value = -128, message = "{Resource.TypeFlag.Min.message}")
    @Max(value = 127, message = "{Resource.TypeFlag.Max.message}")
    @Schema(description = "资源类型", type = "Integer", example = "00")
    private Integer typeFlag;

    @Min(value = -128, message = "{Resource.ServiceFlag.Min.message}")
    @Max(value = 127, message = "{Resource.ServiceFlag.Max.message}")
    @Schema(description = "服务类型", type = "Integer", example = "00")
    private Integer serviceFlag;

}
