package com.lap.permission.dto.response;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "系统管理员返回")
public class UserDto extends BaseEntity {

    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "姓名")
    private String fullName;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "部门ID")
    private Integer departmentId;

    @Schema(description = "部门ID路径")
    private List<Integer> departmentPaths;

    @Schema(description = "岗位ID")
    private Integer positionId;

    @Schema(description = "状态")
    private Integer statusFlag;

    @Schema(description = "状态描述")
    private String statusRemark;

    @Schema(description = "有效期")
    private Date termValidity;

    @Schema(description = "管理员类型")
    private Integer typeFlag;

    @Schema(description = "职位")
    private PositionDto position;

}
