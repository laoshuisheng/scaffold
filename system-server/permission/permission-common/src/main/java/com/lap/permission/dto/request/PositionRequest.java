/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  岗位接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "岗位请求")
public class PositionRequest {

    @NotNull(message = "{Position.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Position.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Position.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "1")
    private Integer id;

    @NotBlank(message = "{Position.Name.NotNull.message}")
    @Length(max = 32, message = "{Position.Name.Length.message}")
    @Schema(description = "名称", type = "String", example = "ENywsQmLwv")
    private String name;

    @NotNull(message = "{Position.StatusFlag.NotNull.message}")
    @Min(value = -128, message = "{Position.StatusFlag.Min.message}")
    @Max(value = 127, message = "{Position.StatusFlag.Max.message}")
    @Schema(description = "状态", type = "Integer", example = "00")
    private Integer statusFlag;

    @Length(max = 64, message = "{Position.Remark.Length.message}")
    @Schema(description = "备注", type = "String", example = "ENywsQmLwv")
    private String remark;

}
