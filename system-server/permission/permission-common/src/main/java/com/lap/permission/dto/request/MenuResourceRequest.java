/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * 菜单和资源关系接收参数，提供服务接口模型
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "菜单和资源关系请求")
public class MenuResourceRequest {

    @NotNull(message = "{MenuResource.MenuId.NotNull.message}")
    @Min(value = -2147483648L, message = "{MenuResource.MenuId.Min.message}")
    @Max(value = 2147483647L, message = "{MenuResource.MenuId.Max.message}")
    @Schema(description = "菜单ID", type = "Integer", example = "5801")
    private Integer menuId;

    @Schema(description = "全选资源编码集合")
    private List<String> halfList;

    @Schema(description = "半选资源编码集合")
    private List<String> checkList;

}
