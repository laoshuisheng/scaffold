package com.lap.permission.enums;

import lombok.Getter;

@Getter
public enum MenuType {
    //菜单类型:0-目录,1-菜单,2-功能
    DIR(0, "目录"),
    MENU(1, "菜单"),
    FUN(2, "功能"),
    ;

    private final Integer value;

    private final String label;

    MenuType(Integer value, String label) {
        this.value = value;
        this.label = label;
    }

}
