/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.query;

import com.lap.framework.dto.request.PageRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * <pre>
 *  消息模版 查询条件,专门提供查询使用，请勿当成提交数据保存使用，
 *	如果有必要新增额外领域模型数据，可以单独创建，请勿使用查询对象做提交数据保存
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(name = "消息模版查询条件")
public class MsgTemplateQuery extends PageRequest {

    @Schema(description = "模板类型")
    private List<Integer> moduleFlags;

    @Schema(description = "模版编码")
    private String code;

    @Schema(description = "模版名称")
    private String name;

}
