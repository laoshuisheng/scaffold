package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@Schema(description = "资源上报请求")
public class ResourceUploadRequest {

    @NotBlank(message = "{ResourceUpload.Module.NotNull.message}")
    @Length(max = 32, message = "{ResourceUpload.Module.Length.message}")
    @Schema(description = "资源模块", type = "String", example = "permission")
    private String module;

    @NotBlank(message = "{ResourceUpload.Name.NotNull.message}")
    @Length(max = 64, message = "{ResourceUpload.Name.Length.message}")
    @Schema(description = "资源名称", type = "String", example = "权限模块")
    private String name;

    @Min(value = -128, message = "{ResourceUpload.ServiceFlag.Min.message}")
    @Max(value = 127, message = "{ResourceUpload.ServiceFlag.Max.message}")
    @Schema(description = "服务类型", type = "Integer", example = "0")
    private Integer serviceFlag;

}
