/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.query;

import com.lap.framework.dto.request.PageRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  服务访问 查询条件,专门提供查询使用，请勿当成提交数据保存使用，
 * 	如果有必要新增额外领域模型数据，可以单独创建，请勿使用查询对象做提交数据保存
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "服务访问查询条件")
public class ServiceLogQuery extends PageRequest {

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "姓名")
    private String fullName;

}
