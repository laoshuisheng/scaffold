/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  角色接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "角色请求")
public class RoleRequest {

    @NotNull(message = "{Role.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Role.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Role.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotNull(message = "{Role.ModuleFlag.NotNull.message}")
    @Min(value = -128, message = "{Role.ModuleFlag.Min.message}")
    @Max(value = 127, message = "{Role.ModuleFlag.Max.message}")
    @Schema(description = "模块类型", type = "Integer", example = "00")
    private Integer moduleFlag;

    @NotBlank(message = "{Role.Name.NotNull.message}")
    @Length(max = 32, message = "{Role.Name.Length.message}")
    @Schema(description = "名字", type = "String", example = "角色名称")
    private String name;

    @Length(max = 64, message = "{Role.Remark.Length.message}")
    @Schema(description = "备注", type = "String", example = "角色备注")
    private String remark;

    @NotNull(message = "{Role.StatusFlag.NotNull.message}")
    @Min(value = -128, message = "{Role.StatusFlag.Min.message}")
    @Max(value = 127, message = "{Role.StatusFlag.Max.message}")
    @Schema(description = "状态", type = "Integer", example = "0")
    private Integer statusFlag;

}
