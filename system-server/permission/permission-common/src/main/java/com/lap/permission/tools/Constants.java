package com.lap.permission.tools;

/**
 * 权限常量
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public final class Constants {

    public static final String COLON = ":";

    public static final String REGULAR = "[;,；，:：]";

    public static final String TEST = "[-=]";

    public static final String ZH_CN = "zh-cn";

    public static final String BLANK = "";

}
