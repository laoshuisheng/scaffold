package com.lap.permission.dto.request;

import com.lap.framework.entity.Resource;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "资源远程上报请求")
public class ResourceRemoteRequest extends ResourceUploadRequest {

    private List<Resource> resources;

}
