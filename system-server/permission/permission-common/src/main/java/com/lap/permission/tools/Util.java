package com.lap.permission.tools;

import org.apache.commons.lang3.StringUtils;

public final class Util {

    public static String getDefault(String param, String defaultValue) {
        return StringUtils.isBlank(param) ? defaultValue : param;
    }

}
