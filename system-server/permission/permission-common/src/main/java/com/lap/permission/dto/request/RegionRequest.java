/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * <pre>
 *  区域接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "区域请求")
public class RegionRequest {

    @NotNull(message = "{Region.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Region.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Region.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "1")
    private Integer id;

    @Schema(description = "父路径", type = "String", example = "[1,2]")
    private List<Integer> pidPaths;

    @NotBlank(message = "{Region.Name.NotNull.message}")
    @Length(max = 256, message = "{Region.Name.Length.message}")
    @Schema(description = "区域名称", type = "String", example = "广东省")
    private String name;

    @NotNull(message = "{Region.Orders.NotNull.message}")
    @Min(value = -128, message = "{Region.Orders.Min.message}")
    @Max(value = 127, message = "{Region.Orders.Max.message}")
    @Schema(description = "排序", type = "Integer", example = "1")
    private Integer orders;

}
