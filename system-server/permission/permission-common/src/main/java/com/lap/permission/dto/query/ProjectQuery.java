/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.query;

import com.lap.framework.dto.request.PageRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  项目配置 查询条件,专门提供查询使用，请勿当成提交数据保存使用，
 * 	如果有必要新增额外领域模型数据，可以单独创建，请勿使用查询对象做提交数据保存
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "项目配置查询条件")
public class ProjectQuery extends PageRequest {

    @Schema(description = "项目上下文")
    private String context;

    @Schema(description = "项目名称")
    private String name;

    @Schema(description = "状态")
    private Integer statusFlag;

}
