/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  数字字典接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "数字字典请求")
public class DictRequest {

    @NotNull(message = "{Dict.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Dict.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Dict.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @Min(value = -2147483648L, message = "{Dict.ModuleFlag.Min.message}")
    @Max(value = 2147483647L, message = "{Dict.ModuleFlag.Max.message}")
    @Schema(description = "模块标识", type = "Integer", example = "5801")
    private Integer moduleFlag;

    @NotBlank(message = "{Dict.Code.NotNull.message}")
    @Length(max = 64, message = "{Dict.Code.Length.message}")
    @Schema(description = "编码", type = "String", example = "ENywsQmLwv")
    private String code;

    @Length(max = 32, message = "{Dict.Name.Length.message}")
    @Schema(description = "字典名称", type = "String", example = "ENywsQmLwv")
    private String name;

    @Length(max = 512, message = "{Dict.Remark.Length.message}")
    @Schema(description = "备注", type = "String", example = "ENywsQmLwv")
    private String remark;

}
