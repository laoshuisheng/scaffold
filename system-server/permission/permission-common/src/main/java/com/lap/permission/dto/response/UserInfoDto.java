package com.lap.permission.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "登录信息返回")
public class UserInfoDto implements Serializable {

    @Schema(description = "用户ID")
    private Integer userId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "用户头像")
    private String photo;

    @Schema(description = "登录成功时间")
    private Date time;

    @Schema(description = "角色")
    private List<RoleDto> roles;

    @Schema(description = "功能权限")
    private List<String> authBtnList;

}
