package com.lap.permission.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Schema(description = "Token返回")
public class TokenDto {

    @Schema(description = "访问token")
    private String token;

}
