/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validate.annotation.Dict;
import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * <pre>
 *  部门接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "部门请求")
public class DepartmentRequest {

    @NotNull(message = "{Department.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Department.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Department.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "1")
    private Integer id;

    @Schema(description = "父ID路径", example = "[5801,9]")
    private List<Integer> pidPaths;

    @Length(max = 64, message = "{Department.Code.Length.message}")
    @Schema(description = "编码", type = "String", example = "JS0001")
    private String code;

    @NotBlank(message = "{Department.Name.NotNull.message}")
    @Length(max = 32, message = "{Department.Name.Length.message}")
    @Schema(description = "名称", type = "String", example = "技术部门")
    private String name;

    @NotNull(message = "{Department.StatusFlag.NotNull.message}")
    @Min(value = -128, message = "{Department.StatusFlag.Min.message}")
    @Max(value = 127, message = "{Department.StatusFlag.Max.message}")
    @Dict(code = "dict.commons.yesNo")
    @Schema(description = "状态", type = "Integer", example = "1")
    private Integer statusFlag;

    @Length(max = 64, message = "{Department.Remark.Length.message}")
    @Schema(description = "备注", type = "String", example = "技术部门")
    private String remark;

    @NotNull(message = "{Department.Orders.NotNull.message}")
    @Min(value = -10_000_000, message = "{Department.Orders.Min.message}")
    @Max(value = 10_000_000, message = "{Department.Orders.Max.message}")
    @Schema(description = "排序", type = "Integer", example = "1")
    private Integer orders;

}
