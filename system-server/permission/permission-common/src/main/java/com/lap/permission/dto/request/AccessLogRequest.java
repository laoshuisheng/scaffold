/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  访问日志接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "访问日志请求")
public class AccessLogRequest {

    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotNull(message = "{AccessLog.UserId.NotNull.message}")
    @Min(value = 0, message = "{AccessLog.UserId.Min.message}")
    @Max(value = 2147483647L, message = "{AccessLog.UserId.Max.message}")
    @Schema(description = "管理员ID", type = "Integer", example = "1")
    private Integer userId;

    @NotBlank(message = "{AccessLog.Username.NotNull.message}")
    @Length(max = 32, message = "{AccessLog.Username.Length.message}")
    @Schema(description = "登录账号", type = "String", example = "admin")
    private String username;

    @Length(max = 16, message = "{AccessLog.FullName.Length.message}")
    @Schema(description = "姓名", type = "String", example = "张三")
    private String fullName;

    @Length(max = 256, message = "{AccessLog.AccessUri.Length.message}")
    @Schema(description = "访问地址", type = "String", example = "/permission/api/menus/me")
    private String accessUri;

    @Length(max = 16, message = "{AccessLog.AccessMethod.Length.message}")
    @Schema(description = "访问方法", type = "String", example = "post")
    private String accessMethod;

    @Length(max = 256, message = "{AccessLog.DeviceInfo.Length.message}")
    @Schema(description = "访问浏览器", type = "String", example = "ie 5")
    private String deviceInfo;

    @Length(max = 256, message = "{AccessLog.SysInfo.Length.message}")
    @Schema(description = "访问系统", type = "String", example = "Macbook")
    private String sysInfo;

    @Length(max = 256, message = "{AccessLog.AccessSign.Length.message}")
    @Schema(description = "访问来源地址", type = "String", example = "192.168.1.1")
    private String accessSign;

    @Length(max = 128, message = "{AccessLog.AccessAddress.Length.message}")
    @Schema(description = "访问来源地点", type = "String", example = "中国广东东莞")
    private String accessAddress;

    @Length(max = 2048, message = "{AccessLog.AccessParam.Length.message}")
    @Schema(description = "访问入参", type = "String", example = "{}")
    private String accessParam;

    @Length(max = 65535, message = "{AccessLog.AccessResult.Length.message}")
    @Schema(description = "访问返回值", type = "String", example = "{code:200,message:操作成功,data:[]}")
    private String accessResult;

    @Schema(description = "访问耗时", type = "Long", example = "6165")
    private Long times;

}
