package com.lap.permission.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * <pre>
 * 局域网网段区间判断
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public enum IpRound {
    ONE("10", 0, 255),
    TWO("172", 16, 31),
    THREE("192", 168, 168),
    ;

    private final String key;

    private final Integer min;

    private final Integer max;

    IpRound(String key, Integer min, Integer max) {
        this.key = key;
        this.min = min;
        this.max = max;
    }

    public static boolean validIp(String ip) {
        if (StringUtils.isBlank(ip)) {
            return true;
        }

        String[] ips = StringUtils.split(ip, "\\.");
        if (ips.length != 4) {
            return true;
        }

        for (IpRound ipRound : IpRound.values()) {
            if (ipRound.key.equals(ips[0])
                    && Integer.valueOf(ips[1]) >= ipRound.min
                    && Integer.valueOf(ips[1]) <= ipRound.max)
                return true;
        }
        return false;
    }

}
