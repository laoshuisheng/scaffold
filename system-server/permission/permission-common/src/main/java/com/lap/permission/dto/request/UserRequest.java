/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Add;
import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

/**
 * <pre>
 *  系统管理员接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "系统管理员请求")
public class UserRequest {

    @NotNull(message = "{User.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{User.Id.Min.message}")
    @Max(value = 2147483647L, message = "{User.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotBlank(message = "{User.Username.NotNull.message}")
    @Length(max = 32, message = "{User.Username.Length.message}")
    @Schema(description = "用户名", type = "String", example = "test")
    private String username;

    @NotBlank(message = "{User.Password.NotNull.message}", groups = {Add.class})
    @Length(max = 64, message = "{User.Password.Length.message}")
    @Schema(description = "密码", type = "String", example = "test")
    private String password;

    @NotBlank(message = "{User.FullName.NotNull.message}")
    @Length(max = 16, message = "{User.FullName.Length.message}")
    @Schema(description = "姓名", type = "String", example = "劳水生")
    private String fullName;

    @NotBlank(message = "{User.Email.NotNull.message}")
    @Length(max = 64, message = "{User.Email.Length.message}")
    @Email(message = "{User.Email.message}")
    @Schema(description = "邮箱", type = "String", example = "laoshuisheng@163.com")
    private String email;

    @Schema(description = "部门路径集合")
    private List<Integer> departmentPaths;

    @Min(value = 0, message = "{User.PositionId.Min.message}")
    @Max(value = 2147483647L, message = "{User.PositionId.Max.message}")
    @Schema(description = "岗位ID", type = "Integer", example = "1")
    private Integer positionId;

    @NotNull(message = "{User.StatusFlag.NotNull.message}")
    @Min(value = -128, message = "{User.StatusFlag.Min.message}")
    @Max(value = 127, message = "{User.StatusFlag.Max.message}")
    @Schema(description = "状态", type = "Integer", example = "1")
    private Integer statusFlag;

    @Length(max = 128, message = "{User.StatusRemark.Length.message}")
    @Schema(description = "状态描述", type = "String", example = "用户备注")
    private String statusRemark;

    @Future(message = "{User.TermValidity.Future.message}")
    @Schema(description = "有效期", type = "Date", example = "2024-06-12")
    private Date termValidity;

}
