/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * <pre>
 *  项目配置接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "项目配置请求")
public class ProjectRequest {

    @NotNull(message = "{Project.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Project.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Project.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "5801")
    private Integer id;

    @NotBlank(message = "{Project.Context.NotNull.message}")
    @Length(max = 64, message = "{Project.Context.Length.message}")
    @Schema(description = "项目上下文", type = "String", example = "permission")
    private String context;

    @NotBlank(message = "{Project.Name.NotNull.message}")
    @Length(max = 64, message = "{Project.Name.Length.message}")
    @Schema(description = "项目名称", type = "String", example = "权限")
    private String name;

    @NotNull(message = "{Project.TypeFlag.NotNull.message}")
    @Min(value = -128, message = "{Project.TypeFlag.Min.message}")
    @Max(value = 127, message = "{Project.TypeFlag.Max.message}")
    @Schema(description = "服务类型", type = "Integer", example = "0")
    private Integer typeFlag;

    @NotNull(message = "{Project.StatusFlag.NotNull.message}")
    @Min(value = -128, message = "{Project.StatusFlag.Min.message}")
    @Max(value = 127, message = "{Project.StatusFlag.Max.message}")
    @Schema(description = "状态", type = "Integer", example = "0")
    private Integer statusFlag;

}
