/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * 列表头接收参数，提供服务接口模型
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "列表头请求")
public class HeaderRequest {

    @NotNull(message = "{Header.Code.NotNull.message}", groups = {Update.class})
    @Length(max = 128, message = "{Header.Code.Length.message}")
    @Schema(description = "头编码", type = "String", example = "UserList")
    private String code;

    @Length(max = 2047, message = "{Header.Content.Length.message}")
    @Schema(description = "头内容", type = "String", example = "username,fullName")
    private String content;

}
