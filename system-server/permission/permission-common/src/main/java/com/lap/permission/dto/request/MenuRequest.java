/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import com.lap.framework.validator.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 菜单接收参数，提供服务接口模型
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "菜单请求")
public class MenuRequest {

    @NotNull(message = "{Menu.Id.NotNull.message}", groups = {Update.class})
    @Min(value = 0, message = "{Menu.Id.Min.message}")
    @Max(value = 2147483647L, message = "{Menu.Id.Max.message}")
    @Schema(description = "主键", type = "Integer", example = "1")
    private Integer id;

    @Schema(description = "父ID路径", example = "[5801,9]")
    private List<Integer> pidPaths;

    @NotNull(message = "{Menu.TypeFlag.NotNull.message}")
    @Min(value = -128, message = "{Menu.TypeFlag.Min.message}")
    @Max(value = 127, message = "{Menu.TypeFlag.Max.message}")
    @Schema(description = "菜单类型", type = "Integer", example = "0")
    private Integer typeFlag;

    @Length(max = 128, message = "{Menu.Path.Length.message}")
    @Schema(description = "菜单路径", type = "String", example = "/system/dept")
    private String path;

    @NotBlank(message = "{Menu.Name.NotNull.message}")
    @Length(max = 64, message = "{Menu.Name.Length.message}")
    @Schema(description = "菜单名称", type = "String", example = "系统管理")
    private String name;

    @Length(max = 128, message = "{Menu.Component.Length.message}")
    @Schema(description = "菜单组件路径", type = "String", example = "system/menu/index")
    private String component;

    @Length(max = 256, message = "{Menu.Redirect.Length.message}")
    @Schema(description = "重新定向路径", type = "String", example = "/home")
    private String redirect;

    @Length(max = 128, message = "{Menu.Link.Length.message}")
    @Schema(description = "外链/内链地址", type = "String", example = "https://www.baidu.com")
    private String link;

    @Length(max = 128, message = "{Menu.PermCode.Length.message}")
    @Schema(description = "功能权限码", type = "String", example = "permission:user:add")
    private String permCode;

    @NotNull(message = "{Menu.Orders.NotNull.message}")
    @Min(value = -128, message = "{Menu.Orders.Min.message}")
    @Max(value = 127, message = "{Menu.Orders.Max.message}")
    @Schema(description = "排序", type = "Integer", example = "1")
    private Integer orders;

    @Valid
    @Schema(description = "元件")
    private Meta meta;

    @Schema(description = "元件请求")
    public static class Meta {

        @Getter
        @Setter
        @Length(max = 128, message = "{Menu.Title.Length.message}")
        @Schema(description = "国际化编码", type = "String", example = "router.system")
        private String title;

        @Getter
        @Setter
        @Length(max = 256, message = "{Menu.Icon.Length.message}")
        @Schema(description = "图标", type = "String", example = "test:image")
        private String icon;

        @Schema(description = "是否隐藏", type = "Integer", example = "1")
        private boolean isHide;

        @Schema(description = "是否缓存", type = "Integer", example = "1")
        private boolean isKeepAlive;

        @Schema(description = "是否固定tag", type = "Integer", example = "1")
        private boolean isAffix;

        @Schema(description = "是否外链", type = "Integer", example = "1")
        private boolean isIframe;

        public boolean getIsHide() {
            return isHide;
        }

        public void setIsHide(boolean hide) {
            isHide = hide;
        }

        public boolean getIsKeepAlive() {
            return isKeepAlive;
        }

        public void setIsKeepAlive(boolean keepAlive) {
            isKeepAlive = keepAlive;
        }

        public boolean getIsAffix() {
            return isAffix;
        }

        public void setIsAffix(boolean affix) {
            isAffix = affix;
        }

        public boolean getIsIframe() {
            return isIframe;
        }

        public void setIsIframe(boolean iframe) {
            isIframe = iframe;
        }

    }

}
