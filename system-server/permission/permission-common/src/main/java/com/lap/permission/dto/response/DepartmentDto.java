package com.lap.permission.dto.response;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "部门返回")
public class DepartmentDto extends BaseEntity implements Comparable<DepartmentDto> {

    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "上级部门ID")
    private Integer pid;

    @Schema(description = "父菜单路径")
    private List<Integer> pidPaths;

    @Schema(description = "编码")
    private String code;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "状态")
    private Integer statusFlag;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "排序")
    private Integer orders;

    @Schema(description = "子集合")
    private List<DepartmentDto> children;

    @Override
    public int compareTo(DepartmentDto o) {
        if (o != null) {
            return this.orders.compareTo(o.getOrders());
        }
        return 0;
    }

}
