/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * <pre>
 *  管理员和角色关系接收参数，提供服务接口模型
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "管理员和角色关系请求")
public class UserRoleRequest {

    @NotNull(message = "{UserRole.UserId.NotNull.message}")
    @Min(value = 0, message = "{UserRole.UserId.Min.message}")
    @Max(value = 2147483647L, message = "{UserRole.UserId.Max.message}")
    @Schema(description = "管理员ID", type = "Integer", example = "5801")
    private Integer userId;

    @Schema(description = "角色ID")
    private List<Integer> roleList;

}
