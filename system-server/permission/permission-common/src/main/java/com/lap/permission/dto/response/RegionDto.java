package com.lap.permission.dto.response;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@Schema(description = "区域返回")
public class RegionDto extends BaseEntity implements Comparable<RegionDto>, Serializable {

    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "父id")
    private Integer pid;

    @Schema(description = "父路径")
    private List<Integer> pidPaths;

    @Schema(description = "区域编码")
    private String code;

    @Schema(description = "区域名称")
    private String name;

    @Schema(description = "排序")
    private Integer orders;

    @Schema(description = "子集合")
    private List<RegionDto> children;

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int compareTo(RegionDto o) {
        if (o != null) {
            return this.orders.compareTo(o.getOrders());
        }
        return 0;
    }

}
