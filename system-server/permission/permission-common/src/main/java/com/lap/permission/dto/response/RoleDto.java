/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.response;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "角色返回")
public class RoleDto extends BaseEntity implements Serializable {

    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "名字")
    private String name;

    @Schema(description = "模块类型")
    private Integer moduleFlag;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "状态")
    private Integer statusFlag;

}
