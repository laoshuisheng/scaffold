package com.lap.permission.dto.response;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单返回
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "菜单返回")
public class MenuDto extends BaseEntity implements Comparable<MenuDto>, Serializable {

    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "父ID")
    private Integer pid;

    @Schema(description = "父菜单路径")
    private List<Integer> pidPaths;

    @Schema(description = "菜单类型")
    private Integer typeFlag;

    @Schema(description = "菜单路径")
    private String path;

    @Schema(description = "菜单名称")
    private String name;

    @Schema(description = "菜单组件路径")
    private String component;

    @Schema(description = "外链")
    private String link;

    @Schema(description = "权限码[功能]")
    private String permCode;

    @Schema(description = "重新定向路径")
    private String redirect;

    @Schema(description = "排序")
    private Integer orders;

    @Schema(description = "元件")
    private Meta meta;

    @Schema(description = "子集合")
    private List<MenuDto> children;

    @Schema(description = "元件")
    public static class Meta implements Serializable {

        @Getter
        @Setter
        @Schema(description = "国际化编码")
        private String title;

        @Getter
        @Setter
        @Schema(description = "图标")
        private String icon;

        @Schema(description = "外链")
        private boolean isLink;

        @Schema(description = "是否隐藏")
        private boolean isHide;

        @Schema(description = "是否缓存")
        private boolean isKeepAlive;

        @Schema(description = "是否固定tag")
        private boolean isAffix;

        @Schema(description = "是否外链")
        private boolean isIframe;

        public boolean getIsLink() {
            return isLink;
        }

        public void setIsLink(boolean isLink) {
            this.isLink = isLink;
        }

        public boolean getIsHide() {
            return isHide;
        }

        public void setIsHide(boolean hide) {
            isHide = hide;
        }

        public boolean getIsKeepAlive() {
            return isKeepAlive;
        }

        public void setIsKeepAlive(boolean keepAlive) {
            isKeepAlive = keepAlive;
        }

        public boolean getIsAffix() {
            return isAffix;
        }

        public void setIsAffix(boolean affix) {
            isAffix = affix;
        }

        public boolean getIsIframe() {
            return isIframe;
        }

        public void setIsIframe(boolean iframe) {
            isIframe = iframe;
        }

    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int compareTo(MenuDto o) {
        if (o != null) {
            return this.orders.compareTo(o.getOrders());
        }
        return 0;
    }

}
