package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * 用户状态请求
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "管理员状态请求")
public class UserStateRequest {

    @NotNull(message = "{UserState.UserId.NotNull.message}")
    @Min(value = 0, message = "{UserState.UserId.Min.message}")
    @Max(value = 2147483647L, message = "{UserState.UserId.Max.message}")
    @Schema(description = "用户Id")
    private Integer id;

    @Length(max = 128, message = "{UserState.StatusRemark.Length.message}")
    @Schema(description = "启禁备注")
    private String statusRemark;

}
