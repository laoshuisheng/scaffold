package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@Schema(description = "登录请求")
public class LoginRequest {

    @NotBlank(message = "{Login.Username.NotNull.message}")
    @Length(max = 32, message = "{Login.Username.Length.message}")
    @Schema(description = "用户名", type = "String", example = "admin")
    private String username;

    @NotBlank(message = "{Login.Password.NotNull.message}")
    @Length(max = 64, message = "{Login.Password.Length.message}")
    @Schema(description = "密码", type = "String", example = "admin123")
    private String password;

}
