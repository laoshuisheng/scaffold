-- MySQL dump 10.13  Distrib 8.0.38, for macos14 (arm64)
--
-- Host: localhost    Database: permission
-- ------------------------------------------------------
-- Server version	8.4.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_access_log`
--

DROP TABLE IF EXISTS `sys_access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_access_log` (
                                  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `user_id` int unsigned NOT NULL COMMENT '管理员ID',
                                  `username` varchar(32) NOT NULL COMMENT '登录账号',
                                  `full_name` varchar(16) DEFAULT NULL COMMENT '姓名',
                                  `access_uri` varchar(256) DEFAULT NULL COMMENT '访问地址',
                                  `access_method` varchar(16) DEFAULT NULL COMMENT '访问方法',
                                  `device_info` varchar(256) DEFAULT NULL COMMENT '访问浏览器',
                                  `sys_info` varchar(256) DEFAULT NULL COMMENT '访问系统',
                                  `access_sign` varchar(256) DEFAULT NULL COMMENT '访问来源地址',
                                  `access_address` varchar(128) DEFAULT NULL COMMENT '访问来源地点',
                                  `access_param` varchar(2048) DEFAULT NULL COMMENT '访问入参',
                                  `access_result` text COMMENT '访问返回值',
                                  `times` bigint DEFAULT '0' COMMENT '访问耗时',
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '访问时间',
                                  PRIMARY KEY (`id`),
                                  KEY `idx_username` (`username`),
                                  KEY `idx_full_name` (`full_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='访问日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_access_log`
--

LOCK TABLES `sys_access_log` WRITE;
/*!40000 ALTER TABLE `sys_access_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_department`
--

DROP TABLE IF EXISTS `sys_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_department` (
                                  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `pid` int unsigned NOT NULL COMMENT '上级部门ID',
                                  `pid_path` varchar(256) DEFAULT NULL COMMENT 'pid路径',
                                  `code` varchar(64) DEFAULT NULL COMMENT '编码',
                                  `name` varchar(32) NOT NULL COMMENT '名称',
                                  `status_flag` tinyint NOT NULL COMMENT '状态:0-禁用,1-启用',
                                  `remark` varchar(64) DEFAULT NULL COMMENT '备注',
                                  `orders` int NOT NULL DEFAULT '0' COMMENT '排序',
                                  `created_by` varchar(32) NOT NULL COMMENT '创建人',
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                                  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                  PRIMARY KEY (`id`,`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_department`
--

LOCK TABLES `sys_department` WRITE;
/*!40000 ALTER TABLE `sys_department` DISABLE KEYS */;
INSERT INTO `sys_department` VALUES (61,0,'',NULL,'集团',1,'集团',1,'系统管理员','2025-02-16 03:11:04','系统管理员','2025-02-16 03:11:04'),(62,61,'61',NULL,'信息部门',1,'信息部门',1,'系统管理员','2025-02-16 03:11:22','系统管理员','2025-02-16 03:11:22'),(63,62,'61,62',NULL,'测试一部',1,'测试一部',2,'系统管理员','2025-02-16 03:11:49','系统管理员','2025-02-16 03:12:24'),(64,62,'61,62',NULL,'测试二部',1,'测试二部',1,'系统管理员','2025-02-16 03:12:00','系统管理员','2025-02-16 03:12:00'),(65,62,'61,62',NULL,'开发一部',1,'开发一部',3,'系统管理员','2025-02-16 03:12:17','系统管理员','2025-02-16 03:12:17'),(66,62,'61,62',NULL,'开发二部',1,'开发二部',4,'系统管理员','2025-02-16 03:12:40','系统管理员','2025-02-16 03:12:40');
/*!40000 ALTER TABLE `sys_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict` (
                            `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `module_flag` int unsigned DEFAULT '0' COMMENT '模块标识',
                            `code` varchar(64) NOT NULL COMMENT '编码',
                            `name` varchar(32) DEFAULT NULL COMMENT '字典名称',
                            `remark` varchar(512) DEFAULT NULL COMMENT '备注',
                            `created_by` varchar(32) NOT NULL COMMENT '创建人',
                            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                            `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                            PRIMARY KEY (`id`),
                            KEY `idx_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='数字字典';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` VALUES (36,0,'dict.commons.module.typeFlag','所属模块','所属模块:0-通用模块,1-权限模块,2-客户管理','admin','2021-05-01 22:47:18','系统管理员','2025-02-16 10:30:05'),(37,0,'dict.commons.yesNo','通用是否','通用是否:0-否,1-是','admin','2021-05-03 20:37:30','系统管理员','2025-02-16 10:52:57'),(39,0,'dict.commons.statusFlag','通用状态','通用状态:0-禁用,1-启用','admin','2021-05-03 20:41:02','系统管理员','2025-02-16 11:09:25'),(41,1,'dict.permission.successFlag','登录状态','登录状态:0-失败,1-成功','admin','2021-09-29 22:12:56','系统管理员','2023-04-12 21:17:40'),(72,1,'dict.permission.project.type','项目类型','项目类型:0-对内服务,1-对外服务','系统管理员','2025-02-16 10:49:40','系统管理员','2025-02-16 10:49:40');
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_detail`
--

DROP TABLE IF EXISTS `sys_dict_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_detail` (
                                   `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                   `dict_id` int unsigned NOT NULL COMMENT '字典ID',
                                   `dict_code` varchar(64) NOT NULL COMMENT '字典编码',
                                   `detail_value` int NOT NULL COMMENT '明细值',
                                   `detail_title` varchar(32) NOT NULL COMMENT '明细标题',
                                   `status_flag` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '状态',
                                   `local` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'zh-cn' COMMENT '语言',
                                   `orders` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '排序',
                                   PRIMARY KEY (`id`),
                                   KEY `udx_dict_code` (`dict_code`,`orders`),
                                   KEY `udx_dict_id_orders` (`dict_id`,`orders`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='数字字典明细';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_detail`
--

LOCK TABLES `sys_dict_detail` WRITE;
/*!40000 ALTER TABLE `sys_dict_detail` DISABLE KEYS */;
INSERT INTO `sys_dict_detail` VALUES (381,72,'dict.permission.project.type',0,'对内服务',1,'zh-cn',1),(382,72,'dict.permission.project.type',0,'In',1,'en',1),(383,72,'dict.permission.project.type',1,'对外服务',1,'zh-cn',2),(384,72,'dict.permission.project.type',1,'Out',1,'en',2),(389,41,'dict.permission.successFlag',0,'失败',1,'zh-cn',1),(390,41,'dict.permission.successFlag',0,'Failure',1,'en',1),(391,41,'dict.permission.successFlag',1,'成功',1,'zh-cn',2),(392,41,'dict.permission.successFlag',1,'Success',1,'en',2),(393,39,'dict.commons.statusFlag',0,'禁用',1,'zh-cn',1),(394,39,'dict.commons.statusFlag',0,'Disabled',1,'en',1),(395,39,'dict.commons.statusFlag',1,'启用',1,'zh-cn',2),(396,39,'dict.commons.statusFlag',1,'Enable',1,'en',2),(397,37,'dict.commons.yesNo',0,'否',1,'zh-cn',1),(398,37,'dict.commons.yesNo',0,'No',1,'en',1),(399,37,'dict.commons.yesNo',1,'是',1,'zh-cn',2),(400,37,'dict.commons.yesNo',1,'Yes',1,'en',2),(401,36,'dict.commons.module.typeFlag',0,'通用模块',1,'zh-cn',1),(402,36,'dict.commons.module.typeFlag',0,'Normal',1,'en',1),(403,36,'dict.commons.module.typeFlag',1,'权限模块',1,'zh-cn',2),(404,36,'dict.commons.module.typeFlag',1,'Permission',1,'en',2),(405,36,'dict.commons.module.typeFlag',2,'进销存模块',1,'zh-cn',3),(406,36,'dict.commons.module.typeFlag',2,'Inventory Management ',1,'en',3);
/*!40000 ALTER TABLE `sys_dict_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_header`
--

DROP TABLE IF EXISTS `sys_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_header` (
                              `user_id` int NOT NULL COMMENT '管理员ID',
                              `code` varchar(128) NOT NULL COMMENT '头编码',
                              `content` varchar(2047) DEFAULT NULL COMMENT '头内容',
                              PRIMARY KEY (`user_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='列表头';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_header`
--

LOCK TABLES `sys_header` WRITE;
/*!40000 ALTER TABLE `sys_header` DISABLE KEYS */;
INSERT INTO `sys_header` VALUES (1,'DictList','moduleFlag,code,name,remark,createdBy,createTime,updatedBy,updateTime'),(1,'LoginLogList','username,fullName,browser,sysInfo,loginIp,loginAddress,times,successFlag,remark,createTime'),(1,'PostList','name,statusFlag,remark,createdBy,createTime,updatedBy,updateTime'),(1,'ProjectList','context,name,statusFlag,typeFlag,createdBy,createTime,updatedBy,updateTime'),(1,'RoleList','moduleFlag,name,statusFlag,remark,createdBy,createTime,updatedBy,updateTime'),(1,'UserList','username,email,fullName,position.name,departmentPaths,termValidity,statusFlag,statusRemark,createdBy,createTime,updatedBy,updateTime');
/*!40000 ALTER TABLE `sys_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_login_log`
--

DROP TABLE IF EXISTS `sys_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_login_log` (
                                 `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `user_id` int NOT NULL COMMENT '管理员ID',
                                 `username` varchar(32) NOT NULL COMMENT '登录账号',
                                 `full_name` varchar(16) NOT NULL COMMENT '姓名',
                                 `browser` varchar(256) DEFAULT NULL COMMENT '浏览器',
                                 `sys_info` varchar(256) DEFAULT NULL COMMENT '系统信息',
                                 `login_ip` bigint NOT NULL COMMENT '登录地址',
                                 `login_address` varchar(256) DEFAULT NULL COMMENT '登录地点',
                                 `success_flag` tinyint NOT NULL COMMENT '状态:0-失败,1-成功',
                                 `remark` varchar(128) DEFAULT NULL COMMENT '备注',
                                 `times` int NOT NULL DEFAULT '0' COMMENT '耗时,单位毫秒',
                                 `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 PRIMARY KEY (`id`),
                                 KEY `idx_username` (`username`),
                                 KEY `idx_create_time` (`create_time`),
                                 KEY `idx_full_name` (`full_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='登录日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_login_log`
--

LOCK TABLES `sys_login_log` WRITE;
/*!40000 ALTER TABLE `sys_login_log` DISABLE KEYS */;
INSERT INTO `sys_login_log` VALUES (53,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',0,'账号已过期,请联系管理员延期',11,'2025-02-15 21:31:48'),(54,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',0,'账号已过期,请联系管理员延期',5,'2025-02-15 21:31:55'),(55,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',0,'账号已过期,请联系管理员延期',5,'2025-02-15 21:32:00'),(56,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',65,'2025-02-15 21:32:25'),(57,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-15 21:35:44'),(58,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-15 21:35:49'),(59,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',6,'2025-02-15 21:35:53'),(60,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-15 21:36:02'),(61,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-15 21:36:10'),(62,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-15 21:36:22'),(63,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-15 21:38:17'),(64,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-15 21:38:22'),(65,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-15 21:48:38'),(66,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',1,'2025-02-15 21:48:51'),(67,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',1,'2025-02-15 21:50:59'),(68,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-15 21:51:39'),(69,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-15 21:55:08'),(70,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-15 21:58:06'),(71,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-15 21:58:28'),(72,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',44,'2025-02-16 08:37:20'),(73,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-16 09:16:19'),(74,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',4,'2025-02-16 09:17:32'),(75,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-16 09:21:22'),(76,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 09:29:41'),(77,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 09:32:24'),(78,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',5,'2025-02-16 09:44:21'),(79,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 09:51:51'),(80,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 09:55:28'),(81,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',2,'2025-02-16 09:58:27'),(82,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 10:15:15'),(83,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 10:23:03'),(84,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 10:54:38'),(85,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 11:16:53'),(86,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 11:46:10'),(87,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',3,'2025-02-16 11:55:12'),(88,1,'admin','系统管理员','Safari','Mac OS X',2130706433,'本地网(01)',1,'success',9,'2025-02-18 20:19:49');
/*!40000 ALTER TABLE `sys_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
                            `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
                            `pid_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '父ID路径',
                            `type_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '菜单类型:0-目录,1-菜单,2-功能',
                            `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单路径',
                            `name` varchar(64) NOT NULL COMMENT '菜单名称',
                            `component` varchar(128) DEFAULT NULL COMMENT '菜单组件路径',
                            `title` varchar(128) DEFAULT NULL COMMENT '国际化编码',
                            `redirect` varchar(256) DEFAULT NULL COMMENT '重新定向路径',
                            `icon` varchar(256) DEFAULT NULL COMMENT '图标',
                            `hide_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
                            `keep_alive_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '是否缓存',
                            `affix_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '是否固定tag',
                            `iframe_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '是否外链',
                            `link` varchar(128) DEFAULT NULL COMMENT '外链',
                            `perm_code` varchar(128) DEFAULT NULL COMMENT '权限码',
                            `orders` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '排序',
                            `created_by` varchar(32) NOT NULL COMMENT '创建人',
                            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                            `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,0,'',1,'/home','home','home/index','router.home',NULL,'iconfont icon-shouye',0,1,1,0,NULL,NULL,1,'system','2023-08-09 20:37:18','系统管理员','2025-02-16 08:52:17'),(2,0,'',1,'/system','system','layout/routerView/parent','router.system','/system/menu','iconfont icon-xitongshezhi',0,1,0,0,NULL,NULL,95,'system','2023-08-09 22:01:13','系统管理员','2025-02-16 09:12:05'),(3,2,'2',1,'/system/menu','systemMenu','system/menu/index','router.systemMenu',NULL,'iconfont icon-shuxingtu',0,1,0,0,NULL,NULL,1,'system','2023-08-09 22:03:27','系统管理员','2025-02-16 11:24:27'),(4,11,'11',1,'/limits/user','systemUser','system/user/index','router.systemUser',NULL,'iconfont icon-icon-',0,1,0,0,NULL,NULL,1,'system','2023-08-09 22:49:29','系统管理员','2025-02-16 10:02:39'),(8,11,'11',1,'/limits/role','systemRole','system/role/index','router.systemRole',NULL,'ele-ColdDrink',0,1,0,0,NULL,NULL,3,'system','2023-08-12 13:14:46','系统管理员','2025-02-16 10:02:30'),(9,2,'2',1,'/system/dept','systemDept','system/dept/index','router.systemDept',NULL,'ele-OfficeBuilding',0,1,0,0,NULL,NULL,3,'system','2023-08-12 13:17:52','系统管理员','2025-02-16 09:27:52'),(10,2,'2',1,'/system/dic','systemDic','system/dic/index','router.systemDic',NULL,'ele-SetUp',0,1,0,0,NULL,NULL,7,'system','2023-08-12 13:18:44','系统管理员','2025-02-16 11:14:04'),(11,0,'',1,'/limits','limits','layout/routerView/parent','router.limits','/limits/frontEnd','iconfont icon-quanxian',0,1,0,0,NULL,NULL,97,'system','2023-08-12 13:22:53','系统管理员','2025-02-16 09:11:43'),(115,3,'2,3',2,NULL,'菜单列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'menu:list',1,'系统管理员','2025-02-16 08:53:40','系统管理员','2025-02-16 08:53:40'),(116,3,'2,3',2,NULL,'菜单新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:add',3,'系统管理员','2025-02-16 08:54:00','系统管理员','2025-02-16 10:17:32'),(117,133,'2,3,133',2,NULL,'菜单编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:edit',1,'系统管理员','2025-02-16 08:54:20','系统管理员','2025-02-16 09:48:51'),(118,133,'2,3,133',2,NULL,'菜单资源',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:resource',5,'系统管理员','2025-02-16 08:54:55','系统管理员','2025-02-16 09:57:14'),(119,133,'2,3,133',2,NULL,'菜单删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:delete',7,'系统管理员','2025-02-16 08:55:18','系统管理员','2025-02-16 09:57:21'),(120,9,'2,9',2,NULL,'部门列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'dept:list',1,'系统管理员','2025-02-16 09:03:33','系统管理员','2025-02-16 09:03:33'),(121,9,'2,9',2,NULL,'部门新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dept:add',3,'系统管理员','2025-02-16 09:05:46','系统管理员','2025-02-16 10:17:23'),(122,9,'2,9',2,NULL,'部门编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dept:edit',5,'系统管理员','2025-02-16 09:06:02','系统管理员','2025-02-16 09:06:02'),(123,9,'2,9',2,NULL,'部门删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dept:delete',7,'系统管理员','2025-02-16 09:06:23','系统管理员','2025-02-16 09:06:23'),(124,0,'',1,'/developer','developer','layout/routerView/parent','router.developer',NULL,'iconfont icon-crew_feature',0,1,0,0,NULL,NULL,99,'系统管理员','2025-02-16 09:11:20','系统管理员','2025-02-16 09:26:05'),(125,124,'124',1,'/developer/project','project','system/project/index','router.project',NULL,'fa fa-map-signs',0,1,0,0,NULL,NULL,3,'系统管理员','2025-02-16 09:14:44','系统管理员','2025-02-16 09:29:15'),(126,124,'124',1,'/developer/resource','resource','system/resource/index','router.resource',NULL,'fa fa-registered',0,1,0,0,NULL,NULL,5,'系统管理员','2025-02-16 09:24:50','系统管理员','2025-02-16 09:29:21'),(127,2,'2',1,'/system/region','systemRegion','system/region/index','router.systemRegion',NULL,'iconfont icon-ditu',0,1,0,0,NULL,NULL,9,'系统管理员','2025-02-16 09:31:48','系统管理员','2025-02-16 11:13:59'),(128,2,'2',1,'/system/msgTemplate','systemMsgTemplate','system/msgTemplate/index','router.systemMsgTemplate',NULL,'fa fa-pinterest-p',0,1,0,0,NULL,NULL,11,'系统管理员','2025-02-16 09:38:58','系统管理员','2025-02-16 11:13:54'),(129,11,'11',1,'/limits/loginLog','systemLoginLog','system/loginLog/index','router.systemLoginLog',NULL,'iconfont icon-shenqingkaiban',0,1,0,0,NULL,NULL,5,'系统管理员','2025-02-16 09:43:56','系统管理员','2025-02-16 09:56:15'),(130,10,'2,10',2,NULL,'字典列表',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:list',1,'系统管理员','2025-02-16 09:46:14','系统管理员','2025-02-16 09:46:14'),(131,10,'2,10',2,NULL,'字典新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:add',3,'系统管理员','2025-02-16 09:46:44','系统管理员','2025-02-16 09:46:44'),(132,10,'2,10',2,NULL,'字典更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:more',5,'系统管理员','2025-02-16 09:47:18','系统管理员','2025-02-16 09:47:18'),(133,3,'2,3',2,NULL,'菜单更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:more',5,'系统管理员','2025-02-16 09:48:04','系统管理员','2025-02-16 09:48:04'),(134,132,'2,10,132',2,NULL,'字典复制',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:copy',3,'系统管理员','2025-02-16 09:50:40','系统管理员','2025-02-16 10:05:53'),(135,132,'2,10,132',2,NULL,'字典明细',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:detail',5,'系统管理员','2025-02-16 09:51:08','系统管理员','2025-02-16 10:05:40'),(136,132,'2,10,132',2,NULL,'字典删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:delete',7,'系统管理员','2025-02-16 09:51:24','系统管理员','2025-02-16 10:05:33'),(137,127,'2,127',2,NULL,'区域列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'region:list',1,'系统管理员','2025-02-16 09:53:41','系统管理员','2025-02-16 09:53:41'),(138,133,'2,3,133',2,NULL,'菜单复制',NULL,NULL,NULL,'',1,0,0,0,NULL,'menu:copy',3,'系统管理员','2025-02-16 09:57:54','系统管理员','2025-02-16 09:57:54'),(139,127,'2,127',2,NULL,'区域更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'region:more',5,'系统管理员','2025-02-16 09:59:21','系统管理员','2025-02-16 11:53:20'),(140,139,'2,127,139',2,NULL,'区域编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'region:edit',1,'系统管理员','2025-02-16 09:59:54','系统管理员','2025-02-16 11:54:07'),(141,139,'2,127,139',2,NULL,'区域删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'region:delete',7,'系统管理员','2025-02-16 10:00:12','系统管理员','2025-02-16 10:00:12'),(142,128,'2,128',2,NULL,'消息模版列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'msgTemplate:list',1,'系统管理员','2025-02-16 10:01:15','系统管理员','2025-02-16 10:01:15'),(143,128,'2,128',2,NULL,'消息模版编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'msgTemplate:edit',5,'系统管理员','2025-02-16 10:01:37','系统管理员','2025-02-16 10:17:56'),(144,128,'2,128',2,NULL,'消息模版删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'msgTemplate:delete',7,'系统管理员','2025-02-16 10:01:52','系统管理员','2025-02-16 10:17:52'),(145,4,'11,4',2,NULL,'用户列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'user:list',1,'系统管理员','2025-02-16 10:03:47','系统管理员','2025-02-16 10:03:47'),(146,4,'11,4',2,NULL,'用户新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:add',3,'系统管理员','2025-02-16 10:04:12','系统管理员','2025-02-16 10:18:30'),(147,4,'11,4',2,NULL,'用户更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:more',5,'系统管理员','2025-02-16 10:04:27','系统管理员','2025-02-16 10:04:27'),(148,147,'11,4,147',2,NULL,'用户编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:edit',1,'系统管理员','2025-02-16 10:05:06','系统管理员','2025-02-16 10:06:51'),(149,132,'2,10,132',2,NULL,'字典编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'dict:edit',1,'系统管理员','2025-02-16 10:06:10','系统管理员','2025-02-16 10:06:10'),(150,147,'11,4,147',2,NULL,'用户复制',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:copy',3,'系统管理员','2025-02-16 10:07:05','系统管理员','2025-02-16 10:07:05'),(151,147,'11,4,147',2,NULL,'用户状态',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:status',5,'系统管理员','2025-02-16 10:09:22','系统管理员','2025-02-16 10:09:22'),(152,147,'11,4,147',2,NULL,'用户授权',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:role',7,'系统管理员','2025-02-16 10:09:48','系统管理员','2025-02-16 10:09:48'),(153,147,'11,4,147',2,NULL,'用户删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:delete',9,'系统管理员','2025-02-16 10:10:19','系统管理员','2025-02-16 10:10:19'),(154,8,'11,8',2,NULL,'角色列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'role:list',1,'系统管理员','2025-02-16 10:11:53','系统管理员','2025-02-16 10:11:53'),(155,8,'11,8',2,NULL,'角色新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:add',3,'系统管理员','2025-02-16 10:12:14','系统管理员','2025-02-16 10:18:47'),(156,8,'11,8',2,NULL,'角色更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:more',5,'系统管理员','2025-02-16 10:12:27','系统管理员','2025-02-16 10:12:27'),(157,156,'11,8,156',2,NULL,'角色编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:edit',1,'系统管理员','2025-02-16 10:13:04','系统管理员','2025-02-16 10:13:04'),(158,156,'11,8,156',2,NULL,'角色复制',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:copy',3,'系统管理员','2025-02-16 10:13:24','系统管理员','2025-02-16 10:13:24'),(159,156,'11,8,156',2,NULL,'角色授权',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:menu',5,'系统管理员','2025-02-16 10:13:45','系统管理员','2025-02-16 10:13:45'),(160,156,'11,8,156',2,NULL,'角色删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:delete',7,'系统管理员','2025-02-16 10:14:02','系统管理员','2025-02-16 10:14:02'),(161,128,'2,128',2,NULL,'消息模版新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'msgTemplate:add',3,'系统管理员','2025-02-16 10:18:14','系统管理员','2025-02-16 10:18:14'),(162,125,'124,125',2,NULL,'工程列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'project:list',1,'系统管理员','2025-02-16 10:19:36','系统管理员','2025-02-16 10:19:36'),(163,125,'124,125',2,NULL,'工程新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'project:add',3,'系统管理员','2025-02-16 10:20:26','系统管理员','2025-02-16 10:20:26'),(164,125,'124,125',2,NULL,'工程更多',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'project:more',5,'系统管理员','2025-02-16 10:20:46','系统管理员','2025-02-16 10:20:46'),(165,164,'124,125,164',2,NULL,'工程上传',NULL,NULL,NULL,'',1,0,0,0,NULL,'project:upload',1,'系统管理员','2025-02-16 10:21:10','系统管理员','2025-02-16 10:21:10'),(166,164,'124,125,164',2,NULL,'工程删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'project:delete',3,'系统管理员','2025-02-16 10:21:32','系统管理员','2025-02-16 10:58:19'),(167,125,'124,125',2,NULL,'工程编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'project:edit',5,'系统管理员','2025-02-16 10:54:11','系统管理员','2025-02-16 10:58:06'),(168,2,'2',1,'/system/position','systemPosition','system/position/index','router.systemPosition',NULL,'fa fa-ravelry',0,1,0,0,NULL,NULL,5,'系统管理员','2025-02-16 11:15:00','系统管理员','2025-02-16 11:15:00'),(169,168,'2,168',2,NULL,'岗位列表',NULL,NULL,NULL,'',1,0,0,0,NULL,'position:list',1,'系统管理员','2025-02-16 11:15:28','系统管理员','2025-02-16 11:15:28'),(170,168,'2,168',2,NULL,'岗位新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'position:add',3,'系统管理员','2025-02-16 11:15:45','系统管理员','2025-02-16 11:15:45'),(171,168,'2,168',2,NULL,'岗位编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'position:edit',5,'系统管理员','2025-02-16 11:16:17','系统管理员','2025-02-16 11:16:17'),(172,168,'2,168',2,NULL,'岗位删除',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'position:delete',7,'系统管理员','2025-02-16 11:16:30','系统管理员','2025-02-16 11:16:30'),(173,126,'124,126',2,NULL,'资源树',NULL,NULL,NULL,'',1,0,0,0,NULL,'resource:list',1,'系统管理员','2025-02-16 11:45:02','系统管理员','2025-02-16 11:45:02'),(174,126,'124,126',2,NULL,'资源编辑',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'resource:update',3,'系统管理员','2025-02-16 11:45:41','系统管理员','2025-02-16 11:45:41'),(175,127,'2,127',2,NULL,'区域新增',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'region:add',3,'系统管理员','2025-02-16 11:53:34','系统管理员','2025-02-16 11:53:34'),(176,139,'2,127,139',2,NULL,'区域复制',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'region:copy',3,'系统管理员','2025-02-16 11:54:21','系统管理员','2025-02-16 11:54:21'),(177,118,'2,3,133,118',2,NULL,'菜单资源获取',NULL,NULL,NULL,'',1,0,0,0,NULL,'menu:resource:get',1,'系统管理员','2025-02-16 14:10:05','系统管理员','2025-02-16 14:10:05'),(178,118,'2,3,133,118',2,NULL,'菜单资源保存',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'menu:resource:save',3,'系统管理员','2025-02-16 14:10:27','系统管理员','2025-02-16 14:10:27'),(179,152,'11,4,147,152',2,NULL,'用户角色获取',NULL,NULL,NULL,'',1,0,0,0,NULL,'user:role:get',1,'系统管理员','2025-02-16 14:11:54','系统管理员','2025-02-16 14:11:54'),(180,152,'11,4,147,152',2,NULL,'用户角色保存',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'user:role:save',3,'系统管理员','2025-02-16 14:12:24','系统管理员','2025-02-16 14:12:24'),(181,159,'11,8,156,159',2,NULL,'角色菜单获取',NULL,NULL,NULL,'',1,0,0,0,NULL,'role:menu:get',1,'系统管理员','2025-02-16 14:13:00','系统管理员','2025-02-16 14:13:00'),(182,159,'11,8,156,159',2,NULL,'角色菜单保存',NULL,NULL,NULL,NULL,1,0,0,0,NULL,'role:menu:save',3,'系统管理员','2025-02-16 14:13:16','系统管理员','2025-02-16 14:13:16');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu_resource`
--

DROP TABLE IF EXISTS `sys_menu_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu_resource` (
                                     `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                     `menu_id` int unsigned NOT NULL COMMENT '菜单ID',
                                     `uri_code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资源编码',
                                     `type_flag` tinyint unsigned NOT NULL COMMENT '类型:0-半选,1-全选',
                                     PRIMARY KEY (`id`),
                                     KEY `udx_menu_id_uri_code_type_flag` (`menu_id`,`uri_code`,`type_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单和资源关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu_resource`
--

LOCK TABLES `sys_menu_resource` WRITE;
/*!40000 ALTER TABLE `sys_menu_resource` DISABLE KEYS */;
INSERT INTO `sys_menu_resource` VALUES (352,3,'permission_get_api_menus',1),(353,116,'permission_post_api_menus',1),(354,117,'permission_put_api_menus',1),(355,119,'permission_delete_api_menus_id',1);
/*!40000 ALTER TABLE `sys_menu_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_msg_template`
--

DROP TABLE IF EXISTS `sys_msg_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_msg_template` (
                                    `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                                    `module_flag` tinyint NOT NULL DEFAULT '0' COMMENT '模板类型,使用数字字典',
                                    `code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模版编码,不能修改',
                                    `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模版名称',
                                    `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模版主题',
                                    `content` longtext NOT NULL COMMENT '模版内容',
                                    `created_by` varchar(32) NOT NULL COMMENT '创建人',
                                    `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                    `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                                    `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                    PRIMARY KEY (`id`),
                                    KEY `idx_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='消息模版';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_msg_template`
--

LOCK TABLES `sys_msg_template` WRITE;
/*!40000 ALTER TABLE `sys_msg_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_msg_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_position`
--

DROP TABLE IF EXISTS `sys_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_position` (
                                `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `name` varchar(32) NOT NULL COMMENT '名称',
                                `status_flag` tinyint NOT NULL COMMENT '状态:0-禁用,1-启用',
                                `remark` varchar(64) DEFAULT NULL COMMENT '备注',
                                `created_by` varchar(32) NOT NULL COMMENT '创建人',
                                `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                                `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_position`
--

LOCK TABLES `sys_position` WRITE;
/*!40000 ALTER TABLE `sys_position` DISABLE KEYS */;
INSERT INTO `sys_position` VALUES (3,'管理岗位',1,'管理岗位','系统管理员','2025-02-16 03:17:17','系统管理员','2025-02-16 03:17:17'),(4,'普通职工',1,'普通职工','系统管理员','2025-02-16 03:17:30','系统管理员','2025-02-16 03:17:30');
/*!40000 ALTER TABLE `sys_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_project`
--

DROP TABLE IF EXISTS `sys_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_project` (
                               `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                               `name` varchar(64) NOT NULL COMMENT '项目名称',
                               `context` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '项目上下文',
                               `type_flag` tinyint NOT NULL DEFAULT '0' COMMENT '服务类型:0-对内,1-对外',
                               `status_flag` tinyint NOT NULL DEFAULT '0' COMMENT '状态',
                               `created_by` varchar(32) NOT NULL COMMENT '创建人',
                               `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                               `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                               PRIMARY KEY (`id`),
                               KEY `idx_code` (`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='项目配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_project`
--

LOCK TABLES `sys_project` WRITE;
/*!40000 ALTER TABLE `sys_project` DISABLE KEYS */;
INSERT INTO `sys_project` VALUES (4,'权限模块','permission',0,1,'系统管理员','2025-02-16 10:50:46','系统管理员','2025-02-16 11:02:32');
/*!40000 ALTER TABLE `sys_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_region`
--

DROP TABLE IF EXISTS `sys_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_region` (
                              `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                              `pid` int DEFAULT NULL COMMENT '父id',
                              `pid_path` varchar(256) DEFAULT NULL COMMENT '父路径',
                              `code` varchar(32) NOT NULL COMMENT '区域编码',
                              `name` varchar(256) NOT NULL COMMENT '区域名称',
                              `orders` int NOT NULL DEFAULT '1',
                              `created_by` varchar(32) NOT NULL COMMENT '创建人',
                              `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                              `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                              PRIMARY KEY (`id`),
                              KEY `idx_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='区域';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_region`
--

LOCK TABLES `sys_region` WRITE;
/*!40000 ALTER TABLE `sys_region` DISABLE KEYS */;
INSERT INTO `sys_region` VALUES (1,0,'','BEIJINGSHI','北京市',1,'系统管理员','2025-02-16 11:55:27','系统管理员','2025-02-16 11:55:27'),(2,0,'','TIANJINSHI','天津市',2,'系统管理员','2025-02-16 12:44:37','系统管理员','2025-02-16 12:44:37'),(3,0,'','HEBEISHENG','河北省',3,'系统管理员','2025-02-16 12:44:53','系统管理员','2025-02-16 12:44:53'),(4,0,'','SHANXISHENG','山西省',4,'系统管理员','2025-02-16 12:45:03','系统管理员','2025-02-16 12:45:03'),(5,0,'','NEIMENGGUZIZHIQU','内蒙古自治区',5,'系统管理员','2025-02-16 12:45:17','系统管理员','2025-02-16 12:45:17'),(6,0,'','LIAONINGSHENG','辽宁省',6,'系统管理员','2025-02-16 12:45:36','系统管理员','2025-02-16 12:45:36'),(7,0,'','JILINSHENG','吉林省',7,'系统管理员','2025-02-16 12:45:45','系统管理员','2025-02-16 12:45:45'),(8,0,'','HEILONGJIANGSHENG','黑龙江省',8,'系统管理员','2025-02-16 12:45:52','系统管理员','2025-02-16 12:45:52'),(9,0,'','SHANGHAISHI','上海市',9,'系统管理员','2025-02-16 12:46:03','系统管理员','2025-02-16 12:46:03'),(10,0,'','JIANGSUSHENG','江苏省',10,'系统管理员','2025-02-16 12:46:13','系统管理员','2025-02-16 12:46:13'),(11,0,'','ZHEJIANGSHENG','浙江省',11,'系统管理员','2025-02-16 12:47:11','系统管理员','2025-02-16 12:47:11'),(12,0,'','ANHUISHENG','安徽省',12,'系统管理员','2025-02-16 12:47:24','系统管理员','2025-02-16 12:47:24'),(13,0,'','FUJIANSHENG','福建省',13,'系统管理员','2025-02-16 12:47:35','系统管理员','2025-02-16 12:47:35'),(14,0,'','JIANGXISHENG','江西省',14,'系统管理员','2025-02-16 12:47:43','系统管理员','2025-02-16 12:47:43'),(15,0,'','SHANDONGSHENG','山东省',15,'系统管理员','2025-02-16 12:47:52','系统管理员','2025-02-16 12:47:52'),(16,0,'','HENANSHENG','河南省',16,'系统管理员','2025-02-16 12:48:05','系统管理员','2025-02-16 12:48:05'),(17,0,'','HUBEISHENG','湖北省',17,'系统管理员','2025-02-16 12:48:09','系统管理员','2025-02-16 12:48:09'),(18,0,'','HUNANSHENG','湖南省',18,'系统管理员','2025-02-16 12:48:17','系统管理员','2025-02-16 12:48:17'),(19,0,'','GUANGDONGSHENG','广东省',19,'系统管理员','2025-02-16 12:48:28','系统管理员','2025-02-16 12:48:28'),(20,0,'','GUANGXIZHUANGZUZIZHIQU','广西壮族自治区',20,'系统管理员','2025-02-16 12:48:42','系统管理员','2025-02-16 12:48:42'),(21,0,'','HAINANSHENG','海南省',21,'系统管理员','2025-02-16 12:48:47','系统管理员','2025-02-16 12:48:47'),(22,0,'','ZHONGQINGSHI','重庆市',22,'系统管理员','2025-02-16 12:49:00','系统管理员','2025-02-16 12:49:00'),(23,0,'','SICHUANSHENG','四川省',23,'系统管理员','2025-02-16 12:49:05','系统管理员','2025-02-16 12:49:05'),(24,0,'','GUIZHOUSHENG','贵州省',24,'系统管理员','2025-02-16 12:49:10','系统管理员','2025-02-16 12:49:10'),(25,0,'','YUNNANSHENG','云南省',25,'系统管理员','2025-02-16 12:49:14','系统管理员','2025-02-16 12:49:14'),(26,0,'','XIZANGZIZHIQU','西藏自治区',26,'系统管理员','2025-02-16 12:49:28','系统管理员','2025-02-16 12:49:28'),(27,0,'','SHANXISHENG','陕西省',27,'系统管理员','2025-02-16 12:50:06','系统管理员','2025-02-16 12:50:06'),(28,0,'','GANSUSHENG','甘肃省',28,'系统管理员','2025-02-16 12:50:15','系统管理员','2025-02-16 12:50:15'),(29,0,'','QINGHAISHENG','青海省',29,'系统管理员','2025-02-16 12:50:24','系统管理员','2025-02-16 12:50:24'),(30,0,'','NINGXIAHUIZUZIZHIQU','宁夏回族自治区',30,'系统管理员','2025-02-16 12:50:37','系统管理员','2025-02-16 12:50:37'),(31,0,'','XINJIANGWEIWUERZIZHIQU','新疆维吾尔自治区',31,'系统管理员','2025-02-16 12:50:49','系统管理员','2025-02-16 12:50:49'),(32,0,'','XIANGGANGTEBIEXINGZHENGQU','香港特别行政区',32,'系统管理员','2025-02-16 12:50:58','系统管理员','2025-02-16 12:50:58'),(33,0,'','AOMENTEBIEXINGZHENGQU','澳门特别行政区',33,'系统管理员','2025-02-16 12:51:05','系统管理员','2025-02-16 12:51:05'),(34,0,'','TAIWANSHENG','台湾省',34,'系统管理员','2025-02-16 12:51:17','系统管理员','2025-02-16 12:51:17'),(35,1,'1','DONGCHENGQU','东城区',1,'系统管理员','2025-02-16 12:53:00','系统管理员','2025-02-16 12:53:00'),(36,1,'1','XICHENGQU','西城区',2,'系统管理员','2025-02-16 12:53:05','系统管理员','2025-02-16 12:53:05'),(37,1,'1','CHAOYANGQU','朝阳区',3,'系统管理员','2025-02-16 12:53:14','系统管理员','2025-02-16 12:53:14'),(38,1,'1','HAIDIANQU','海淀区',4,'系统管理员','2025-02-16 12:53:19','系统管理员','2025-02-16 12:53:19'),(39,1,'1','FENGTAIQU','丰台区',5,'系统管理员','2025-02-16 12:53:33','系统管理员','2025-02-16 12:53:33'),(40,1,'1','SHIJINGSHANQU','石景山区',6,'系统管理员','2025-02-16 12:53:35','系统管理员','2025-02-16 12:53:35'),(41,1,'1','TONGZHOUQU','通州区',7,'系统管理员','2025-02-16 12:54:06','系统管理员','2025-02-16 12:54:06'),(42,1,'1','SHUNYIQU','顺义区',8,'系统管理员','2025-02-16 12:54:15','系统管理员','2025-02-16 12:54:15'),(43,1,'1','DAXINGQU','大兴区',9,'系统管理员','2025-02-16 12:54:21','系统管理员','2025-02-16 12:54:21'),(44,1,'1','CHANGPINGQU','昌平区',10,'系统管理员','2025-02-16 12:54:28','系统管理员','2025-02-16 12:54:28'),(45,1,'1','FANGSHANQU','房山区',11,'系统管理员','2025-02-16 12:54:36','系统管理员','2025-02-16 12:54:36'),(46,1,'1','MENTOUGOUQU','门头沟区',12,'系统管理员','2025-02-16 12:54:43','系统管理员','2025-02-16 12:54:43'),(47,1,'1','HUAIROUQU','怀柔区',13,'系统管理员','2025-02-16 12:54:49','系统管理员','2025-02-16 12:54:49'),(48,1,'1','PINGGUQU','平谷区',14,'系统管理员','2025-02-16 12:54:56','系统管理员','2025-02-16 12:54:56'),(49,1,'1','MIYUNQU','密云区',15,'系统管理员','2025-02-16 12:55:03','系统管理员','2025-02-16 12:55:03'),(50,1,'1','YANQINGQU','延庆区',16,'系统管理员','2025-02-16 12:55:09','系统管理员','2025-02-16 12:55:09'),(51,2,'2','HEPINGQU','和平区',1,'系统管理员','2025-02-16 12:56:39','系统管理员','2025-02-16 12:56:39'),(52,2,'2','HEDONGQU','河东区',2,'系统管理员','2025-02-16 12:56:46','系统管理员','2025-02-16 12:56:46'),(53,2,'2','HEXIQU','河西区',3,'系统管理员','2025-02-16 12:56:53','系统管理员','2025-02-16 12:56:53'),(54,2,'2','NANKAIQU','南开区',4,'系统管理员','2025-02-16 12:57:00','系统管理员','2025-02-16 12:57:00'),(55,2,'2','HEBEIQU','河北区',5,'系统管理员','2025-02-16 12:57:07','系统管理员','2025-02-16 12:57:07'),(56,2,'2','HONGQIAOQU','红桥区',6,'系统管理员','2025-02-16 12:57:14','系统管理员','2025-02-16 12:57:14'),(57,2,'2','DONGLIQU','东丽区',7,'系统管理员','2025-02-16 12:57:22','系统管理员','2025-02-16 12:57:22'),(58,2,'2','XIQINGQU','西青区',8,'系统管理员','2025-02-16 12:57:28','系统管理员','2025-02-16 12:57:28'),(59,2,'2','JINNANQU','津南区',9,'系统管理员','2025-02-16 12:57:33','系统管理员','2025-02-16 12:57:33'),(60,2,'2','BEICHENQU','北辰区',10,'系统管理员','2025-02-16 12:57:40','系统管理员','2025-02-16 12:57:40'),(61,2,'2','WUQINGQU','武清区',11,'系统管理员','2025-02-16 12:57:45','系统管理员','2025-02-16 12:57:45'),(62,2,'2','BAOCHIQU','宝坻区',12,'系统管理员','2025-02-16 12:57:53','系统管理员','2025-02-16 12:57:53'),(63,2,'2','JIZHOUQU','蓟州区',13,'系统管理员','2025-02-16 12:57:59','系统管理员','2025-02-16 12:57:59'),(64,2,'2','JINGHAIQU','静海区',14,'系统管理员','2025-02-16 12:58:05','系统管理员','2025-02-16 12:58:05'),(65,2,'2','NINGHEQU','宁河区',15,'系统管理员','2025-02-16 12:58:11','系统管理员','2025-02-16 12:58:11'),(66,2,'2','BINHAIXINQU','滨海新区',16,'系统管理员','2025-02-16 12:58:17','系统管理员','2025-02-16 12:58:17'),(67,3,'3','SHIJIAZHUANGSHI','石家庄市',1,'系统管理员','2025-02-16 13:00:21','系统管理员','2025-02-16 13:00:21'),(68,3,'3','TANGSHANSHI','唐山市',2,'系统管理员','2025-02-16 13:00:30','系统管理员','2025-02-16 13:00:30'),(69,3,'3','QINHUANGDAOSHI','秦皇岛市',3,'系统管理员','2025-02-16 13:00:37','系统管理员','2025-02-16 13:00:37'),(70,3,'3','HANDANSHI','邯郸市',4,'系统管理员','2025-02-16 13:00:44','系统管理员','2025-02-16 13:00:44'),(71,3,'3','XINGTAISHI','邢台市',5,'系统管理员','2025-02-16 13:00:50','系统管理员','2025-02-16 13:00:50'),(72,3,'3','BAODINGSHI','保定市',6,'系统管理员','2025-02-16 13:00:56','系统管理员','2025-02-16 13:00:56'),(73,3,'3','ZHANGJIAKOUSHI','张家口市',7,'系统管理员','2025-02-16 13:01:02','系统管理员','2025-02-16 13:01:02'),(74,3,'3','CHENGDESHI','承德市',8,'系统管理员','2025-02-16 13:01:08','系统管理员','2025-02-16 13:01:08'),(75,3,'3','CANGZHOUSHI','沧州市',9,'系统管理员','2025-02-16 13:01:14','系统管理员','2025-02-16 13:01:14'),(76,3,'3','LANGFANGSHI','廊坊市',10,'系统管理员','2025-02-16 13:01:20','系统管理员','2025-02-16 13:01:20'),(77,3,'3','HENGSHUISHI','衡水市',11,'系统管理员','2025-02-16 13:01:25','系统管理员','2025-02-16 13:01:25'),(78,3,'3','DINGZHOUSHI','定州市',12,'系统管理员','2025-02-16 13:01:32','系统管理员','2025-02-16 13:01:32'),(79,3,'3','XINJISHI','辛集市',13,'系统管理员','2025-02-16 13:01:39','系统管理员','2025-02-16 13:01:39'),(80,4,'4','TAIYUANSHI','太原市',1,'系统管理员','2025-02-16 13:04:46','系统管理员','2025-02-16 13:04:46'),(81,4,'4','DATONGSHI','大同市',2,'系统管理员','2025-02-16 13:04:52','系统管理员','2025-02-16 13:04:52'),(82,4,'4','YANGQUANSHI','阳泉市',3,'系统管理员','2025-02-16 13:04:58','系统管理员','2025-02-16 13:04:58'),(83,4,'4','ZHANGZHISHI','长治市',4,'系统管理员','2025-02-16 13:05:04','系统管理员','2025-02-16 13:05:04'),(84,4,'4','JINCHENGSHI','晋城市',5,'系统管理员','2025-02-16 13:05:10','系统管理员','2025-02-16 13:05:10'),(85,4,'4','SHUOZHOUSHI','朔州市',6,'系统管理员','2025-02-16 13:05:18','系统管理员','2025-02-16 13:05:18'),(86,4,'4','JINZHONGSHI','晋中市',7,'系统管理员','2025-02-16 13:05:23','系统管理员','2025-02-16 13:05:23'),(87,4,'4','YUNCHENGSHI','运城市',8,'系统管理员','2025-02-16 13:05:29','系统管理员','2025-02-16 13:05:29'),(88,4,'4','XINZHOUSHI','忻州市',9,'系统管理员','2025-02-16 13:05:35','系统管理员','2025-02-16 13:05:35'),(89,4,'4','LINFENSHI','临汾市',10,'系统管理员','2025-02-16 13:05:41','系统管理员','2025-02-16 13:05:41'),(90,4,'4','LVLIANGSHI','吕梁市',11,'系统管理员','2025-02-16 13:05:47','系统管理员','2025-02-16 13:05:47'),(91,5,'5','HUHEHAOTESHI','呼和浩特市',1,'系统管理员','2025-02-16 13:08:16','系统管理员','2025-02-16 13:08:16'),(92,5,'5','BAOTOUSHI','包头市',2,'系统管理员','2025-02-16 13:08:26','系统管理员','2025-02-16 13:08:26'),(93,5,'5','HULUNBEIERSHI','呼伦贝尔市',3,'系统管理员','2025-02-16 13:08:33','系统管理员','2025-02-16 13:08:33'),(94,5,'5','XINGANMENG','兴安盟',4,'系统管理员','2025-02-16 13:08:39','系统管理员','2025-02-16 13:08:39'),(95,5,'5','TONGLIAOSHI','通辽市',5,'系统管理员','2025-02-16 13:09:03','系统管理员','2025-02-16 13:09:03'),(96,5,'5','CHIFENGSHI','赤峰市',6,'系统管理员','2025-02-16 13:09:11','系统管理员','2025-02-16 13:09:11'),(97,5,'5','XILINGUOLEMENG','锡林郭勒盟',7,'系统管理员','2025-02-16 13:09:18','系统管理员','2025-02-16 13:09:18'),(98,5,'5','WULANCHABUSHI','乌兰察布市',8,'系统管理员','2025-02-16 13:09:35','系统管理员','2025-02-16 13:09:35'),(99,5,'5','EERDUOSISHI','鄂尔多斯市',9,'系统管理员','2025-02-16 13:09:43','系统管理员','2025-02-16 13:09:43'),(100,5,'5','BAYANNAOERSHI','巴彦淖尔市',10,'系统管理员','2025-02-16 13:09:50','系统管理员','2025-02-16 13:09:50'),(101,5,'5','WUHAISHI','乌海市',11,'系统管理员','2025-02-16 13:09:56','系统管理员','2025-02-16 13:09:56'),(102,6,'6','SHENYANGSHI','沈阳市',1,'系统管理员','2025-02-16 13:12:26','系统管理员','2025-02-16 13:12:26'),(103,6,'6','ANSHANSHI','鞍山市',2,'系统管理员','2025-02-16 13:12:36','系统管理员','2025-02-16 13:12:36'),(104,6,'6','FUSHUNSHI','抚顺市',3,'系统管理员','2025-02-16 13:12:43','系统管理员','2025-02-16 13:12:43'),(105,6,'6','BENXISHI','本溪市',4,'系统管理员','2025-02-16 13:12:58','系统管理员','2025-02-16 13:12:58'),(106,6,'6','DANDONGSHI','丹东市',5,'系统管理员','2025-02-16 13:13:06','系统管理员','2025-02-16 13:13:06'),(107,6,'6','JINZHOUSHI','锦州市',6,'系统管理员','2025-02-16 13:13:17','系统管理员','2025-02-16 13:13:17'),(108,6,'6','YINGKOUSHI','营口市',7,'系统管理员','2025-02-16 13:13:26','系统管理员','2025-02-16 13:13:26'),(109,6,'6','FUXINSHI','阜新市',8,'系统管理员','2025-02-16 13:13:43','系统管理员','2025-02-16 13:13:43'),(110,6,'6','LIAOYANGSHI','辽阳市',9,'系统管理员','2025-02-16 13:14:04','系统管理员','2025-02-16 13:14:04'),(111,6,'6','PANJINSHI','盘锦市',10,'系统管理员','2025-02-16 13:14:13','系统管理员','2025-02-16 13:14:13'),(112,6,'6','TIELINGSHI','铁岭市',11,'系统管理员','2025-02-16 13:14:22','系统管理员','2025-02-16 13:14:22'),(113,6,'6','CHAOYANGSHI','朝阳市',12,'系统管理员','2025-02-16 13:14:30','系统管理员','2025-02-16 13:14:30'),(114,6,'6','HULUDAOSHI','葫芦岛市',13,'系统管理员','2025-02-16 13:14:39','系统管理员','2025-02-16 13:14:39'),(115,7,'7','ZHANGCHUNSHI‌','长春市‌',1,'系统管理员','2025-02-16 13:15:37','系统管理员','2025-02-16 13:15:37'),(116,7,'7','JILINSHI','吉林市',2,'系统管理员','2025-02-16 13:15:45','系统管理员','2025-02-16 13:15:45'),(117,7,'7','SIPINGSHI‌','四平市‌',3,'系统管理员','2025-02-16 13:15:53','系统管理员','2025-02-16 13:15:53'),(118,7,'7','LIAOYUANSHI','辽源市',4,'系统管理员','2025-02-16 13:16:05','系统管理员','2025-02-16 13:16:05'),(119,7,'7','TONGHUASHI','通化市',5,'系统管理员','2025-02-16 13:16:12','系统管理员','2025-02-16 13:16:12'),(120,7,'7','BAISHANSHI','白山市',6,'系统管理员','2025-02-16 13:16:18','系统管理员','2025-02-16 13:16:18'),(121,7,'7','SONGYUANSHI','松原市',7,'系统管理员','2025-02-16 13:16:26','系统管理员','2025-02-16 13:16:26'),(122,7,'7','BAICHENGSHI','白城市',8,'系统管理员','2025-02-16 13:16:32','系统管理员','2025-02-16 13:16:32'),(123,8,'8','HAERBINSHI‌','哈尔滨市‌',1,'系统管理员','2025-02-16 13:17:15','系统管理员','2025-02-16 13:17:15'),(124,8,'8','QIQIHAERSHI‌','齐齐哈尔市‌',2,'系统管理员','2025-02-16 13:17:23','系统管理员','2025-02-16 13:17:23'),(125,8,'8','MUDANJIANGSHI‌','牡丹江市‌',3,'系统管理员','2025-02-16 13:17:30','系统管理员','2025-02-16 13:17:30'),(126,8,'8','JIAMUSISHI‌','佳木斯市‌',4,'系统管理员','2025-02-16 13:17:37','系统管理员','2025-02-16 13:17:37'),(127,8,'8','DAQINGSHI','大庆市',5,'系统管理员','2025-02-16 13:17:47','系统管理员','2025-02-16 13:17:47'),(128,8,'8','JIXISHI','鸡西市',6,'系统管理员','2025-02-16 13:17:56','系统管理员','2025-02-16 13:17:56'),(129,8,'8','SHUANGYASHANSHI','双鸭山市',7,'系统管理员','2025-02-16 13:18:04','系统管理员','2025-02-16 13:18:04'),(130,8,'8','YICHUNSHI','伊春市',8,'系统管理员','2025-02-16 13:18:11','系统管理员','2025-02-16 13:18:11'),(131,8,'8','HEGANGSHI','鹤岗市',9,'系统管理员','2025-02-16 13:18:18','系统管理员','2025-02-16 13:18:18'),(132,8,'8','HEIHESHI','黑河市',10,'系统管理员','2025-02-16 13:18:25','系统管理员','2025-02-16 13:18:25'),(133,8,'8','SUIHUASHI','绥化市',11,'系统管理员','2025-02-16 13:18:32','系统管理员','2025-02-16 13:18:32'),(134,9,'9','HUANGPUQU','黄浦区',1,'系统管理员','2025-02-16 13:19:19','系统管理员','2025-02-16 13:19:19'),(135,9,'9','XUHUIQU','徐汇区',2,'系统管理员','2025-02-16 13:19:27','系统管理员','2025-02-16 13:19:27'),(136,9,'9','ZHANGNINGQU','长宁区',3,'系统管理员','2025-02-16 13:19:35','系统管理员','2025-02-16 13:19:35'),(137,9,'9','JINGANQU','静安区',4,'系统管理员','2025-02-16 13:19:42','系统管理员','2025-02-16 13:19:42'),(138,9,'9','PUTUOQU','普陀区',5,'系统管理员','2025-02-16 13:19:50','系统管理员','2025-02-16 13:19:50'),(139,9,'9','HONGKOUQU','虹口区',6,'系统管理员','2025-02-16 13:19:57','系统管理员','2025-02-16 13:19:57'),(140,9,'9','YANGPUQU','杨浦区',7,'系统管理员','2025-02-16 13:20:03','系统管理员','2025-02-16 13:20:03'),(141,9,'9','BAOSHANQU','宝山区',8,'系统管理员','2025-02-16 13:20:11','系统管理员','2025-02-16 13:20:11'),(142,9,'9','MINXINGQU','闵行区',9,'系统管理员','2025-02-16 13:20:18','系统管理员','2025-02-16 13:20:18'),(143,9,'9','JIADINGQU','嘉定区',10,'系统管理员','2025-02-16 13:20:26','系统管理员','2025-02-16 13:20:26'),(144,9,'9','PUDONGXINQU‌','浦东新区‌',11,'系统管理员','2025-02-16 13:20:34','系统管理员','2025-02-16 13:20:34'),(145,9,'9','JINSHANQU','金山区',12,'系统管理员','2025-02-16 13:20:42','系统管理员','2025-02-16 13:20:42'),(146,9,'9','SONGJIANGQU','松江区',13,'系统管理员','2025-02-16 13:20:49','系统管理员','2025-02-16 13:20:49'),(147,9,'9','QINGPUQU','青浦区',14,'系统管理员','2025-02-16 13:20:58','系统管理员','2025-02-16 13:20:58'),(148,9,'9','FENGXIANQU','奉贤区',15,'系统管理员','2025-02-16 13:21:05','系统管理员','2025-02-16 13:21:05'),(149,9,'9','CHONGMINGQU','崇明区',16,'系统管理员','2025-02-16 13:21:12','系统管理员','2025-02-16 13:21:12'),(150,10,'10','NANJINGSHI','南京市',1,'系统管理员','2025-02-16 13:21:49','系统管理员','2025-02-16 13:21:49'),(151,10,'10','WUXI‌SHI','无锡‌市',2,'系统管理员','2025-02-16 13:21:59','系统管理员','2025-02-16 13:21:59'),(152,10,'10','XUZHOU‌SHI','徐州‌市',3,'系统管理员','2025-02-16 13:22:10','系统管理员','2025-02-16 13:22:10'),(153,10,'10','CHANGZHOU‌SHI','常州‌市',4,'系统管理员','2025-02-16 13:22:19','系统管理员','2025-02-16 13:22:19'),(154,10,'10','SUZHOU‌SHI','苏州‌市',5,'系统管理员','2025-02-16 13:22:29','系统管理员','2025-02-16 13:22:29'),(155,10,'10','NANTONG‌SHI','南通‌市',6,'系统管理员','2025-02-16 13:22:39','系统管理员','2025-02-16 13:22:39'),(156,10,'10','LIANYUNGANG‌SHI','连云港‌市',7,'系统管理员','2025-02-16 13:22:48','系统管理员','2025-02-16 13:22:48'),(157,10,'10','HUAIAN‌SHI','淮安‌市',8,'系统管理员','2025-02-16 13:23:06','系统管理员','2025-02-16 13:23:06'),(158,10,'10','YANGZHOU‌SHI','扬州‌市',9,'系统管理员','2025-02-16 13:23:31','系统管理员','2025-02-16 13:23:31'),(159,10,'10','ZHENJIANG‌SHI','镇江‌市',10,'系统管理员','2025-02-16 13:23:40','系统管理员','2025-02-16 13:23:40'),(160,10,'10','TAIZHOU‌SHI','泰州‌市',11,'系统管理员','2025-02-16 13:23:50','系统管理员','2025-02-16 13:23:50'),(161,10,'10','SUQIANSHI','宿迁市',12,'系统管理员','2025-02-16 13:24:00','系统管理员','2025-02-16 13:24:00'),(162,10,'10','YANCHENG‌SHI','盐城‌市',13,'系统管理员','2025-02-16 13:24:09','系统管理员','2025-02-16 13:24:09'),(163,11,'11','HANGZHOUSHI','杭州市',1,'系统管理员','2025-02-16 13:24:46','系统管理员','2025-02-16 13:24:46'),(164,11,'11','NINGBOSHI','宁波市',2,'系统管理员','2025-02-16 13:24:54','系统管理员','2025-02-16 13:24:54'),(165,11,'11','WENZHOUSHI','温州市',3,'系统管理员','2025-02-16 13:25:02','系统管理员','2025-02-16 13:25:02'),(166,11,'11','JIAXINGSHI','嘉兴市',4,'系统管理员','2025-02-16 13:25:08','系统管理员','2025-02-16 13:25:08'),(167,11,'11','HUZHOUSHI','湖州市',5,'系统管理员','2025-02-16 13:25:16','系统管理员','2025-02-16 13:25:16'),(168,11,'11','SHAOXINGSHI','绍兴市',6,'系统管理员','2025-02-16 13:25:23','系统管理员','2025-02-16 13:25:23'),(169,11,'11','JINHUASHI','金华市',7,'系统管理员','2025-02-16 13:25:29','系统管理员','2025-02-16 13:25:29'),(170,11,'11','QUZHOUSHI','衢州市',8,'系统管理员','2025-02-16 13:25:36','系统管理员','2025-02-16 13:25:36'),(171,11,'11','ZHOUSHANSHI','舟山市',9,'系统管理员','2025-02-16 13:25:42','系统管理员','2025-02-16 13:25:42'),(172,11,'11','TAIZHOUSHI','台州市',10,'系统管理员','2025-02-16 13:25:49','系统管理员','2025-02-16 13:25:49'),(173,11,'11','LISHUISHI','丽水市',11,'系统管理员','2025-02-16 13:25:57','系统管理员','2025-02-16 13:25:57'),(174,12,'12','HEFEISHI‌','合肥市‌',1,'系统管理员','2025-02-16 13:26:41','系统管理员','2025-02-16 13:26:41'),(175,12,'12','WUHUSHI','芜湖市',2,'系统管理员','2025-02-16 13:26:49','系统管理员','2025-02-16 13:26:49'),(176,12,'12','BANGBUSHI‌','蚌埠市‌',3,'系统管理员','2025-02-16 13:26:59','系统管理员','2025-02-16 13:26:59'),(177,12,'12','HUAINANSHI','淮南市',4,'系统管理员','2025-02-16 13:27:07','系统管理员','2025-02-16 13:27:07'),(178,12,'12','MAANSHANSHI‌','马鞍山市‌',5,'系统管理员','2025-02-16 13:27:15','系统管理员','2025-02-16 13:27:15'),(179,12,'12','TONGLINGSHI','铜陵市',6,'系统管理员','2025-02-16 13:27:21','系统管理员','2025-02-16 13:27:21'),(180,12,'12','ANQINGSHI','安庆市',7,'系统管理员','2025-02-16 13:27:29','系统管理员','2025-02-16 13:27:29'),(181,12,'12','HUANGSHANSHI','黄山市',8,'系统管理员','2025-02-16 13:27:36','系统管理员','2025-02-16 13:27:36'),(182,12,'12','CHUZHOUSHI‌','滁州市‌',9,'系统管理员','2025-02-16 13:27:42','系统管理员','2025-02-16 13:27:42'),(183,12,'12','FUYANGSHI','阜阳市',10,'系统管理员','2025-02-16 13:27:49','系统管理员','2025-02-16 13:27:49'),(184,12,'12','SUZHOUSHI','宿州市',11,'系统管理员','2025-02-16 13:27:56','系统管理员','2025-02-16 13:27:56'),(185,12,'12','LIUANSHI‌','六安市‌',12,'系统管理员','2025-02-16 13:28:03','系统管理员','2025-02-16 13:28:03'),(186,12,'12','CHAOHUSHI','巢湖市',13,'系统管理员','2025-02-16 13:28:12','系统管理员','2025-02-16 13:28:12'),(187,12,'12','CHIZHOUSHI','池州市',14,'系统管理员','2025-02-16 13:28:20','系统管理员','2025-02-16 13:28:20'),(188,12,'12','XUANCHENGSHI','宣城市',15,'系统管理员','2025-02-16 13:28:26','系统管理员','2025-02-16 13:28:26'),(189,13,'13','FUZHOUSHI','福州市',1,'系统管理员','2025-02-16 13:29:01','系统管理员','2025-02-16 13:29:01'),(190,13,'13','SHAMENSHI','厦门市',2,'系统管理员','2025-02-16 13:29:07','系统管理员','2025-02-16 13:29:07'),(191,13,'13','PUTIANSHI','莆田市',3,'系统管理员','2025-02-16 13:29:13','系统管理员','2025-02-16 13:29:13'),(192,13,'13','SANMINGSHI','三明市',4,'系统管理员','2025-02-16 13:29:20','系统管理员','2025-02-16 13:29:20'),(193,13,'13','QUANZHOUSHI','泉州市',5,'系统管理员','2025-02-16 13:29:27','系统管理员','2025-02-16 13:29:27'),(194,13,'13','ZHANGZHOUSHI','漳州市',6,'系统管理员','2025-02-16 13:29:33','系统管理员','2025-02-16 13:29:33'),(195,13,'13','NANPINGSHI','南平市',7,'系统管理员','2025-02-16 13:29:39','系统管理员','2025-02-16 13:29:39'),(196,13,'13','LONGYANSHI','龙岩市',8,'系统管理员','2025-02-16 13:29:45','系统管理员','2025-02-16 13:29:45'),(197,13,'13','NINGDESHI','宁德市',9,'系统管理员','2025-02-16 13:29:54','系统管理员','2025-02-16 13:29:54'),(198,14,'14','NANCHANGSHI','南昌市',1,'系统管理员','2025-02-16 13:30:21','系统管理员','2025-02-16 13:30:21'),(199,14,'14','JIUJIANGSHI','九江市',2,'系统管理员','2025-02-16 13:30:27','系统管理员','2025-02-16 13:30:27'),(200,14,'14','JINGDEZHENSHI','景德镇市',3,'系统管理员','2025-02-16 13:30:33','系统管理员','2025-02-16 13:30:33'),(201,14,'14','SHANGRAOSHI','上饶市',4,'系统管理员','2025-02-16 13:30:39','系统管理员','2025-02-16 13:30:39'),(202,14,'14','YINGTANSHI','鹰潭市',5,'系统管理员','2025-02-16 13:30:46','系统管理员','2025-02-16 13:30:46'),(203,14,'14','FUZHOUSHI','抚州市',6,'系统管理员','2025-02-16 13:30:53','系统管理员','2025-02-16 13:30:53'),(204,14,'14','YICHUNSHI','宜春市',7,'系统管理员','2025-02-16 13:31:00','系统管理员','2025-02-16 13:31:00'),(205,14,'14','XINYUSHI','新余市',8,'系统管理员','2025-02-16 13:31:07','系统管理员','2025-02-16 13:31:07'),(206,14,'14','PINGXIANGSHI','萍乡市',9,'系统管理员','2025-02-16 13:31:13','系统管理员','2025-02-16 13:31:13'),(207,14,'14','GANZHOUSHI','赣州市',10,'系统管理员','2025-02-16 13:31:20','系统管理员','2025-02-16 13:31:20'),(208,15,'15','JINANSHI','济南市',1,'系统管理员','2025-02-16 13:33:18','系统管理员','2025-02-16 13:33:18'),(209,15,'15','QINGDAOSHI','青岛市',2,'系统管理员','2025-02-16 13:33:26','系统管理员','2025-02-16 13:33:26'),(210,15,'15','ZIBOSHI','淄博市',3,'系统管理员','2025-02-16 13:33:33','系统管理员','2025-02-16 13:33:33'),(211,15,'15','ZAOZHUANGSHI','枣庄市',4,'系统管理员','2025-02-16 13:33:39','系统管理员','2025-02-16 13:33:39'),(212,15,'15','DONGYINGSHI‌','东营市‌',5,'系统管理员','2025-02-16 13:33:48','系统管理员','2025-02-16 13:33:48'),(213,15,'15','YANTAISHI','烟台市',6,'系统管理员','2025-02-16 13:33:54','系统管理员','2025-02-16 13:33:54'),(214,15,'15','WEIFANGSHI','潍坊市',7,'系统管理员','2025-02-16 13:34:02','系统管理员','2025-02-16 13:34:02'),(215,15,'15','JININGSHI','济宁市',8,'系统管理员','2025-02-16 13:34:09','系统管理员','2025-02-16 13:34:09'),(216,15,'15','TAIANSHI','泰安市',9,'系统管理员','2025-02-16 13:34:16','系统管理员','2025-02-16 13:34:16'),(217,15,'15','WEIHAISHI','威海市',10,'系统管理员','2025-02-16 13:34:23','系统管理员','2025-02-16 13:34:23'),(218,15,'15','RIZHAOSHI','日照市',11,'系统管理员','2025-02-16 13:34:30','系统管理员','2025-02-16 13:34:30'),(219,15,'15','BINZHOUSHI','滨州市',12,'系统管理员','2025-02-16 13:34:36','系统管理员','2025-02-16 13:34:36'),(220,15,'15','DEZHOUSHI','德州市',13,'系统管理员','2025-02-16 13:34:43','系统管理员','2025-02-16 13:34:43'),(221,15,'15','LIAOCHENGSHI','聊城市',14,'系统管理员','2025-02-16 13:34:50','系统管理员','2025-02-16 13:34:50'),(222,15,'15','LINYISHI','临沂市',15,'系统管理员','2025-02-16 13:34:56','系统管理员','2025-02-16 13:34:56'),(223,15,'15','HEZESHI','菏泽市',16,'系统管理员','2025-02-16 13:35:03','系统管理员','2025-02-16 13:35:03'),(224,16,'16','ZHENGZHOUSHI','郑州市',1,'系统管理员','2025-02-16 13:35:48','系统管理员','2025-02-16 13:35:48'),(225,16,'16','KAIFENGSHI','开封市',2,'系统管理员','2025-02-16 13:35:54','系统管理员','2025-02-16 13:35:54'),(226,16,'16','LUOYANGSHI','洛阳市',3,'系统管理员','2025-02-16 13:36:01','系统管理员','2025-02-16 13:36:01'),(227,16,'16','PINGDINGSHANSHI','平顶山市',4,'系统管理员','2025-02-16 13:36:08','系统管理员','2025-02-16 13:36:08'),(228,16,'16','ANYANGSHI','安阳市',5,'系统管理员','2025-02-16 13:36:15','系统管理员','2025-02-16 13:36:15'),(229,16,'16','HEBISHI','鹤壁市',6,'系统管理员','2025-02-16 13:36:21','系统管理员','2025-02-16 13:36:21'),(230,16,'16','XINXIANGSHI','新乡市',7,'系统管理员','2025-02-16 13:36:34','系统管理员','2025-02-16 13:36:34'),(231,16,'16','JIAOZUOSHI','焦作市',8,'系统管理员','2025-02-16 13:36:41','系统管理员','2025-02-16 13:36:41'),(232,16,'16','PUYANGSHI','濮阳市',9,'系统管理员','2025-02-16 13:36:48','系统管理员','2025-02-16 13:36:48'),(233,16,'16','XUCHANGSHI','许昌市',10,'系统管理员','2025-02-16 13:36:54','系统管理员','2025-02-16 13:36:54'),(234,16,'16','LUOHESHI','漯河市',11,'系统管理员','2025-02-16 13:37:00','系统管理员','2025-02-16 13:37:00'),(235,16,'16','SANMENXIASHI','三门峡市',12,'系统管理员','2025-02-16 13:37:06','系统管理员','2025-02-16 13:37:06'),(236,16,'16','NANYANGSHI','南阳市',13,'系统管理员','2025-02-16 13:37:12','系统管理员','2025-02-16 13:37:12'),(237,16,'16','SHANGQIUSHI','商丘市',14,'系统管理员','2025-02-16 13:37:18','系统管理员','2025-02-16 13:37:18'),(238,16,'16','XINYANGSHI','信阳市',15,'系统管理员','2025-02-16 13:37:44','系统管理员','2025-02-16 13:37:44'),(239,16,'16','ZHOUKOUSHI','周口市',16,'系统管理员','2025-02-16 13:37:51','系统管理员','2025-02-16 13:37:51'),(240,16,'16','ZHUMADIANSHI','驻马店市',17,'系统管理员','2025-02-16 13:38:01','系统管理员','2025-02-16 13:38:01'),(241,17,'17','WUHANSHI','武汉市',1,'系统管理员','2025-02-16 13:38:34','系统管理员','2025-02-16 13:38:34'),(242,17,'17','HUANGSHISHI','黄石市',2,'系统管理员','2025-02-16 13:38:41','系统管理员','2025-02-16 13:38:41'),(243,17,'17','XIANGYANGSHI','襄阳市',3,'系统管理员','2025-02-16 13:38:47','系统管理员','2025-02-16 13:38:47'),(244,17,'17','JINGZHOUSHI','荆州市',4,'系统管理员','2025-02-16 13:38:54','系统管理员','2025-02-16 13:38:54'),(245,17,'17','YICHANGSHI','宜昌市',5,'系统管理员','2025-02-16 13:39:00','系统管理员','2025-02-16 13:39:00'),(246,17,'17','SHIYANSHI','十堰市',6,'系统管理员','2025-02-16 13:39:06','系统管理员','2025-02-16 13:39:06'),(247,17,'17','XIAOGANSHI','孝感市',7,'系统管理员','2025-02-16 13:39:12','系统管理员','2025-02-16 13:39:12'),(248,17,'17','JINGMENSHI','荆门市',8,'系统管理员','2025-02-16 13:39:19','系统管理员','2025-02-16 13:39:19'),(249,17,'17','EZHOUSHI','鄂州市',9,'系统管理员','2025-02-16 13:39:25','系统管理员','2025-02-16 13:39:25'),(250,17,'17','HUANGGANGSHI','黄冈市',10,'系统管理员','2025-02-16 13:39:34','系统管理员','2025-02-16 13:39:34'),(251,17,'17','XIANNINGSHI','咸宁市',11,'系统管理员','2025-02-16 13:39:42','系统管理员','2025-02-16 13:39:42'),(252,17,'17','SUIZHOUSHI','随州市',12,'系统管理员','2025-02-16 13:39:48','系统管理员','2025-02-16 13:39:48'),(253,18,'18','ZHANGSHASHI','长沙市',1,'系统管理员','2025-02-16 13:40:24','系统管理员','2025-02-16 13:40:24'),(254,18,'18','ZHUZHOUSHI','株洲市',2,'系统管理员','2025-02-16 13:40:30','系统管理员','2025-02-16 13:40:30'),(255,18,'18','XIANGTANSHI','湘潭市',3,'系统管理员','2025-02-16 13:40:37','系统管理员','2025-02-16 13:40:37'),(256,18,'18','HENGYANGSHI','衡阳市',4,'系统管理员','2025-02-16 13:40:43','系统管理员','2025-02-16 13:40:43'),(257,18,'18','YUEYANGSHI','岳阳市',5,'系统管理员','2025-02-16 13:40:49','系统管理员','2025-02-16 13:40:49'),(258,18,'18','CHANGDESHI','常德市',6,'系统管理员','2025-02-16 13:40:55','系统管理员','2025-02-16 13:40:55'),(259,18,'18','SHAOYANGSHI','邵阳市',7,'系统管理员','2025-02-16 13:41:02','系统管理员','2025-02-16 13:41:02'),(260,18,'18','ZHANGJIAJIESHI','张家界市',8,'系统管理员','2025-02-16 13:41:08','系统管理员','2025-02-16 13:41:08'),(261,18,'18','YIYANGSHI','益阳市',9,'系统管理员','2025-02-16 13:41:15','系统管理员','2025-02-16 13:41:15'),(262,18,'18','CHENZHOUSHI','郴州市',10,'系统管理员','2025-02-16 13:41:25','系统管理员','2025-02-16 13:41:25'),(263,18,'18','YONGZHOUSHI','永州市',11,'系统管理员','2025-02-16 13:41:32','系统管理员','2025-02-16 13:41:32'),(264,18,'18','LOUDISHI','娄底市',12,'系统管理员','2025-02-16 13:41:37','系统管理员','2025-02-16 13:41:37'),(265,18,'18','HUAIHUASHI‌','怀化市‌',13,'系统管理员','2025-02-16 13:41:44','系统管理员','2025-02-16 13:41:44'),(266,19,'19','GUANGZHOUSHI','广州市',1,'系统管理员','2025-02-16 13:42:20','系统管理员','2025-02-16 13:42:20'),(267,19,'19','SHAOGUANSHI','韶关市',2,'系统管理员','2025-02-16 13:42:26','系统管理员','2025-02-16 13:42:26'),(268,19,'19','SHENZHENSHI','深圳市',3,'系统管理员','2025-02-16 13:42:33','系统管理员','2025-02-16 13:42:33'),(269,19,'19','ZHUHAISHI','珠海市',4,'系统管理员','2025-02-16 13:42:40','系统管理员','2025-02-16 13:42:40'),(270,19,'19','SHANTOUSHI','汕头市',5,'系统管理员','2025-02-16 13:42:46','系统管理员','2025-02-16 13:42:46'),(271,19,'19','FOSHANSHI','佛山市',6,'系统管理员','2025-02-16 13:42:53','系统管理员','2025-02-16 13:42:53'),(272,19,'19','JIANGMENSHI','江门市',7,'系统管理员','2025-02-16 13:43:01','系统管理员','2025-02-16 13:43:01'),(273,19,'19','ZHANJIANGSHI','湛江市',8,'系统管理员','2025-02-16 13:43:07','系统管理员','2025-02-16 13:43:07'),(274,19,'19','MAOMINGSHI','茂名市',9,'系统管理员','2025-02-16 13:43:13','系统管理员','2025-02-16 13:43:13'),(275,19,'19','ZHAOQINGSHI','肇庆市',10,'系统管理员','2025-02-16 13:43:19','系统管理员','2025-02-16 13:43:19'),(276,19,'19','HUIZHOUSHI','惠州市',11,'系统管理员','2025-02-16 13:43:25','系统管理员','2025-02-16 13:43:25'),(277,19,'19','MEIZHOUSHI','梅州市',12,'系统管理员','2025-02-16 13:43:30','系统管理员','2025-02-16 13:43:30'),(278,19,'19','SHANWEISHI','汕尾市',13,'系统管理员','2025-02-16 13:43:36','系统管理员','2025-02-16 13:43:36'),(279,19,'19','HEYUANSHI','河源市',14,'系统管理员','2025-02-16 13:43:42','系统管理员','2025-02-16 13:43:42'),(280,19,'19','YANGJIANGSHI','阳江市',15,'系统管理员','2025-02-16 13:43:47','系统管理员','2025-02-16 13:43:47'),(281,19,'19','QINGYUANSHI','清远市',16,'系统管理员','2025-02-16 13:43:53','系统管理员','2025-02-16 13:43:53'),(282,19,'19','DONGGUANSHI','东莞市',17,'系统管理员','2025-02-16 13:44:02','系统管理员','2025-02-16 13:44:02'),(283,19,'19','ZHONGSHANSHI','中山市',18,'系统管理员','2025-02-16 13:44:09','系统管理员','2025-02-16 13:44:09'),(284,19,'19','CHAOZHOUSHI','潮州市',19,'系统管理员','2025-02-16 13:44:14','系统管理员','2025-02-16 13:44:14'),(285,19,'19','JIEYANGSHI','揭阳市',20,'系统管理员','2025-02-16 13:44:20','系统管理员','2025-02-16 13:44:20'),(286,19,'19','YUNFUSHI','云浮市',21,'系统管理员','2025-02-16 13:44:27','系统管理员','2025-02-16 13:44:27'),(287,20,'20','NANNINGSHI','南宁市',1,'系统管理员','2025-02-16 13:45:00','系统管理员','2025-02-16 13:45:00'),(288,20,'20','LIUZHOUSHI','柳州市',2,'系统管理员','2025-02-16 13:45:07','系统管理员','2025-02-16 13:45:07'),(289,20,'20','GUILINSHI','桂林市',3,'系统管理员','2025-02-16 13:45:14','系统管理员','2025-02-16 13:45:14'),(290,20,'20','WUZHOUSHI','梧州市',4,'系统管理员','2025-02-16 13:45:20','系统管理员','2025-02-16 13:45:20'),(291,20,'20','BEIHAISHI','北海市',5,'系统管理员','2025-02-16 13:45:26','系统管理员','2025-02-16 13:45:26'),(292,20,'20','FANGCHENGGANGSHI','防城港市',6,'系统管理员','2025-02-16 13:45:35','系统管理员','2025-02-16 13:45:35'),(293,20,'20','QINZHOUSHI','钦州市',7,'系统管理员','2025-02-16 13:45:42','系统管理员','2025-02-16 13:45:42'),(294,20,'20','GUIGANGSHI','贵港市',8,'系统管理员','2025-02-16 13:45:48','系统管理员','2025-02-16 13:45:48'),(295,20,'20','YULINSHI','玉林市',9,'系统管理员','2025-02-16 13:45:55','系统管理员','2025-02-16 13:45:55'),(296,20,'20','BAISESHI','百色市',10,'系统管理员','2025-02-16 13:46:01','系统管理员','2025-02-16 13:46:01'),(297,20,'20','HEZHOUSHI','贺州市',11,'系统管理员','2025-02-16 13:46:07','系统管理员','2025-02-16 13:46:07'),(298,20,'20','HECHISHI','河池市',12,'系统管理员','2025-02-16 13:46:14','系统管理员','2025-02-16 13:46:14'),(299,20,'20','LAIBINSHI','来宾市',13,'系统管理员','2025-02-16 13:46:22','系统管理员','2025-02-16 13:46:22'),(300,20,'20','CHONGZUOSHI‌','崇左市‌',14,'系统管理员','2025-02-16 13:46:29','系统管理员','2025-02-16 13:46:29'),(301,21,'21','HAIKOUSHI','海口市',1,'系统管理员','2025-02-16 13:47:08','系统管理员','2025-02-16 13:47:08'),(302,21,'21','SANYASHI','三亚市',2,'系统管理员','2025-02-16 13:47:15','系统管理员','2025-02-16 13:47:15'),(303,21,'21','SANSHASHI','三沙市',3,'系统管理员','2025-02-16 13:47:22','系统管理员','2025-02-16 13:47:22'),(304,21,'21','DANZHOUSHI‌','儋州市‌',4,'系统管理员','2025-02-16 13:47:28','系统管理员','2025-02-16 13:47:28'),(305,21,'21','WUZHISHANSHI','五指山市',5,'系统管理员','2025-02-16 13:47:38','系统管理员','2025-02-16 13:47:38'),(306,21,'21','WENCHANGSHI','文昌市',6,'系统管理员','2025-02-16 13:47:44','系统管理员','2025-02-16 13:47:44'),(307,21,'21','QIONGHAISHI','琼海市',7,'系统管理员','2025-02-16 13:47:50','系统管理员','2025-02-16 13:47:50'),(308,21,'21','WANNINGSHI','万宁市',8,'系统管理员','2025-02-16 13:47:57','系统管理员','2025-02-16 13:47:57'),(309,21,'21','DONGFANGSHI‌','东方市‌',9,'系统管理员','2025-02-16 13:48:05','系统管理员','2025-02-16 13:48:05'),(310,23,'23','CHENGDOUSHI','成都市',1,'系统管理员','2025-02-16 13:49:02','系统管理员','2025-02-16 13:49:02'),(311,23,'23','MIANYANGSHI','绵阳市',2,'系统管理员','2025-02-16 13:49:12','系统管理员','2025-02-16 13:49:12'),(312,23,'23','ZIGONGSHI','自贡市',3,'系统管理员','2025-02-16 13:49:19','系统管理员','2025-02-16 13:49:19'),(313,23,'23','PANZHIHUASHI','攀枝花市',4,'系统管理员','2025-02-16 13:49:25','系统管理员','2025-02-16 13:49:25'),(314,23,'23','LUZHOUSHI','泸州市',5,'系统管理员','2025-02-16 13:49:31','系统管理员','2025-02-16 13:49:31'),(315,23,'23','DEYANGSHI','德阳市',6,'系统管理员','2025-02-16 13:49:37','系统管理员','2025-02-16 13:49:37'),(316,23,'23','GUANGYUANSHI','广元市',7,'系统管理员','2025-02-16 13:49:43','系统管理员','2025-02-16 13:49:43'),(317,23,'23','SUININGSHI','遂宁市',8,'系统管理员','2025-02-16 13:49:49','系统管理员','2025-02-16 13:49:49'),(318,23,'23','NEIJIANGSHI','内江市',9,'系统管理员','2025-02-16 13:49:55','系统管理员','2025-02-16 13:49:55'),(319,23,'23','LESHANSHI','乐山市',10,'系统管理员','2025-02-16 13:50:01','系统管理员','2025-02-16 13:50:01'),(320,23,'23','NANCHONGSHI','南充市',11,'系统管理员','2025-02-16 13:50:07','系统管理员','2025-02-16 13:50:07'),(321,23,'23','MEISHANSHI','眉山市',12,'系统管理员','2025-02-16 13:50:12','系统管理员','2025-02-16 13:50:12'),(322,23,'23','YIBINSHI','宜宾市',13,'系统管理员','2025-02-16 13:50:19','系统管理员','2025-02-16 13:50:19'),(323,23,'23','GUANGANSHI','广安市',14,'系统管理员','2025-02-16 13:50:24','系统管理员','2025-02-16 13:50:24'),(324,23,'23','DAZHOUSHI','达州市',15,'系统管理员','2025-02-16 13:50:30','系统管理员','2025-02-16 13:50:30'),(325,23,'23','YAANSHI','雅安市',16,'系统管理员','2025-02-16 13:50:35','系统管理员','2025-02-16 13:50:35'),(326,23,'23','BAZHONGSHI','巴中市',17,'系统管理员','2025-02-16 13:50:41','系统管理员','2025-02-16 13:50:41'),(327,23,'23','ZIYANGSHI','资阳市',18,'系统管理员','2025-02-16 13:50:47','系统管理员','2025-02-16 13:50:47'),(328,24,'24','GUIYANGSHI','贵阳市',1,'系统管理员','2025-02-16 13:51:16','系统管理员','2025-02-16 13:51:16'),(329,24,'24','LIUPANSHUISHI','六盘水市',2,'系统管理员','2025-02-16 13:51:24','系统管理员','2025-02-16 13:51:24'),(330,24,'24','ZUNYISHI','遵义市',3,'系统管理员','2025-02-16 13:51:29','系统管理员','2025-02-16 13:51:29'),(331,24,'24','ANSHUNSHI','安顺市',4,'系统管理员','2025-02-16 13:51:35','系统管理员','2025-02-16 13:51:35'),(332,24,'24','BIJIESHI','毕节市',5,'系统管理员','2025-02-16 13:51:41','系统管理员','2025-02-16 13:51:41'),(333,24,'24','TONGRENSHI','铜仁市',6,'系统管理员','2025-02-16 13:51:50','系统管理员','2025-02-16 13:51:50'),(334,25,'25','KUNMINGSHI','昆明市',1,'系统管理员','2025-02-16 13:52:24','系统管理员','2025-02-16 13:52:24'),(335,25,'25','ZHAOTONGSHI','昭通市',2,'系统管理员','2025-02-16 13:52:30','系统管理员','2025-02-16 13:52:30'),(336,25,'25','QUJINGSHI','曲靖市',3,'系统管理员','2025-02-16 13:52:36','系统管理员','2025-02-16 13:52:36'),(337,25,'25','YUXISHI','玉溪市',4,'系统管理员','2025-02-16 13:52:42','系统管理员','2025-02-16 13:52:42'),(338,25,'25','BAOSHANSHI','保山市',5,'系统管理员','2025-02-16 13:52:48','系统管理员','2025-02-16 13:52:48'),(339,25,'25','LIJIANGSHI','丽江市',6,'系统管理员','2025-02-16 13:52:55','系统管理员','2025-02-16 13:52:55'),(340,25,'25','PUERSHI','普洱市',7,'系统管理员','2025-02-16 13:53:01','系统管理员','2025-02-16 13:53:01'),(341,25,'25','LINCANGSHI','临沧市',8,'系统管理员','2025-02-16 13:53:11','系统管理员','2025-02-16 13:53:11'),(342,25,'25','CHUXIONGYIZUZIZHIZHOU','楚雄彝族自治州',9,'系统管理员','2025-02-16 13:53:19','系统管理员','2025-02-16 13:53:19'),(343,25,'25','HONGHEHANIZUYIZUZIZHIZHOU','红河哈尼族彝族自治州',10,'系统管理员','2025-02-16 13:53:26','系统管理员','2025-02-16 13:53:26'),(344,25,'25','WENSHANZHUANGZUMIAOZUZIZHIZHOU','文山壮族苗族自治州',11,'系统管理员','2025-02-16 13:53:33','系统管理员','2025-02-16 13:53:33'),(345,25,'25','XISHUANGBANNADAIZUZIZHIZHOU','西双版纳傣族自治州',12,'系统管理员','2025-02-16 13:53:39','系统管理员','2025-02-16 13:53:39'),(346,25,'25','DALIBAIZUZIZHIZHOU','大理白族自治州',13,'系统管理员','2025-02-16 13:53:46','系统管理员','2025-02-16 13:53:46'),(347,25,'25','DEHONGDAIZUJINGPOZUZIZHIZHOU','德宏傣族景颇族自治州',14,'系统管理员','2025-02-16 13:53:53','系统管理员','2025-02-16 13:53:53'),(348,25,'25','NUJIANGLISUZUZIZHIZHOU','怒江傈僳族自治州',15,'系统管理员','2025-02-16 13:53:59','系统管理员','2025-02-16 13:53:59'),(349,25,'25','DIQINGZANGZUZIZHIZHOU','迪庆藏族自治州',16,'系统管理员','2025-02-16 13:54:06','系统管理员','2025-02-16 13:54:06'),(350,26,'26','LASASHI','拉萨市',1,'系统管理员','2025-02-16 13:54:47','系统管理员','2025-02-16 13:54:47'),(351,26,'26','RIKAZESHI','日喀则市',2,'系统管理员','2025-02-16 13:54:54','系统管理员','2025-02-16 13:54:54'),(352,26,'26','SHANNANSHI','山南市',3,'系统管理员','2025-02-16 13:54:59','系统管理员','2025-02-16 13:54:59'),(353,26,'26','LINZHISHI','林芝市',4,'系统管理员','2025-02-16 13:55:05','系统管理员','2025-02-16 13:55:05'),(354,26,'26','CHANGDOUSHI','昌都市',5,'系统管理员','2025-02-16 13:55:11','系统管理员','2025-02-16 13:55:11'),(355,26,'26','NEIQUSHI','那曲市',6,'系统管理员','2025-02-16 13:55:17','系统管理员','2025-02-16 13:55:17'),(356,27,'27','XIANSHI','西安市',1,'系统管理员','2025-02-16 13:56:02','系统管理员','2025-02-16 13:56:02'),(357,27,'27','BAOJISHI','宝鸡市',2,'系统管理员','2025-02-16 13:56:09','系统管理员','2025-02-16 13:56:09'),(358,27,'27','XIANYANGSHI','咸阳市',3,'系统管理员','2025-02-16 13:56:16','系统管理员','2025-02-16 13:56:16'),(359,27,'27','TONGCHUANSHI','铜川市',4,'系统管理员','2025-02-16 13:56:24','系统管理员','2025-02-16 13:56:24'),(360,27,'27','WEINANSHI','渭南市',5,'系统管理员','2025-02-16 13:56:30','系统管理员','2025-02-16 13:56:30'),(361,27,'27','YANANSHI','延安市',6,'系统管理员','2025-02-16 13:56:38','系统管理员','2025-02-16 13:56:38'),(362,27,'27','YULINSHI','榆林市',7,'系统管理员','2025-02-16 13:56:45','系统管理员','2025-02-16 13:56:45'),(363,27,'27','HANZHONGSHI','汉中市',8,'系统管理员','2025-02-16 13:56:51','系统管理员','2025-02-16 13:56:51'),(364,27,'27','ANKANGSHI','安康市',9,'系统管理员','2025-02-16 13:56:57','系统管理员','2025-02-16 13:56:57'),(365,27,'27','SHANGLUOSHI','商洛市',10,'系统管理员','2025-02-16 13:57:03','系统管理员','2025-02-16 13:57:03'),(366,28,'28','LANZHOUSHI','兰州市',1,'系统管理员','2025-02-16 13:57:39','系统管理员','2025-02-16 13:57:39'),(367,28,'28','JIAYUGUANSHI','嘉峪关市',2,'系统管理员','2025-02-16 13:57:46','系统管理员','2025-02-16 13:57:46'),(368,28,'28','JINCHANGSHI','金昌市',3,'系统管理员','2025-02-16 13:57:53','系统管理员','2025-02-16 13:57:53'),(369,28,'28','BAIYINSHI','白银市',4,'系统管理员','2025-02-16 13:57:59','系统管理员','2025-02-16 13:57:59'),(370,28,'28','TIANSHUISHI','天水市',5,'系统管理员','2025-02-16 13:58:05','系统管理员','2025-02-16 13:58:05'),(371,28,'28','WUWEISHI','武威市',6,'系统管理员','2025-02-16 13:58:12','系统管理员','2025-02-16 13:58:12'),(372,28,'28','ZHANGYESHI','张掖市',7,'系统管理员','2025-02-16 13:58:18','系统管理员','2025-02-16 13:58:18'),(373,28,'28','PINGLIANGSHI','平凉市',8,'系统管理员','2025-02-16 13:58:24','系统管理员','2025-02-16 13:58:24'),(374,28,'28','JIUQUANSHI','酒泉市',9,'系统管理员','2025-02-16 13:58:31','系统管理员','2025-02-16 13:58:31'),(375,28,'28','QINGYANGSHI','庆阳市',10,'系统管理员','2025-02-16 13:58:37','系统管理员','2025-02-16 13:58:37'),(376,28,'28','DINGXISHI','定西市',11,'系统管理员','2025-02-16 13:58:43','系统管理员','2025-02-16 13:58:43'),(377,28,'28','LONGNANSHI','陇南市',12,'系统管理员','2025-02-16 13:58:52','系统管理员','2025-02-16 13:58:52'),(378,28,'28','LINXIAHUIZUZIZHIZHOU','临夏回族自治州',13,'系统管理员','2025-02-16 13:59:00','系统管理员','2025-02-16 13:59:00'),(379,28,'28','GANNANZANGZUZIZHIZHOU‌','甘南藏族自治州‌',14,'系统管理员','2025-02-16 13:59:08','系统管理员','2025-02-16 13:59:08'),(380,29,'29','XININGSHI','西宁市',1,'系统管理员','2025-02-16 13:59:50','系统管理员','2025-02-16 13:59:50'),(381,29,'29','HAIDONGSHI','海东市',2,'系统管理员','2025-02-16 13:59:57','系统管理员','2025-02-16 13:59:57'),(382,29,'29','HAIBEIZANGZUZIZHIZHOU','海北藏族自治州',3,'系统管理员','2025-02-16 14:00:12','系统管理员','2025-02-16 14:00:12'),(383,29,'29','HUANGNANZANGZUZIZHIZHOU','黄南藏族自治州',4,'系统管理员','2025-02-16 14:00:19','系统管理员','2025-02-16 14:00:19'),(384,29,'29','HAINANZANGZUZIZHIZHOU','海南藏族自治州',5,'系统管理员','2025-02-16 14:00:25','系统管理员','2025-02-16 14:00:25'),(385,29,'29','GUOLUOZANGZUZIZHIZHOU','果洛藏族自治州',6,'系统管理员','2025-02-16 14:00:34','系统管理员','2025-02-16 14:00:34'),(386,29,'29','YUSHUZANGZUZIZHIZHOU','玉树藏族自治州',7,'系统管理员','2025-02-16 14:00:40','系统管理员','2025-02-16 14:00:40'),(387,29,'29','HAIXIMENGGUZUZANGZUZIZHIZHOU','海西蒙古族藏族自治州',8,'系统管理员','2025-02-16 14:00:49','系统管理员','2025-02-16 14:00:49'),(388,30,'30','YINCHUANSHI','银川市',1,'系统管理员','2025-02-16 14:01:23','系统管理员','2025-02-16 14:01:23'),(389,30,'30','SHIZUISHANSHI','石嘴山市',2,'系统管理员','2025-02-16 14:01:29','系统管理员','2025-02-16 14:01:29'),(390,30,'30','WUZHONGSHI','吴忠市',3,'系统管理员','2025-02-16 14:01:36','系统管理员','2025-02-16 14:01:36'),(391,30,'30','GUYUANSHI','固原市',4,'系统管理员','2025-02-16 14:01:44','系统管理员','2025-02-16 14:01:44'),(392,30,'30','ZHONGWEISHI','中卫市',5,'系统管理员','2025-02-16 14:01:51','系统管理员','2025-02-16 14:01:51'),(393,31,'31','WULUMUQISHI‌','乌鲁木齐市‌',1,'系统管理员','2025-02-16 14:02:48','系统管理员','2025-02-16 14:02:48'),(394,31,'31','KELAMAYISHI','克拉玛依市',2,'系统管理员','2025-02-16 14:02:54','系统管理员','2025-02-16 14:02:54'),(395,31,'31','TULUFANSHI','吐鲁番市',3,'系统管理员','2025-02-16 14:03:01','系统管理员','2025-02-16 14:03:01'),(396,31,'31','HAMISHI','哈密市',4,'系统管理员','2025-02-16 14:03:08','系统管理员','2025-02-16 14:03:08'),(397,31,'31','AKESUDIQU','阿克苏地区',5,'系统管理员','2025-02-16 14:03:15','系统管理员','2025-02-16 14:03:15'),(398,31,'31','KASHENDIQU','喀什地区',6,'系统管理员','2025-02-16 14:03:23','系统管理员','2025-02-16 14:03:23'),(399,31,'31','HETIANDIQU','和田地区',7,'系统管理员','2025-02-16 14:03:29','系统管理员','2025-02-16 14:03:29'),(400,31,'31','TACHENGDIQU','塔城地区',8,'系统管理员','2025-02-16 14:03:36','系统管理员','2025-02-16 14:03:36'),(401,31,'31','ALETAIDIQU','阿勒泰地区',9,'系统管理员','2025-02-16 14:03:43','系统管理员','2025-02-16 14:03:43'),(402,31,'31','CHANGJIHUIZUZIZHIZHOU','昌吉回族自治州',10,'系统管理员','2025-02-16 14:03:51','系统管理员','2025-02-16 14:03:51'),(403,31,'31','BAYINGUOLENGMENGGUZIZHIZHOU','巴音郭楞蒙古自治州',11,'系统管理员','2025-02-16 14:03:59','系统管理员','2025-02-16 14:03:59'),(404,31,'31','BOERTALAMENGGUZIZHIZHOU','博尔塔拉蒙古自治州',12,'系统管理员','2025-02-16 14:04:06','系统管理员','2025-02-16 14:04:06'),(405,31,'31','KEZILESUKEERKEZIZIZHIZHOU','克孜勒苏柯尔克孜自治州',13,'系统管理员','2025-02-16 14:04:14','系统管理员','2025-02-16 14:04:14'),(406,31,'31','YILIHASAKEZIZHIZHOU','伊犁哈萨克自治州',14,'系统管理员','2025-02-16 14:04:22','系统管理员','2025-02-16 14:04:22'),(407,34,'34','TAIBEISHI','台北市',1,'系统管理员','2025-02-16 14:05:01','系统管理员','2025-02-16 14:05:01'),(408,34,'34','XINBEISHI','新北市',2,'系统管理员','2025-02-16 14:05:08','系统管理员','2025-02-16 14:05:08'),(409,34,'34','TAOYUANSHI','桃园市',3,'系统管理员','2025-02-16 14:05:14','系统管理员','2025-02-16 14:05:14'),(410,34,'34','TAIZHONGSHI','台中市',4,'系统管理员','2025-02-16 14:05:20','系统管理员','2025-02-16 14:05:20'),(411,34,'34','TAINANSHI','台南市',5,'系统管理员','2025-02-16 14:05:26','系统管理员','2025-02-16 14:05:26'),(412,34,'34','GAOXIONGSHI','高雄市',6,'系统管理员','2025-02-16 14:05:32','系统管理员','2025-02-16 14:05:32');
/*!40000 ALTER TABLE `sys_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_resource`
--

DROP TABLE IF EXISTS `sys_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_resource` (
                                `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `module` varchar(32) NOT NULL COMMENT '资源模块',
                                `parent_code` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '父资源编码,提供建立关系',
                                `code` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资源编码,必须唯一',
                                `name` varchar(64) NOT NULL COMMENT '资源名称',
                                `uri` varchar(256) NOT NULL COMMENT '资源地址',
                                `method` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '方法类型:POST,PUT,GET,DELETE',
                                `function_flag` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '功能权限:0-否,1-是',
                                `data_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '数据权限:0-否,1-是',
                                `desensitize_flag` tinyint unsigned DEFAULT '0' COMMENT '数据脱敏:0-否,1-是',
                                `desensitize_json` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '脱敏信息:{field:{start:1,end:100}}',
                                `type_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '资源类型:0-模块,1-类,2-方法',
                                `service_flag` tinyint DEFAULT '0' COMMENT '服务类型:0-对内,1-对外',
                                `update_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '是否更新:0-否,1-是',
                                `created_by` varchar(32) NOT NULL COMMENT '创建人',
                                `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                                `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                PRIMARY KEY (`id`),
                                KEY `idx_module` (`module`),
                                KEY `idx_uri_code` (`code`),
                                KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='资源';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_resource`
--

LOCK TABLES `sys_resource` WRITE;
/*!40000 ALTER TABLE `sys_resource` DISABLE KEYS */;
INSERT INTO `sys_resource` VALUES (14703,'permission',NULL,'permission','权限模块','permission',NULL,1,0,0,NULL,0,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14704,'permission','permission','permissionAccessLogController','访问日志','/api/access-logs',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14705,'permission','permissionAccessLogController','permission_post_api_access_logs','访问日志新增','/api/access-logs','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14706,'permission','permissionAccessLogController','permission_get_api_access_logs','访问日志列表','/api/access-logs','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14707,'permission','permissionAccessLogController','permission_delete_api_access_logs_id','访问日志删除','/api/access-logs/{accessLogId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14708,'permission','permission','permissionAuthenticationController','权限访问','/api/auth',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14709,'permission','permissionAuthenticationController','permission_get_api_auth_validate','验证','/api/auth/validate','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14710,'permission','permissionAuthenticationController','permission_get_api_auth_signOut','登出','/api/auth/signOut','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14711,'permission','permissionAuthenticationController','permission_post_api_auth_signIn','登录','/api/auth/signIn','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14712,'permission','permission','permissionDepartmentController','部门','/api/departments',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14713,'permission','permissionDepartmentController','permission_post_api_departments','部门新增','/api/departments','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14714,'permission','permissionDepartmentController','permission_put_api_departments','部门更新','/api/departments','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14715,'permission','permissionDepartmentController','permission_get_api_departments','部门列表','/api/departments','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14716,'permission','permissionDepartmentController','permission_delete_api_departments_id','部门删除','/api/departments/{departmentId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14717,'permission','permissionDepartmentController','permission_get_api_departments_tree','部门树','/api/departments/tree','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14718,'permission','permissionDepartmentController','permission_post_api_departments_list','部门渲染','/api/departments/list','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14719,'permission','permission','permissionDictController','数字字典','/api/dicts',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14720,'permission','permissionDictController','permission_get_api_dicts','数字字典列表','/api/dicts','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14721,'permission','permissionDictController','permission_delete_api_dicts_id','数字字典删除','/api/dicts/{dictId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14722,'permission','permissionDictController','permission_put_api_dicts','数字字典更新','/api/dicts','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14723,'permission','permissionDictController','permission_post_api_dicts','数字字典新增','/api/dicts','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14724,'permission','permission','permissionDictDetailController','数字字典明细','/api/dict-details',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14725,'permission','permissionDictDetailController','permission_post_api_dict_details','数字字典明细新增','/api/dict-details','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14726,'permission','permissionDictDetailController','permission_post_api_dict_details_list','根据编码数字字典明细','/api/dict-details/list','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14727,'permission','permissionDictDetailController','permission_get_api_dict_details_id','根据ID查询数字字典明细','/api/dict-details/{dictId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14728,'permission','permission','permissionHeaderController','列表头','/api/headers',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14729,'permission','permissionHeaderController','permission_get_api_headers','根据编码获取列表头','/api/headers','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14730,'permission','permissionHeaderController','permission_put_api_headers','列表头保存','/api/headers','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14731,'permission','permission','permissionLoginLogController','登录日志','/api/login-logs',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14732,'permission','permissionLoginLogController','permission_get_api_login_logs','登录日志列表','/api/login-logs','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14733,'permission','permission','permissionMenuController','菜单','/api/menus',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14734,'permission','permissionMenuController','permission_delete_api_menus_id','菜单删除','/api/menus/{menuId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14735,'permission','permissionMenuController','permission_get_api_menus','菜单树','/api/menus','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14736,'permission','permissionMenuController','permission_put_api_menus','菜单更新','/api/menus','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14737,'permission','permissionMenuController','permission_get_api_menus_me','菜单当前用户获取','/api/menus/me','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14738,'permission','permissionMenuController','permission_post_api_menus','菜单新增','/api/menus','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14739,'permission','permission','permissionMenuResourceController','菜单和资源关系','/api/menu-resources',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14740,'permission','permissionMenuResourceController','permission_get_api_menu_resources_id','菜单和资源关系列表','/api/menu-resources/{menuId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14741,'permission','permissionMenuResourceController','permission_post_api_menu_resources','菜单和资源关系新增','/api/menu-resources','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14742,'permission','permission','permissionMsgTemplateController','消息模版','/api/msg-templates',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14743,'permission','permissionMsgTemplateController','permission_post_api_msg_templates','消息模版新增','/api/msg-templates','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14744,'permission','permissionMsgTemplateController','permission_put_api_msg_templates','消息模版更新','/api/msg-templates','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14745,'permission','permissionMsgTemplateController','permission_get_api_msg_templates_code','消息模版列表','/api/msg-templates/code','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14746,'permission','permissionMsgTemplateController','permission_get_api_msg_templates','消息模版列表','/api/msg-templates','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14747,'permission','permissionMsgTemplateController','permission_delete_api_msg_templates_id','消息模版删除','/api/msg-templates/{msgTemplateId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14748,'permission','permission','permissionPositionController','岗位','/api/positions',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14749,'permission','permissionPositionController','permission_post_api_positions','岗位新增','/api/positions','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14750,'permission','permissionPositionController','permission_put_api_positions','岗位更新','/api/positions','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14751,'permission','permissionPositionController','permission_get_api_positions','岗位列表','/api/positions','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14752,'permission','permissionPositionController','permission_delete_api_positions_id','岗位删除','/api/positions/{positionId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14753,'permission','permission','permissionProjectController','项目配置','/api/projects',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14754,'permission','permissionProjectController','permission_post_api_projects','项目配置新增','/api/projects','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14755,'permission','permissionProjectController','permission_put_api_projects','项目配置更新','/api/projects','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14756,'permission','permissionProjectController','permission_get_api_projects','项目配置列表','/api/projects','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14757,'permission','permissionProjectController','permission_delete_api_projects_id','项目配置删除','/api/projects/{projectId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14758,'permission','permission','permissionRegionController','区域','/api/regions',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14759,'permission','permissionRegionController','permission_put_api_regions','区域更新','/api/regions','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14760,'permission','permissionRegionController','permission_delete_api_regions_id','区域删除','/api/regions/{regionId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14761,'permission','permissionRegionController','permission_get_api_regions','区域树','/api/regions','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14762,'permission','permissionRegionController','permission_post_api_regions_list','区域渲染','/api/regions/list','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14763,'permission','permissionRegionController','permission_post_api_regions','区域新增','/api/regions','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14764,'permission','permission','permissionResourceController','资源','/api/resources',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14765,'permission','permissionResourceController','permission_get_api_resources_menu_id','根据菜单ID获取资源','/api/resources/menu/{menuId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14766,'permission','permissionResourceController','permission_get_api_resources_user_id','用户资源获取','/api/resources/user/{userId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14767,'permission','permissionResourceController','permission_get_api_resources_service_id','根据服务类型获取','/api/resources/service/{type}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14768,'permission','permissionResourceController','permission_put_api_resources','资源更新','/api/resources','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14769,'permission','permissionResourceController','permission_post_api_resources_upload_remote','资源远程上报','/api/resources/upload/remote','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14770,'permission','permissionResourceController','permission_post_api_resources_upload','资源上报','/api/resources/upload','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14771,'permission','permissionResourceController','permission_get_api_resources','资源列表','/api/resources','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14772,'permission','permission','permissionRoleController','角色','/api/roles',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14773,'permission','permissionRoleController','permission_get_api_roles','角色列表','/api/roles','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14774,'permission','permissionRoleController','permission_delete_api_roles_id','角色删除','/api/roles/{roleId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14775,'permission','permissionRoleController','permission_put_api_roles','角色更新','/api/roles','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14776,'permission','permissionRoleController','permission_post_api_roles','角色新增','/api/roles','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14777,'permission','permission','permissionRoleMenuController','角色和菜单关系','/api/role-menus',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14778,'permission','permissionRoleMenuController','permission_get_api_role_menus_id','角色和菜单关系获取','/api/role-menus/{roleId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14779,'permission','permissionRoleMenuController','permission_post_api_role_menus','角色和菜单关系新增','/api/role-menus','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14780,'permission','permission','permissionServiceLogController','服务访问','/api/service-logs',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14781,'permission','permissionServiceLogController','permission_post_api_service_logs','服务访问新增','/api/service-logs','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14782,'permission','permissionServiceLogController','permission_get_api_service_logs','服务访问列表','/api/service-logs','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14783,'permission','permission','permissionUserController','系统管理员','/api/users',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14784,'permission','permissionUserController','permission_get_api_users_select','管理员选择','/api/users/select','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14785,'permission','permissionUserController','permission_put_api_users_disable','管理员禁用','/api/users/disable','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14786,'permission','permissionUserController','permission_get_api_users','管理员列表','/api/users','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14787,'permission','permissionUserController','permission_delete_api_users_id','管理员删除','/api/users/{userId}','delete',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14788,'permission','permissionUserController','permission_get_api_users_id','管理员获取','/api/users/{userId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14789,'permission','permissionUserController','permission_post_api_users_list','管理员渲染','/api/users/list','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14790,'permission','permissionUserController','permission_get_api_users_me','管理员登录','/api/users/me','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14791,'permission','permissionUserController','permission_put_api_users','管理员更新','/api/users','put',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14792,'permission','permissionUserController','permission_post_api_users','管理员新增','/api/users','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14793,'permission','permissionUserController','permission_put_api_users_enable','管理员启用','/api/users/enable','put',1,0,0,NULL,2,0,1,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-18 20:21:43'),(14794,'permission','permission','permissionUserRoleController','管理员和角色关系','/api/user-roles',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14795,'permission','permissionUserRoleController','permission_get_api_user_roles_id','管理员和角色关系获取','/api/user-roles/{userId}','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14796,'permission','permissionUserRoleController','permission_post_api_user_roles','管理员和角色关系新增','/api/user-roles','post',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14797,'permission','permission','permissionError404Controller','基础错误提示','/error',NULL,1,0,0,NULL,1,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07'),(14798,'permission','permissionError404Controller','permission_get_error','404错误','/error','get',1,0,0,NULL,2,0,0,'系统管理员','2025-02-16 11:49:07','系统管理员','2025-02-16 11:49:07');
/*!40000 ALTER TABLE `sys_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
                            `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `name` varchar(32) NOT NULL COMMENT '名字',
                            `module_flag` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '模块类型:0-权限模块,数字字典',
                            `remark` varchar(64) DEFAULT NULL COMMENT '备注',
                            `status_flag` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '状态:字典dict.permission.statusFlag',
                            `created_by` varchar(32) NOT NULL COMMENT '创建人',
                            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                            `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                            PRIMARY KEY (`id`),
                            KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (38,'超级管理员',0,'超级管理员',1,'admin','2021-05-02 19:38:20','系统管理员','2023-10-10 21:24:11'),(39,'系统管理',1,'提供系统管理，包括用户授权，角色授权',1,'admin','2021-10-04 15:04:19','系统管理员','2023-09-02 16:17:19'),(40,'授权管理',0,'提供系统授权管理，包括角色授权，用户授权',1,'admin','2021-10-04 15:06:58','系统管理员','2025-02-16 11:10:04');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_menu` (
                                 `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `role_id` int unsigned NOT NULL COMMENT '角色ID',
                                 `menu_id` int unsigned NOT NULL COMMENT '菜单ID',
                                 `type_flag` tinyint unsigned NOT NULL COMMENT '类型:0-半选,1-全选',
                                 PRIMARY KEY (`id`),
                                 KEY `udx_role_id_menu_id_type_flag` (`role_id`,`menu_id`,`type_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和菜单关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (3947,38,1,1),(3948,38,2,1),(3949,38,3,1),(3990,38,4,1),(4001,38,8,1),(3959,38,9,1),(3969,38,10,1),(3989,38,11,1),(3950,38,115,1),(3951,38,116,1),(3953,38,117,1),(3955,38,118,1),(3958,38,119,1),(3960,38,120,1),(3961,38,121,1),(3962,38,122,1),(3963,38,123,1),(4012,38,124,1),(4013,38,125,1),(4020,38,126,1),(3977,38,127,1),(3984,38,128,1),(4011,38,129,1),(3970,38,130,1),(3971,38,131,1),(3972,38,132,1),(3952,38,133,1),(3974,38,134,1),(3975,38,135,1),(3976,38,136,1),(3978,38,137,1),(3954,38,138,1),(3980,38,139,1),(3981,38,140,1),(3983,38,141,1),(3985,38,142,1),(3987,38,143,1),(3988,38,144,1),(3991,38,145,1),(3992,38,146,1),(3993,38,147,1),(3994,38,148,1),(3973,38,149,1),(3995,38,150,1),(3996,38,151,1),(3997,38,152,1),(4000,38,153,1),(4002,38,154,1),(4003,38,155,1),(4004,38,156,1),(4005,38,157,1),(4006,38,158,1),(4007,38,159,1),(4010,38,160,1),(3986,38,161,1),(4014,38,162,1),(4015,38,163,1),(4017,38,164,1),(4018,38,165,1),(4019,38,166,1),(4016,38,167,1),(3964,38,168,1),(3965,38,169,1),(3966,38,170,1),(3967,38,171,1),(3968,38,172,1),(4021,38,173,1),(4022,38,174,1),(3979,38,175,1),(3982,38,176,1),(3956,38,177,1),(3957,38,178,1),(3998,38,179,1),(3999,38,180,1),(4008,38,181,1),(4009,38,182,1);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_service_log`
--

DROP TABLE IF EXISTS `sys_service_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_service_log` (
                                   `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                                   `user_id` int NOT NULL COMMENT '管理员ID',
                                   `username` varchar(32) NOT NULL COMMENT '用户名',
                                   `full_name` varchar(16) NOT NULL COMMENT '姓名',
                                   `target_server` varchar(64) DEFAULT NULL COMMENT '访问实例',
                                   `schema` varchar(64) DEFAULT NULL COMMENT '访问协议',
                                   `request_path` varchar(128) NOT NULL COMMENT '访问地址',
                                   `request_method` varchar(32) NOT NULL COMMENT '访问方法',
                                   `browser` varchar(64) DEFAULT NULL COMMENT '访问者浏览器',
                                   `client_os` varchar(32) NOT NULL COMMENT '访问者操作系统',
                                   `request_ip` mediumtext NOT NULL COMMENT '登录地址',
                                   `request_address` varchar(32) NOT NULL COMMENT '访问来源地点',
                                   `request_body` longtext NOT NULL COMMENT '访问入参',
                                   `request_time` datetime DEFAULT NULL COMMENT '请求开始时间',
                                   `response_time` datetime DEFAULT NULL COMMENT '请求结束时间',
                                   `response_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '访问返回值',
                                   `times` bigint NOT NULL DEFAULT '0' COMMENT '访问耗时',
                                   `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='服务访问';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_service_log`
--

LOCK TABLES `sys_service_log` WRITE;
/*!40000 ALTER TABLE `sys_service_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_service_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
                            `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `username` varchar(32) NOT NULL COMMENT '用户名',
                            `password` varchar(64) NOT NULL COMMENT '密码',
                            `full_name` varchar(16) NOT NULL COMMENT '姓名',
                            `email` varchar(64) NOT NULL COMMENT '邮箱',
                            `department_id` int unsigned NOT NULL COMMENT '部门ID,数据权限',
                            `department_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '部门路径',
                            `position_id` int DEFAULT NULL COMMENT '岗位ID',
                            `status_flag` tinyint unsigned NOT NULL COMMENT '状态:0-禁用,1-启用',
                            `status_remark` varchar(128) DEFAULT NULL COMMENT '状态描述',
                            `term_validity` datetime DEFAULT NULL COMMENT '有效期,格式:yyyy-MM-dd HH:mm:ss',
                            `type_flag` tinyint unsigned NOT NULL COMMENT '管理员类型:0-普通管理员,1-系统创建',
                            `created_by` varchar(32) NOT NULL COMMENT '创建人',
                            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `updated_by` varchar(32) NOT NULL COMMENT '修改人',
                            `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                            PRIMARY KEY (`id`),
                            KEY `idx_username` (`username`),
                            KEY `idx_full_name` (`full_name`),
                            KEY `idx_department_id` (`department_id`),
                            KEY `idx_postion_id` (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'admin','123456','系统管理员','laoshuisheng@163.com',64,'61,62,64',4,1,'启动管理员','2099-07-31 00:00:00',1,'admin','2020-03-20 23:13:41','系统管理员','2025-02-18 20:23:19');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
                                 `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `user_id` int NOT NULL COMMENT '管理员ID',
                                 `role_id` int NOT NULL COMMENT '角色ID',
                                 PRIMARY KEY (`id`),
                                 KEY `udx_user_id_role_id` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理员和角色关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (117,1,38);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-18 20:35:41
