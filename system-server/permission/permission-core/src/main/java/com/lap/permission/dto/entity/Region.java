/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <pre>
 *  <b>表名</b>：sys_region
 *  区域，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "区域实体")
public class Region extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: pid
     * 描述: 父id;是否必填:否。
     * </pre>
     */
    @Schema(description = "父id")
    private Integer pid;
    /**
     * <pre>
     * 数据库字段: pid_path
     * 描述: 父路径;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "父路径")
    private String pidPath;
    /**
     * <pre>
     * 数据库字段: code
     * 描述: 区域编码;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "区域编码")
    private String code;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 区域名称;字段长度:256,是否必填:是。
     * </pre>
     */
    @Schema(description = "区域名称")
    private String name;
    /**
     * <pre>
     * 数据库字段: orders
     * 描述: 排序;是否必填:是。
     * </pre>
     */
    @Schema(description = "排序")
    private Integer orders;

}
