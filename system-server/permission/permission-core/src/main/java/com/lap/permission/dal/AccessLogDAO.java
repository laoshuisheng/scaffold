/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.AccessLog;
import com.lap.permission.dto.query.AccessLogQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_access_log
 *  访问日志持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface AccessLogDAO {
    /**
     * 分页查询 访问日志
     *
     * @param query 查询条件
     * @return 访问日志集合
     */
    List<AccessLog> queryPage(AccessLogQuery query);

    /**
     * 根据查询条件统计访问日志数量
     *
     * @param query 查询条件
     * @return 访问日志数量
     */
    long countPage(AccessLogQuery query);

    /**
     * 保存信息
     *
     * @param accessLog 信息
     */
    void insertAccessLog(AccessLog accessLog);

    /**
     * 根据ID删除信息
     *
     * @param accessLogId 访问日志Id
     * @return 删除数量
     */
    int deleteById(Integer accessLogId);

}
