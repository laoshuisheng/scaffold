/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.pojo.UserState;
import com.lap.permission.dto.entity.User;
import com.lap.permission.dto.query.UserQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_user
 *  系统管理员持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface UserDAO {
    /**
     * 分页查询 系统管理员
     *
     * @param query 查询条件
     * @return 系统管理员集合
     */
    List<User> queryPage(UserQuery query);

    /**
     * 根据查询条件统计系统管理员数量
     *
     * @param query 查询条件
     * @return 系统管理员数量
     */
    long countPage(UserQuery query);

    /**
     * 分页查询简易管理员
     *
     * @param query 查询条件,继承分页信息
     * @return 系统管理员集合
     */
    List<User> querySelect(UserQuery query);

    /**
     * 统计简易管理员
     *
     * @param query 查询条件,继承分页信息
     * @return 系统管理员集合
     */
    long countSelect(UserQuery query);

    /**
     * 根据ID查询系统管理员
     *
     * @param userId 系统管理员Id
     * @return 单条记录数据
     */
    User queryById(Integer userId);

    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户吗
     * @return 用户信息
     */
    User queryByUsername(String username);

    /**
     * 根据用户id集合查询用户
     *
     * @param ids 用户id集合
     * @return 用户集合
     */
    List<User> queryByIds(List<Integer> ids);

    /**
     * 保存信息
     *
     * @param user 信息
     */
    void insertUser(User user);

    /**
     * 根据ID更新信息
     *
     * @param user 信息
     * @return 修改数量
     */
    int updateUser(User user);

    /**
     * 根据id更新系统管理状态
     *
     * @param userState 状态对象
     * @return 更新数量
     */
    int updateState(UserState userState);

    /**
     * 根据ID删除信息
     *
     * @param userId 系统管理员Id
     * @return 删除数量
     */
    int deleteById(Integer userId);
}
