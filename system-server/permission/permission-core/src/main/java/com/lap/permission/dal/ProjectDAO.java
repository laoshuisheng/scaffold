/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Project;
import com.lap.permission.dto.query.ProjectQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_project
 *  项目配置持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface ProjectDAO {
    /**
     * 分页查询 项目配置
     *
     * @param query 查询条件
     * @return 项目配置集合
     */
    List<Project> queryPage(ProjectQuery query);

    /**
     * 根据查询条件统计项目配置数量
     *
     * @param query 查询条件
     * @return 项目配置数量
     */
    long countPage(ProjectQuery query);

    /**
     * 根据ID查询项目配置
     *
     * @param projectId 项目配置Id
     * @return 单条记录数据
     */
    Project queryById(Integer projectId);

    /**
     * 根据编码查询项目配置
     *
     * @param code 项目编码
     * @return 项目配置
     */
    Project queryByCode(String code);

    /**
     * 保存信息
     *
     * @param project 项目信息
     */
    void insertProject(Project project);

    /**
     * 根据ID更新信息
     *
     * @param project 项目信息
     * @return 修改数量
     */
    int updateProject(Project project);

    /**
     * 根据ID删除信息
     *
     * @param projectId 项目配置Id
     * @return 删除数量
     */
    int deleteById(Integer projectId);

}
