/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.framework.mybatis.annotation.MyBatch;
import com.lap.permission.dto.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_user_role
 *  管理员和角色关系持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface UserRoleDAO {
    /**
     * 根据ID查询管理员和角色关系
     *
     * @param userId 管理员和角色关系Id
     * @return 单条记录数据
     */
    List<Integer> queryByUserId(Integer userId);

    /**
     * 保存信息
     *
     * @param list 信息
     */
    @MyBatch
    void insertUserRoleBatch(List<UserRole> list);

    /**
     * 根据ID删除信息
     *
     * @param userId 管理员Id
     * @return 删除数量
     */
    int deleteByUserId(Integer userId);

}
