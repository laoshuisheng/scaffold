/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <pre>
 *  <b>表名</b>：sys_menu
 *  菜单，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "菜单实体")
public class Menu extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: pid
     * 描述: 父ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "父ID")
    private Integer pid;
    /**
     * <pre>
     * 数据库字段: pid_paths
     * 描述: 父ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "父ID路径")
    private String pidPath;
    /**
     * <pre>
     * 数据库字段: type_flag
     * 描述: 菜单类型:0-目录,1-菜单,2-功能;是否必填:是。
     * </pre>
     */
    @Schema(description = "菜单类型")
    private Integer typeFlag;
    /**
     * <pre>
     * 数据库字段: path
     * 描述: 菜单路径;字段长度:128,是否必填:是。
     * </pre>
     */
    @Schema(description = "菜单路径")
    private String path;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 菜单名称;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "菜单名称")
    private String name;
    /**
     * <pre>
     * 数据库字段: component
     * 描述: 菜单组件路径;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "菜单组件路径")
    private String component;
    /**
     * <pre>
     * 数据库字段: title
     * 描述: 国际化编码;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "国际化编码")
    private String title;
    /**
     * <pre>
     * 数据库字段: redirect
     * 描述: 重新定向路径;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "重新定向路径")
    private String redirect;
    /**
     * <pre>
     * 数据库字段: icon
     * 描述: 图标;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "图标")
    private String icon;
    /**
     * <pre>
     * 数据库字段: hide_flag
     * 描述: 是否隐藏;是否必填:是。
     * </pre>
     */
    @Schema(description = "是否隐藏")
    private Integer hideFlag;
    /**
     * <pre>
     * 数据库字段: keep_alive_flag
     * 描述: 是否缓存;是否必填:是。
     * </pre>
     */
    @Schema(description = "是否缓存")
    private Integer keepAliveFlag;
    /**
     * <pre>
     * 数据库字段: affix_flag
     * 描述: 是否固定tag;是否必填:是。
     * </pre>
     */
    @Schema(description = "是否固定tag")
    private Integer affixFlag;
    /**
     * <pre>
     * 数据库字段: iframe_flag
     * 描述: 是否外链;是否必填:是。
     * </pre>
     */
    @Schema(description = "是否外链")
    private Integer iframeFlag;
    /**
     * <pre>
     * 数据库字段: link
     * 描述: 外链;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "外链")
    private String link;
    /**
     * <pre>
     * 数据库字段: perm_code
     * 描述: 权限码;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "权限码")
    private String permCode;
    /**
     * <pre>
     * 数据库字段: orders
     * 描述: 排序;是否必填:是。
     * </pre>
     */
    @Schema(description = "排序")
    private Integer orders;

}
