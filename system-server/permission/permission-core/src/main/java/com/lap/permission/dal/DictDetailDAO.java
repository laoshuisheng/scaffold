/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.framework.mybatis.annotation.MyBatch;
import com.lap.permission.dto.entity.DictDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_dict_detail
 *  数字字典明细持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface DictDetailDAO {

    /**
     * 根据字典编码和字典明细值查询字典明细
     *
     * @param dictCode    字典编码
     * @param detailValue 字典明细值
     * @param lang        语言
     * @return 数字字典明细
     */
    DictDetail queryByDictCode(@Param("dictCode") String dictCode,
                               @Param("detailValue") Integer detailValue,
                               @Param("lang") String lang);

    /**
     * 根据数字字典编码集合查询字典明细
     *
     * @param dictCodes 数字字典编码集合
     * @param lang      语言
     * @return 数字字典明细
     */
    List<DictDetail> queryByDictCodes(@Param("dictCodes") List<String> dictCodes, @Param("lang") String lang);

    /**
     * 根据字典ID查询明细集合
     *
     * @param dictId 字典id
     * @return 字典明细集合
     */
    List<DictDetail> queryByDictId(Integer dictId);

    /**
     * 批量字典明细
     *
     * @param dataList 字典明细
     */
    @MyBatch
    void insertDictDetailBatch(List<DictDetail> dataList);

    /**
     * 根据ID删除信息
     *
     * @param dictId 数字字典Id
     * @return 删除数量
     */
    int deleteByDictId(Integer dictId);

}
