/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <pre>
 *  <b>表名</b>：sys_dict_detail
 *  数字字典明细，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "数字字典明细实体")
public class DictDetail extends BaseEntity implements Serializable {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: dict_id
     * 描述: 字典ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "字典ID")
    private Integer dictId;
    /**
     * <pre>
     * 数据库字段: dict_code
     * 描述: 字典编码;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "字典编码")
    private String dictCode;
    /**
     * <pre>
     * 数据库字段: detail_value
     * 描述: 明细值;是否必填:是。
     * </pre>
     */
    @Schema(description = "明细值")
    private Integer detailValue;
    /**
     * <pre>
     * 数据库字段: detail_title
     * 描述: 明细标题;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "明细标题")
    private String detailTitle;
    /**
     * <pre>
     * 数据库字段: status_flag
     * 描述: 状态;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer statusFlag;
    /**
     * <pre>
     * 数据库字段: local
     * 描述: 语言;字段长度:8,是否必填:是。
     * </pre>
     */
    @Schema(description = "语言")
    private String local;
    /**
     * <pre>
     * 数据库字段: orders
     * 描述: 排序;是否必填:是。
     * </pre>
     */
    @Schema(description = "排序")
    private Integer orders;

}
