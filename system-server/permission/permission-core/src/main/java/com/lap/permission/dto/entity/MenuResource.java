/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * <pre>
 *  <b>表名</b>：sys_menu_resource
 *  菜单和资源关系，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Schema(description = "菜单和资源关系实体")
public class MenuResource extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: menu_id
     * 描述: 菜单ID;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "菜单ID")
    private Integer menuId;
    /**
     * <pre>
     * 数据库字段: uri_code
     * 描述: 资源编码;字段长度:128,是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "资源编码")
    private String uriCode;
    /**
     * <pre>
     * 数据库字段: type_flag
     * 描述: 类型:0-半选,1-全选;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "类型")
    private Integer typeFlag;

}
