package com.lap.permission.dto.pojo;

import com.lap.framework.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户状态数据库操作，这个包里面定义了操作数据库，跟 entity 区分是非数据库所有字段
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserState extends BaseEntity {
    /**
     * 系统用户
     */
    private Integer id;
    /**
     * 状态标识
     */
    private Integer statusFlag;
    /**
     * 状态备注
     */
    private String statusRemark;

}
