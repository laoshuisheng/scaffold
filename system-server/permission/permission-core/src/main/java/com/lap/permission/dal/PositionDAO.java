/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Position;
import com.lap.permission.dto.query.PositionQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_position
 *  岗位持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface PositionDAO {
    /**
     * 分页查询 岗位
     *
     * @param query 查询条件
     * @return 岗位集合
     */
    List<Position> queryPage(PositionQuery query);

    /**
     * 根据查询条件统计岗位数量
     *
     * @param query 查询条件
     * @return 岗位数量
     */
    long countPage(PositionQuery query);

    /**
     * 根据ID查询岗位
     *
     * @param positionId 岗位Id
     * @return 单条记录数据
     */
    Position queryById(Integer positionId);

    /**
     * 根据名称查询岗位
     *
     * @param name 名称
     * @return 岗位
     */
    Position queryByName(String name);

    /**
     * 保存信息
     *
     * @param position 信息
     */
    void insertPosition(Position position);

    /**
     * 根据ID更新信息
     *
     * @param position 信息
     * @return 修改数量
     */
    int updatePosition(Position position);

    /**
     * 根据ID删除信息
     *
     * @param positionId 岗位Id
     * @return 删除数量
     */
    int deleteById(Integer positionId);
}
