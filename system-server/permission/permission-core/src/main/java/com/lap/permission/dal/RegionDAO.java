/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Region;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_region
 *  区域持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface RegionDAO {
    /**
     * 分页查询 区域
     *
     * @return 区域集合
     */
    List<Region> queryList();

    /**
     * 根据ID查询区域
     *
     * @param regionId 区域Id
     * @return 单条记录数据
     */
    Region queryById(Integer regionId);

    /**
     * 根据编码查询区域
     *
     * @param name 名称
     * @return 区域
     */
    Region queryByName(String name);

    /**
     * 根据id集合查询区域
     *
     * @param list id集合
     * @return 区域集合
     */
    List<Region> queryByIds(List<Integer> list);

    /**
     * 保存信息
     *
     * @param region 信息
     */
    void insertRegion(Region region);

    /**
     * 根据ID更新信息
     *
     * @param region 信息
     * @return 修改数量
     */
    int updateRegion(Region region);

    /**
     * 根据ID删除信息
     *
     * @param regionId 区域Id
     * @return 删除数量
     */
    int deleteById(Integer regionId);
}
