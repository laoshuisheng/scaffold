package com.lap.permission.manager.impl;

import com.lap.permission.dal.MenuDAO;
import com.lap.permission.dal.MenuResourceDAO;
import com.lap.permission.manager.MenuManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class MenuManagerImpl implements MenuManager {

    private final MenuDAO menuDAO;

    private final MenuResourceDAO menuResourceDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteById(Integer menuId) {
        menuResourceDAO.deleteByMenuId(menuId);
        return menuDAO.deleteById(menuId);
    }

}
