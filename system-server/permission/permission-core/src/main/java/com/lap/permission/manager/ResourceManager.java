package com.lap.permission.manager;

import com.lap.framework.entity.Resource;

import java.util.List;

/**
 * 资源原子操作
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface ResourceManager {

    /**
     * 批量保存资源
     *
     * @param module    模块
     * @param resources 资源集合
     * @return 资源id
     */
    Integer insertResourceBatch(String module, List<Resource> resources);

}
