/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Dict;
import com.lap.permission.dto.query.DictQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_dict
 *  数字字典持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface DictDAO {
    /**
     * 分页查询 数字字典
     *
     * @param query 查询条件
     * @return 数字字典集合
     */
    List<Dict> queryPage(DictQuery query);

    /**
     * 根据查询条件统计数字字典数量
     *
     * @param query 查询条件
     * @return 数字字典数量
     */
    long countPage(DictQuery query);

    /**
     * 根据ID查询数字字典
     *
     * @param dictId 数字字典Id
     * @return 单条记录数据
     */
    Dict queryById(Integer dictId);

    /**
     * 根据编码查询数字字典
     *
     * @param code 数字字典编码
     * @return 数字字典
     */
    Dict queryByCode(String code);

    /**
     * 保存信息
     *
     * @param dict 信息
     */
    void insertDict(Dict dict);

    /**
     * 根据ID更新信息
     *
     * @param dict 信息
     * @return 修改数量
     */
    int updateDict(Dict dict);

    /**
     * 根据ID删除信息
     *
     * @param dictId 数字字典Id
     * @return 删除数量
     */
    int deleteById(Integer dictId);

}
