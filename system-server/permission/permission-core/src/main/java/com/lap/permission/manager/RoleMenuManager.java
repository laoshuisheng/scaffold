package com.lap.permission.manager;

import com.lap.permission.dto.entity.RoleMenu;

import java.util.List;

public interface RoleMenuManager {
    /**
     * 保存角色和菜单关系
     *
     * @param roleId   角色id
     * @param dataList 角色和菜单关系
     * @return 角色和菜单关系Id
     */
    Integer insertRoleMenu(Integer roleId, List<RoleMenu> dataList);
}
