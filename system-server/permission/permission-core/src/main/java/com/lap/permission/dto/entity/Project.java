/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  <b>表名</b>：sys_project
 *  项目配置，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "项目配置实体")
public class Project extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 项目名称;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "项目名称")
    private String name;
    /**
     * <pre>
     * 数据库字段: context
     * 描述: 项目上下文;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "项目上下文")
    private String context;
    /**
     * <pre>
     * 数据库字段: type_flag
     * 描述: 服务类型:0-对内,1-对外;是否必填:是。
     * </pre>
     */
    @Schema(description = "服务类型")
    private Integer typeFlag;
    /**
     * <pre>
     * 数据库字段: status_flag
     * 描述: 状态:字典dict.permission.status;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer statusFlag;

}
