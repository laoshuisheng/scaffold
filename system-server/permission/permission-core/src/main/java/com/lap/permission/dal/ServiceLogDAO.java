/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.ServiceLog;
import com.lap.permission.dto.query.ServiceLogQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_service_log
 *  服务访问持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface ServiceLogDAO {
    /**
     * 分页查询 服务访问
     *
     * @param query 查询条件
     * @return 服务访问集合
     */
    List<ServiceLog> queryPage(ServiceLogQuery query);

    /**
     * 根据查询条件统计服务访问数量
     *
     * @param query 查询条件
     * @return 服务访问数量
     */
    long countPage(ServiceLogQuery query);

    /**
     * 根据ID查询服务访问
     *
     * @param serviceLogId 服务访问Id
     * @return 单条记录数据
     */
    ServiceLog queryById(Integer serviceLogId);

    /**
     * 保存信息
     *
     * @param serviceLog 信息
     */
    void insertServiceLog(ServiceLog serviceLog);

}
