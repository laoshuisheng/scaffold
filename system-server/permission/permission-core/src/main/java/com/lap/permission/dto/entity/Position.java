/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  <b>表名</b>：sys_position
 *  岗位，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "岗位实体")
public class Position extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 名称;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "名称")
    private String name;
    /**
     * <pre>
     * 数据库字段: status_flag
     * 描述: 状态:0-禁用,1-启用;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer statusFlag;
    /**
     * <pre>
     * 数据库字段: remark
     * 描述: 备注;字段长度:64,是否必填:否。
     * </pre>
     */
    @Schema(description = "备注")
    private String remark;

}
