package com.lap.permission.manager;

import com.lap.permission.dto.entity.UserRole;

import java.util.List;

public interface UserRoleManager {

    /**
     * 保存管理员和角色关系
     *
     * @param userId 管理员id
     * @param list   管理员和角色关系集合
     * @return 管理员和角色关系Id
     */
    Integer insertUserRole(Integer userId, List<UserRole> list);

}
