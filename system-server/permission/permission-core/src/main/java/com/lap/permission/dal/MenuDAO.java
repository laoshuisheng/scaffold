/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_menu
 *  菜单持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface MenuDAO {
    /**
     * 分页查询 菜单
     *
     * @return 菜单集合
     */
    List<Menu> queryList();

    /**
     * 根据ID查询菜单
     *
     * @param menuId 菜单Id
     * @return 单条记录数据
     */
    Menu queryById(Integer menuId);

    /**
     * 获取当前用户菜单
     *
     * @param userId 用户id
     * @return 菜单集合
     */
    List<Menu> queryByUserId(Integer userId);

    /**
     * 根据角色ID集合查询权限码
     *
     * @param roleIds 角色ID集合
     * @return 权限码集合
     */
    List<String> queryPermByRoleIds(List<Integer> roleIds);

    /**
     * 根据父ID查询菜单集合
     *
     * @param parentId 父ID
     * @return 菜单集合
     */
    List<Menu> queryByPid(Integer parentId);

    /**
     * 保存信息
     *
     * @param menu 信息
     */
    void insertMenu(Menu menu);

    /**
     * 根据ID更新信息
     *
     * @param menu 信息
     * @return 修改数量
     */
    int updateMenu(Menu menu);

    /**
     * 根据ID删除信息
     *
     * @param menuId 菜单Id
     * @return 删除数量
     */
    int deleteById(Integer menuId);

}
