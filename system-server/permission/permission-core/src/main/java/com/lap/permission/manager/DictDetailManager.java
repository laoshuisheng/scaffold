package com.lap.permission.manager;

import com.lap.permission.dto.entity.DictDetail;

import java.util.List;

/**
 * 字典明细原子管理
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface DictDetailManager {

    /**
     * 保存数字字典明细
     *
     * @param dictId   字典id
     * @param dataList 数字字典明细
     * @return 数字字典明细Id
     */
    Integer insertDictDetail(Integer dictId, List<DictDetail> dataList);

}
