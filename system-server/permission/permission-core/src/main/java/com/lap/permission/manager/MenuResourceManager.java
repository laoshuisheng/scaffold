package com.lap.permission.manager;

import com.lap.permission.dto.entity.MenuResource;

import java.util.List;

/**
 * 菜单和资源关系原子类
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface MenuResourceManager {

    /**
     * 保存菜单和资源关系
     *
     * @param menuId   菜单id
     * @param dataList 菜单和资源关系集合
     * @return 菜单和资源关系Id
     */
    Integer insertMenuResource(Integer menuId, List<MenuResource> dataList);

}
