package com.lap.permission.manager;

public interface UserManager {
    /**
     * 根据ID删除系统管理员
     *
     * @param userId 系统管理员Id
     * @return 删除数量
     */
    int deleteById(Integer userId);

}
