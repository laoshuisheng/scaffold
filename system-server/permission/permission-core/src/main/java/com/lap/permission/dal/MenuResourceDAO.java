/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.framework.mybatis.annotation.MyBatch;
import com.lap.permission.dto.entity.MenuResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_menu_resource
 *  菜单和资源关系持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface MenuResourceDAO {

    /**
     * 批量保存菜单和资源关系
     *
     * @param list 数据集合
     */
    @MyBatch
    void insertMenuResourceBatch(List<MenuResource> list);

    /**
     * 根据ID查询菜单和资源关系
     *
     * @param menuId 菜单和资源关系Id
     * @return 单条记录数据
     */
    List<String> queryByMenuId(Integer menuId);

    /**
     * 根据ID删除信息
     *
     * @param menuId 菜单和资源关系Id
     * @return 删除数量
     */
    int deleteByMenuId(Integer menuId);

}
