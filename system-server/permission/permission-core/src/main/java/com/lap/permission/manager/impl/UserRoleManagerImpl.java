package com.lap.permission.manager.impl;

import com.lap.permission.dal.UserRoleDAO;
import com.lap.permission.dto.entity.UserRole;
import com.lap.permission.manager.UserRoleManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserRoleManagerImpl implements UserRoleManager {

    private final UserRoleDAO userRoleDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer insertUserRole(Integer userId, List<UserRole> list) {
        int flag = userRoleDAO.deleteByUserId(userId);
        userRoleDAO.insertUserRoleBatch(list);
        return flag;
    }

}
