package com.lap.permission.manager.impl;

import com.lap.permission.dal.RoleDAO;
import com.lap.permission.dal.RoleMenuDAO;
import com.lap.permission.manager.RoleManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class RoleManagerImpl implements RoleManager {

    private final RoleDAO roleDAO;

    private final RoleMenuDAO roleMenuDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteById(Integer roleId) {
        roleMenuDAO.deleteByRoleId(roleId);
        return roleDAO.deleteById(roleId);
    }

}
