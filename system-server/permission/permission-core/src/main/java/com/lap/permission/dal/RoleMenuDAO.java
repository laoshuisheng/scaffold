/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.framework.mybatis.annotation.MyBatch;
import com.lap.permission.dto.entity.RoleMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_role_menu
 *  角色和菜单关系持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface RoleMenuDAO {

    /**
     * 根据ID查询角色和菜单关系
     *
     * @param roleId 角色Id
     * @return 单条记录数据
     */
    List<Integer> queryByRoleId(Integer roleId);

    /**
     * 保存信息
     *
     * @param list 信息
     */
    @MyBatch
    void insertRoleMenuBatch(List<RoleMenu> list);

    /**
     * 根据角色ID删除信息
     *
     * @param roleId 角色Id
     * @return 删除数量
     */
    int deleteByRoleId(Integer roleId);

}
