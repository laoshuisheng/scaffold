/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * <pre>
 *  <b>表名</b>：sys_role
 *  角色，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "角色实体")
public class Role extends BaseEntity implements Serializable {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 名字;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "名字")
    private String name;
    /**
     * <pre>
     * 数据库字段: module_flag
     * 描述: 模块类型:0-权限模块,数字字典;是否必填:是。
     * </pre>
     */
    @Schema(description = "模块类型")
    private Integer moduleFlag;
    /**
     * <pre>
     * 数据库字段: remark
     * 描述: 备注;字段长度:64,是否必填:否。
     * </pre>
     */
    @Schema(description = "备注")
    private String remark;
    /**
     * <pre>
     * 数据库字段: state_flag
     * 描述: 状态:字典dict.permission.statusFlag;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer statusFlag;

}
