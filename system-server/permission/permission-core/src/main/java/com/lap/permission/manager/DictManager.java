package com.lap.permission.manager;

import com.lap.permission.dto.entity.Dict;
import com.lap.permission.dto.entity.DictDetail;

import java.util.List;

/**
 * 字典原子类
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public interface DictManager {

    /**
     * 保存数字字典
     *
     * @param dict     数字字典
     * @param dataList 数字字典明细
     * @return 数字字典Id
     */
    Integer insertDict(Dict dict, List<DictDetail> dataList);

    /**
     * 根据ID删除数字字典
     *
     * @param dictId 数字字典Id
     * @return 删除数量
     */
    int deleteById(Integer dictId);

}
