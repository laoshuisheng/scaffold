/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  <b>表名</b>：sys_access_log
 *  访问日志，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "访问日志实体")
public class AccessLog extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: user_id
     * 描述: 管理员ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "管理员ID")
    private Integer userId;
    /**
     * <pre>
     * 数据库字段: username
     * 描述: 登录账号;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "登录账号")
    private String username;
    /**
     * <pre>
     * 数据库字段: full_name
     * 描述: 姓名;字段长度:16,是否必填:否。
     * </pre>
     */
    @Schema(description = "姓名")
    private String fullName;
    /**
     * <pre>
     * 数据库字段: access_uri
     * 描述: 访问地址;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问地址")
    private String accessUri;
    /**
     * <pre>
     * 数据库字段: access_method
     * 描述: 访问方法;字段长度:16,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问方法")
    private String accessMethod;
    /**
     * <pre>
     * 数据库字段: device_info
     * 描述: 访问浏览器;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问浏览器")
    private String deviceInfo;
    /**
     * <pre>
     * 数据库字段: sys_info
     * 描述: 访问系统;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问系统")
    private String sysInfo;
    /**
     * <pre>
     * 数据库字段: access_sign
     * 描述: 访问来源地址;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问来源地址")
    private String accessSign;
    /**
     * <pre>
     * 数据库字段: access_address
     * 描述: 访问来源地点;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问来源地点")
    private String accessAddress;
    /**
     * <pre>
     * 数据库字段: access_param
     * 描述: 访问入参;字段长度:2048,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问入参")
    private String accessParam;
    /**
     * <pre>
     * 数据库字段: access_result
     * 描述: 访问返回值;字段长度:65535,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问返回值")
    private String accessResult;
    /**
     * <pre>
     * 数据库字段: times
     * 描述: 访问耗时;是否必填:否。
     * </pre>
     */
    @Schema(description = "访问耗时")
    private Long times;

}
