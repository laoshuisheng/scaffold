package com.lap.permission.convert;

import com.lap.framework.entity.Resource;
import com.lap.framework.tools.BooleanStrategy;
import com.lap.framework.tools.IntStrategy;
import com.lap.permission.dto.entity.*;
import com.lap.permission.dto.pojo.UserState;
import com.lap.permission.dto.request.*;
import com.lap.permission.dto.response.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * 对象类型转换
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper(imports = {BooleanStrategy.class, IntStrategy.class}, componentModel = "spring")
public interface Convert {

    AccessLog toAccessLog(AccessLogRequest request);

    @Mapping(expression = "java(IntStrategy.toString(request.getPidPaths()))", target = "pidPath")
    @Mapping(expression = "java(IntStrategy.getLast(request.getPidPaths()))", target = "pid")
    Department toDepartment(DepartmentRequest request);

    @Mapping(expression = "java(IntStrategy.toList(depart.getPidPath()))", target = "pidPaths")
    DepartmentDto toDepartDto(Department depart);

    Dict toDict(DictRequest request);

    DictDetail toDictDetail(DictDetailRequest.DetailRequest detail);

    @Mapping(source = "meta.title", target = "title")
    @Mapping(source = "meta.icon", target = "icon")
    @Mapping(expression = "java(IntStrategy.toString(request.getPidPaths()))", target = "pidPath")
    @Mapping(expression = "java(BooleanStrategy.toInteger(request.getMeta().getIsKeepAlive()))", target = "keepAliveFlag")
    @Mapping(expression = "java(BooleanStrategy.toInteger(request.getMeta().getIsAffix()))", target = "affixFlag")
    @Mapping(expression = "java(BooleanStrategy.toInteger(request.getMeta().getIsIframe()))", target = "iframeFlag")
    @Mapping(expression = "java(BooleanStrategy.toInteger(request.getMeta().getIsHide()))", target = "hideFlag")
    Menu toMenu(MenuRequest request);

    @Mapping(target = "children", ignore = true)
    @Mapping(source = "title", target = "meta.title")
    @Mapping(source = "icon", target = "meta.icon")
    @Mapping(expression = "java(IntStrategy.toList(menu.getPidPath()))", target = "pidPaths")
    @Mapping(expression = "java(BooleanStrategy.notBlankToBoolean(menu.getLink()))", target = "meta.isLink")
    @Mapping(expression = "java(BooleanStrategy.toBoolean(menu.getKeepAliveFlag()))", target = "meta.isKeepAlive")
    @Mapping(expression = "java(BooleanStrategy.toBoolean(menu.getAffixFlag()))", target = "meta.isAffix")
    @Mapping(expression = "java(BooleanStrategy.toBoolean(menu.getIframeFlag()))", target = "meta.isIframe")
    @Mapping(expression = "java(BooleanStrategy.toBoolean(menu.getHideFlag()))", target = "meta.isHide")
    MenuDto toMenuDto(Menu menu);

    MsgTemplate toMsgTemplate(MsgTemplateRequest request);

    Position toPosition(PositionRequest request);

    Project toProject(ProjectRequest request);

    @Mapping(expression = "java(IntStrategy.toString(request.getPidPaths()))", target = "pidPath")
    @Mapping(expression = "java(IntStrategy.getLast(request.getPidPaths()))", target = "pid")
    Region toRegion(RegionRequest request);

    @Mapping(target = "children", ignore = true)
    @Mapping(expression = "java(IntStrategy.toList(region.getPidPath()))", target = "pidPaths")
    RegionDto toRegionDto(Region region);

    Resource toResource(ResourceRequest request);

    Role toRole(RoleRequest request);

    List<RoleDto> toRoleDto(List<Role> roleList);

    ServiceLog toServiceLog(ServiceLogRequest request);

    @Mapping(expression = "java(IntStrategy.toString(request.getDepartmentPaths()))", target = "departmentPath")
    @Mapping(expression = "java(IntStrategy.getLast(request.getDepartmentPaths()))", target = "departmentId")
    User toUser(UserRequest request);

    @Mapping(expression = "java(IntStrategy.toList(user.getDepartmentPath()))", target = "departmentPaths")
    UserDto toUserDto(User user);

    UserState toUserState(UserStateRequest request);

}
