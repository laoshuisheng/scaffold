/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.framework.entity.Resource;
import com.lap.framework.mybatis.annotation.MyBatch;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_resource
 *  资源持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface ResourceDAO {
    /**
     * 分页查询 资源
     *
     * @return 资源集合
     */
    List<Resource> queryList();

    /**
     * 根据ID查询资源
     *
     * @param resourceId 资源Id
     * @return 单条记录数据
     */
    Resource queryById(Integer resourceId);

    /**
     * 根据模块查询资源集合
     *
     * @param module 模块
     * @return 资源集合
     */
    List<Resource> queryByModule(String module);

    /**
     * 根据服务类型查询资源
     *
     * @param type 服务类型
     * @return 资源集合
     */
    List<Resource> queryByService(Integer type);

    /**
     * 根据用户id 查询资源
     *
     * @param roleIds 用户id
     * @return 资源集合
     */
    List<Resource> queryByRoleIds(List<Integer> roleIds);

    /**
     * 根据菜单ID查询资源集合
     *
     * @param menuId 菜单ID
     * @return 资源集合
     */
    List<Resource> queryByMenuId(Integer menuId);

    /**
     * 批量保存资源
     *
     * @param resources 资源集合
     */
    @MyBatch
    void insertResourceBatch(List<Resource> resources);

    /**
     * 根据ID更新信息
     *
     * @param resource 信息
     * @return 修改数量
     */
    int updateResource(Resource resource);

    /**
     * 根据模块删除资源
     *
     * @param module 资源模块
     * @return 修改数量
     */
    int deleteByModule(String module);
}
