/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.LoginLog;
import com.lap.permission.dto.query.LoginLogQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_login_log
 *  登录日志持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface LoginLogDAO {
    /**
     * 分页查询 登录日志
     *
     * @param query 查询条件
     * @return 登录日志集合
     */
    List<LoginLog> queryPage(LoginLogQuery query);

    /**
     * 根据查询条件统计登录日志数量
     *
     * @param query 查询条件
     * @return 登录日志数量
     */
    long countPage(LoginLogQuery query);

    /**
     * 保存信息
     *
     * @param loginLog 信息
     */
    void insertLoginLog(LoginLog loginLog);

}
