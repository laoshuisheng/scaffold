/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.MsgTemplate;
import com.lap.permission.dto.query.MsgTemplateQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_msg_template
 *  消息模版持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface MsgTemplateDAO {
    /**
     * 分页查询 消息模版
     *
     * @param query 查询条件
     * @return 消息模版集合
     */
    List<MsgTemplate> queryPage(MsgTemplateQuery query);

    /**
     * 根据查询条件统计消息模版数量
     *
     * @param query 查询条件
     * @return 消息模版数量
     */
    long countPage(MsgTemplateQuery query);

    /**
     * 根据ID查询消息模版
     *
     * @param msgTemplateId 消息模版Id
     * @return 单条记录数据
     */
    MsgTemplate queryById(Integer msgTemplateId);

    /**
     * 根据编码查询消息模版
     *
     * @param code 编码
     * @return 消息模版
     */
    MsgTemplate queryByCode(String code);

    /**
     * 保存信息
     *
     * @param msgTemplate 信息
     */
    void insertMsgTemplate(MsgTemplate msgTemplate);

    /**
     * 根据ID更新信息
     *
     * @param msgTemplate 信息
     * @return 修改数量
     */
    int updateMsgTemplate(MsgTemplate msgTemplate);
    /**
     *  根据ID删除信息
     *
     * @param msgTemplateId 消息模版Id
     * @return 删除数量
     */
    int deleteById(Integer msgTemplateId);

}
