package com.lap.permission.manager.impl;

import com.lap.permission.dal.MenuResourceDAO;
import com.lap.permission.dto.entity.MenuResource;
import com.lap.permission.manager.MenuResourceManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MenuResourceManagerImpl implements MenuResourceManager {

    private final MenuResourceDAO menuResourceDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer insertMenuResource(Integer menuId, List<MenuResource> dataList) {
        int flag = menuResourceDAO.deleteByMenuId(menuId);
        menuResourceDAO.insertMenuResourceBatch(dataList);
        return flag;
    }

}
