/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  <b>表名</b>：sys_msg_template
 *  消息模版，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "消息模版实体")
public class MsgTemplate extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: module_flag
     * 描述: 模板类型,使用数字字典;是否必填:是。
     * </pre>
     */
    @Schema(description = "所属模块")
    private Integer moduleFlag;
    /**
     * <pre>
     * 数据库字段: code
     * 描述: 模版编码,不能修改;字段长度:128,是否必填:是。
     * </pre>
     */
    @Schema(description = "模版编码")
    private String code;
    /**
     * <pre>
     * 数据库字段: name
     * 描述: 模版名称;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "模版名称")
    private String name;
    /**
     * <pre>
     * 数据库字段: title
     * 描述: 模版主题;字段长度:256,是否必填:是。
     * </pre>
     */
    @Schema(description = "模版主题")
    private String title;
    /**
     * <pre>
     * 数据库字段: content
     * 描述: 模版内容;字段长度:4294967295,是否必填:是。
     * </pre>
     */
    @Schema(description = "模版内容")
    private String content;

}
