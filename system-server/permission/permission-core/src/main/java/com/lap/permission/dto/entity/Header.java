/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <pre>
 *  <b>表名</b>：sys_header
 *  列表头，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "列表头实体")
public class Header extends BaseEntity implements Serializable {
    /**
     * <pre>
     * 数据库字段: user_id
     * 描述: 管理员ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "管理员ID")
    private Integer userId;
    /**
     * <pre>
     * 数据库字段: code
     * 描述: 头编码;字段长度:128,是否必填:是。
     * </pre>
     */
    @Schema(description = "头编码")
    private String code;
    /**
     * <pre>
     * 数据库字段: content
     * 描述: 头内容;字段长度:2047,是否必填:否。
     * </pre>
     */
    @Schema(description = "头内容")
    private String content;

}
