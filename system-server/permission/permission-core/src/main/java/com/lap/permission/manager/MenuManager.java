package com.lap.permission.manager;

public interface MenuManager {

    /**
     * 根据ID删除菜单
     *
     * @param menuId 菜单Id
     * @return 删除数量
     */
    int deleteById(Integer menuId);

}
