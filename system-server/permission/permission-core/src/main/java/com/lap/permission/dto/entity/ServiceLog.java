/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * <pre>
 *  <b>表名</b>：sys_service_log
 *  服务访问，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@Schema(description = "服务访问实体")
public class ServiceLog extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: user_id
     * 描述: 管理员ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "管理员ID")
    private Integer userId;
    /**
     * <pre>
     * 数据库字段: username
     * 描述: 用户名;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "用户名")
    private String username;
    /**
     * <pre>
     * 数据库字段: full_name
     * 描述: 姓名;字段长度:16,是否必填:是。
     * </pre>
     */
    @Schema(description = "姓名")
    private String fullName;
    /**
     * <pre>
     * 数据库字段: target_server
     * 描述: 访问实例;字段长度:64,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问实例")
    private String targetServer;
    /**
     * <pre>
     * 数据库字段: schema
     * 描述: 访问协议;字段长度:64,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问协议")
    private String schema;
    /**
     * <pre>
     * 数据库字段: request_path
     * 描述: 访问地址;字段长度:128,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问地址")
    private String requestPath;
    /**
     * <pre>
     * 数据库字段: request_method
     * 描述: 访问方法;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问方法")
    private String requestMethod;
    /**
     * <pre>
     * 数据库字段: browser
     * 描述: 访问者浏览器;字段长度:64,是否必填:否。
     * </pre>
     */
    @Schema(description = "访问者浏览器")
    private String browser;
    /**
     * <pre>
     * 数据库字段: client_os
     * 描述: 访问者操作系统;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问者操作系统")
    private String clientOs;
    /**
     * <pre>
     * 数据库字段: request_ip
     * 描述: 登录地址;字段长度:16777215,是否必填:是。
     * </pre>
     */
    @Schema(description = "登录地址")
    private String requestIp;
    /**
     * <pre>
     * 数据库字段: request_address
     * 描述: 访问来源地点;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问来源地点")
    private String requestAddress;
    /**
     * <pre>
     * 数据库字段: request_body
     * 描述: 访问入参;字段长度:4294967295,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问入参")
    private String requestBody;
    /**
     * <pre>
     * 数据库字段: request_time
     * 描述: 请求开始时间;是否必填:否。
     * </pre>
     */
    @Schema(description = "请求开始时间")
    private Date requestTime;
    /**
     * <pre>
     * 数据库字段: response_time
     * 描述: 请求结束时间;是否必填:否。
     * </pre>
     */
    @Schema(description = "请求结束时间")
    private Date responseTime;
    /**
     * <pre>
     * 数据库字段: response_data
     * 描述: 访问返回值;字段长度:4294967295,是否必填:是。
     * </pre>
     */
    @Schema(description = "访问返回值")
    private String responseData;
    /**
     * <pre>
     * 数据库字段: times
     * 描述: 访问耗时;是否必填:是。
     * </pre>
     */
    @Schema(description = "访问耗时")
    private Long times;

}
