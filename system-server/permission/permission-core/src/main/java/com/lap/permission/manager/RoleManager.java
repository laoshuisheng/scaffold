package com.lap.permission.manager;

public interface RoleManager {

    /**
     * 根据ID删除角色
     *
     * @param roleId 角色Id
     * @return 删除数量
     */
    int deleteById(Integer roleId);

}
