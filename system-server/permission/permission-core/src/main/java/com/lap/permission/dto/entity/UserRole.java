/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;


/**
 * <pre>
 *  <b>表名</b>：sys_user_role
 *  管理员和角色关系，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Schema(description = "管理员和角色关系实体")
public class UserRole extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: user_id
     * 描述: 管理员ID;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "管理员ID")
    private Integer userId;
    /**
     * <pre>
     * 数据库字段: role_id
     * 描述: 角色ID;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "角色ID")
    private Integer roleId;

}
