/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 *  <b>表名</b>：sys_login_log
 *  登录日志，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "登录日志实体")
public class LoginLog extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: user_id
     * 描述: 管理员ID;是否必填:是。
     * </pre>
     */
    @Schema(description = "管理员ID")
    private Integer userId;
    /**
     * <pre>
     * 数据库字段: username
     * 描述: 登录账号;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "登录账号")
    private String username;
    /**
     * <pre>
     * 数据库字段: full_name
     * 描述: 姓名;字段长度:16,是否必填:是。
     * </pre>
     */
    @Schema(description = "姓名")
    private String fullName;
    /**
     * <pre>
     * 数据库字段: browser
     * 描述: 浏览器;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "浏览器")
    private String browser;
    /**
     * <pre>
     * 数据库字段: sys_info
     * 描述: 系统信息;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "系统信息")
    private String sysInfo;
    /**
     * <pre>
     * 数据库字段: login_ip
     * 描述: 登录地址;是否必填:是。
     * </pre>
     */
    @Schema(description = "登录地址")
    private String loginIp;
    /**
     * <pre>
     * 数据库字段: login_address
     * 描述: 登录地点;字段长度:256,是否必填:否。
     * </pre>
     */
    @Schema(description = "登录地点")
    private String loginAddress;
    /**
     * <pre>
     * 数据库字段: success_flag
     * 描述: 状态:0-失败,1-成功;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer successFlag;
    /**
     * <pre>
     * 数据库字段: remark
     * 描述: 备注;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "备注")
    private String remark;
    /**
     * <pre>
     * 数据库字段: times
     * 描述: 耗时,单位毫秒;是否必填:是。
     * </pre>
     */
    @Schema(description = "耗时,单位毫秒")
    private Long times;

}
