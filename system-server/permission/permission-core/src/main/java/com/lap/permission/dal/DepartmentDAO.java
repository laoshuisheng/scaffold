/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Department;
import com.lap.permission.dto.query.DepartmentQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_department
 *  部门持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface DepartmentDAO {
    /**
     * 分页查询 部门
     *
     * @param query 查询条件
     * @return 部门集合
     */
    List<Department> queryPage(DepartmentQuery query);

    /**
     * 根据查询条件统计部门数量
     *
     * @param query 查询条件
     * @return 部门数量
     */
    long countPage(DepartmentQuery query);

    /**
     * 默认查询10000 信息
     *
     * @return 部门数据
     */
    List<Department> queryList();

    /**
     * 根据ID查询部门
     *
     * @param departmentId 部门Id
     * @return 单条记录数据
     */
    Department queryById(Integer departmentId);

    /**
     * 根据编码查询部门
     *
     * @param code 部门编码
     * @return 单条记录数据
     */
    Department queryByCode(String code);

    /**
     * 根据部门父ID查询子部门
     *
     * @param pid 父ID
     * @return 部门记录集合
     */
    List<Department> queryByPid(Integer pid);

    /**
     * 根据id集合查询部门
     *
     * @param list id集合
     * @return 部门记录集合
     */
    List<Department> queryByIds(List<Integer> list);

    /**
     * 保存信息
     *
     * @param department 信息
     */
    void insertDepartment(Department department);

    /**
     * 根据ID更新信息
     *
     * @param department 信息
     * @return 修改数量
     */
    int updateDepartment(Department department);

    /**
     * 根据ID删除信息
     *
     * @param departmentId 部门Id
     * @return 删除数量
     */
    int deleteById(Integer departmentId);

}
