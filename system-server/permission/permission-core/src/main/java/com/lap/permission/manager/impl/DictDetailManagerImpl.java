package com.lap.permission.manager.impl;

import com.lap.permission.dal.DictDetailDAO;
import com.lap.permission.dto.entity.DictDetail;
import com.lap.permission.manager.DictDetailManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DictDetailManagerImpl implements DictDetailManager {

    private final DictDetailDAO dictDetailDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer insertDictDetail(Integer dictId, List<DictDetail> dataList) {
        int flag = dictDetailDAO.deleteByDictId(dictId);
        dictDetailDAO.insertDictDetailBatch(dataList);
        return flag;
    }

}
