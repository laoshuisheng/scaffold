package com.lap.permission.manager.impl;

import com.lap.permission.dal.UserDAO;
import com.lap.permission.dal.UserRoleDAO;
import com.lap.permission.manager.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class UserManagerImpl implements UserManager {

    private final UserDAO userDAO;

    private final UserRoleDAO userRoleDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteById(Integer userId) {
        userRoleDAO.deleteByUserId(userId);
        return userDAO.deleteById(userId);
    }

}
