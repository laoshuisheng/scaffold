package com.lap.permission.manager.impl;

import com.lap.permission.dal.RoleMenuDAO;
import com.lap.permission.dto.entity.RoleMenu;
import com.lap.permission.manager.RoleMenuManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class RoleMenuManagerImpl implements RoleMenuManager {

    private final RoleMenuDAO roleMenuDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer insertRoleMenu(Integer roleId, List<RoleMenu> dataList) {
        int flag = roleMenuDAO.deleteByRoleId(roleId);
        roleMenuDAO.insertRoleMenuBatch(dataList);
        return flag;
    }

}
