/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *  <b>表名</b>：sys_user
 *  系统管理员，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "系统管理员实体")
public class User extends BaseEntity implements Serializable {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: username
     * 描述: 用户名;字段长度:32,是否必填:是。
     * </pre>
     */
    @Schema(description = "用户名")
    private String username;
    /**
     * <pre>
     * 数据库字段: password
     * 描述: 密码;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "密码")
    private String password;
    /**
     * <pre>
     * 数据库字段: full_name
     * 描述: 姓名;字段长度:16,是否必填:是。
     * </pre>
     */
    @Schema(description = "姓名")
    private String fullName;
    /**
     * <pre>
     * 数据库字段: email
     * 描述: 邮箱;字段长度:64,是否必填:是。
     * </pre>
     */
    @Schema(description = "邮箱")
    private String email;
    /**
     * <pre>
     * 数据库字段: department_id
     * 描述: 部门ID,数据权限;是否必填:是。
     * </pre>
     */
    @Schema(description = "部门ID")
    private Integer departmentId;
    /**
     * <pre>
     * 数据库字段: department_id_path
     * 描述: 部门ID,数据权限;是否必填:是。
     * </pre>
     */
    @Schema(description = "部门ID路径")
    private String departmentPath;
    /**
     * <pre>
     * 数据库字段: position_id
     * 描述: 岗位ID;是否必填:否。
     * </pre>
     */
    @Schema(description = "岗位ID")
    private Integer positionId;
    /**
     * <pre>
     * 数据库字段: status_flag
     * 描述: 状态:0-禁用,1-启用;是否必填:是。
     * </pre>
     */
    @Schema(description = "状态")
    private Integer statusFlag;
    /**
     * <pre>
     * 数据库字段: status_remark
     * 描述: 状态描述;字段长度:128,是否必填:否。
     * </pre>
     */
    @Schema(description = "状态描述")
    private String statusRemark;
    /**
     * <pre>
     * 数据库字段: term_validity
     * 描述: 有效期,格式:yyyy-MM-dd;是否必填:否。
     * </pre>
     */
    @Schema(description = "有效期")
    private Date termValidity;
    /**
     * <pre>
     * 数据库字段: type_flag
     * 描述: 管理员类型:0-普通管理员,1-系统创建;是否必填:是。
     * </pre>
     */
    @Schema(description = "管理员类型")
    private Integer typeFlag;

    @Schema(description = "职位")
    private Position position;

}
