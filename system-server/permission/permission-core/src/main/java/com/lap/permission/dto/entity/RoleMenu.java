/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dto.entity;

import com.lap.framework.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * <pre>
 *  <b>表名</b>：sys_role_menu
 *  角色和菜单关系，其中一些通用字段 BaseEntity。
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Schema(description = "角色和菜单关系实体")
public class RoleMenu extends BaseEntity {
    /**
     * <pre>
     * 数据库字段: id
     * 描述: 主键;是否必填:是。
     * </pre>
     */
    @Schema(description = "主键")
    private Integer id;
    /**
     * <pre>
     * 数据库字段: role_id
     * 描述: 角色ID;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "角色ID")
    private Integer roleId;
    /**
     * <pre>
     * 数据库字段: menu_id
     * 描述: 菜单ID;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "菜单ID")
    private Integer menuId;
    /**
     * <pre>
     * 数据库字段: type_flag
     * 描述: 类型:0-半选,1-全选;是否必填:是。
     * </pre>
     */
    @NonNull
    @Schema(description = "类型")
    private Integer typeFlag;

}
