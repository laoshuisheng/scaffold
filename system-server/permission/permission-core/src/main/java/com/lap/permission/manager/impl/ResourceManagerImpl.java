package com.lap.permission.manager.impl;

import com.lap.framework.entity.Resource;
import com.lap.permission.dal.ResourceDAO;
import com.lap.permission.manager.ResourceManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ResourceManagerImpl implements ResourceManager {

    private final ResourceDAO resourceDAO;

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public Integer insertResourceBatch(String module, List<Resource> resources) {
        resourceDAO.deleteByModule(module);
        resourceDAO.insertResourceBatch(resources);
        return resources.size();
    }

}
