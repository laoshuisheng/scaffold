/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Role;
import com.lap.permission.dto.query.RoleQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 *  <b>表名</b>：sys_role
 *  角色持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface RoleDAO {
    /**
     * 分页查询 角色
     *
     * @param query 查询条件
     * @return 角色集合
     */
    List<Role> queryPage(RoleQuery query);

    /**
     * 根据查询条件统计角色数量
     *
     * @param query 查询条件
     * @return 角色数量
     */
    long countPage(RoleQuery query);

    /**
     * 根据ID查询角色
     *
     * @param roleId 角色Id
     * @return 单条记录数据
     */
    Role queryById(Integer roleId);

    /**
     * 根据名称查询
     *
     * @param name 名称
     * @return 角色
     */
    Role queryByName(String name);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return
     */
    List<Role> queryByUserId(Integer userId);

    /**
     * 保存信息
     *
     * @param role 信息
     */
    void insertRole(Role role);

    /**
     * 根据ID更新信息
     *
     * @param role 信息
     * @return 修改数量
     */
    int updateRole(Role role);

    /**
     * 根据ID删除信息
     *
     * @param roleId 角色Id
     * @return 删除数量
     */
    int deleteById(Integer roleId);
}
