/*
 * Copyright 2024 The JA-SIG Collaborative. All rights reserved.
 * distributed with thi file and available online at
 */
package com.lap.permission.dal;

import com.lap.permission.dto.entity.Header;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <pre>
 *  <b>表名</b>：sys_header
 *  列表头持久操作对象
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Mapper
public interface HeaderDAO {
    /**
     * 保存信息
     *
     * @param header 信息
     */
    void insertHeader(Header header);

    /**
     * 根据ID查询列表头
     *
     * @param userId 用户Id
     * @param code   编码
     * @return 单条记录数据
     */
    Header queryByUserIdAndCode(@Param("userId") Integer userId, @Param("code") String code);

    /**
     * 根据ID更新信息
     *
     * @param header 信息
     * @return 修改数量
     */
    int updateHeader(Header header);

}
