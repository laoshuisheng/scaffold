#定义服务名称
```yaml
feign:
  permission: permission

```

#优化feign线程池
```yaml
feign:
  httpclient:
    enabled: true # 支持HttpClient的开关
    max-connections: 200 # 最大连接数
    max-connections-per-route: 50 # 单个路径的最大连接数

```