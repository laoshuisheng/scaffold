package com.lap.permission.client;

import com.lap.framework.dto.response.Result;
import com.lap.framework.entity.Resource;
import com.lap.permission.client.fallback.ResourceFallback;
import com.lap.permission.dto.request.ResourceRemoteRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(
        name = "${feign.permission:permission}",
        path = "api/resources",
        fallbackFactory = ResourceFallback.class
)
public interface ResourceClient {
    /**
     * 根据用户id获取资源
     *
     * @param userId 用户id
     * @return 用户资源
     */
    @GetMapping("user/{userId}")
    Result<List<Resource>> queryByUserId(@PathVariable Integer userId);

    /**
     * 上报资源
     *
     * @param request 上报资源请求
     * @return 上报数量
     */
    @PostMapping("upload/remote")
    Result<Integer> uploadResource(@RequestBody ResourceRemoteRequest request);

}
