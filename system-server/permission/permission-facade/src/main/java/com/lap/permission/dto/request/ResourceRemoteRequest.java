package com.lap.permission.dto.request;

import com.lap.framework.entity.Resource;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
@Schema(description = "资源远程上报请求")
public class ResourceRemoteRequest extends ResourceUploadRequest {

    private List<Resource> resources;

}
