package com.lap.permission.client.fallback;

import com.lap.framework.dto.response.Result;
import com.lap.framework.enums.ResultCode;
import com.lap.permission.client.AuthenticationClient;
import com.lap.permission.constant.Constant;
import com.lap.permission.dto.response.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuthenticationFallback implements FallbackFactory<AuthenticationClient> {
    @Override
    public AuthenticationClient create(Throwable cause) {
        /*
        不记录栈，防止日志过多
         */
        log.error("fallback error:{}", cause.getLocalizedMessage());

        return new AuthenticationClient() {

            @Override
            public Result<UserDto> validate(String token) {
                return Result.failed(ResultCode.SYS_FALLBACK, Constant.FALLBACK);
            }

        };

    }
}
