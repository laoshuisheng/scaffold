package com.lap.permission.client;

import com.lap.framework.dto.response.Result;
import com.lap.permission.client.fallback.AuthenticationFallback;
import com.lap.permission.dto.response.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 这里不再提供熔断，使用统一异常处理方式处理，熔断会把非熔断的异常也吞并，提示不友好。
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@FeignClient(
        name = "${feign.permission:permission}",
        path = "api/auth",
        fallbackFactory = AuthenticationFallback.class
)
public interface AuthenticationClient {
    /**
     * 验证token信息
     *
     * @param token token
     * @return 获取用户信息
     */
    @GetMapping("validate")
    Result<UserDto> validate(@RequestParam("token") String token);

}
