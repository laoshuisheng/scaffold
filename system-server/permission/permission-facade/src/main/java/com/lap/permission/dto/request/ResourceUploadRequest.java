package com.lap.permission.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "资源上报请求")
public class ResourceUploadRequest {

    @Schema(description = "资源模块")
    private String module;

    @Schema(description = "资源名称")
    private String name;

    @Schema(description = "服务类型")
    private Integer serviceFlag;

}
