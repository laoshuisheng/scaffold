package com.lap.permission.client;

import com.lap.framework.dto.response.Result;
import com.lap.permission.client.fallback.MsgTemplateClientFallback;
import com.lap.permission.dto.response.MsgTemplateDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "${feign.permission:permission}",
        path = "api/msg-templates",
        fallbackFactory = MsgTemplateClientFallback.class
)
public interface MsgTemplateClient {
    /**
     * 根据消息编码获取消息模版信息
     *
     * @param code 消息编码
     * @return 消息模版信息
     */
    @GetMapping("code")
    Result<MsgTemplateDto> queryByCode(@RequestParam("code") String code);

}
