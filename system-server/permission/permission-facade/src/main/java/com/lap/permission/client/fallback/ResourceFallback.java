package com.lap.permission.client.fallback;

import com.lap.framework.dto.response.Result;
import com.lap.framework.entity.Resource;
import com.lap.framework.enums.ResultCode;
import com.lap.permission.client.ResourceClient;
import com.lap.permission.constant.Constant;
import com.lap.permission.dto.request.ResourceRemoteRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ResourceFallback implements FallbackFactory<ResourceClient> {

    @Override
    public ResourceClient create(Throwable cause) {
        log.error("fallback error:{}", cause.getLocalizedMessage());

        return new ResourceClient() {

            @Override
            public Result<List<Resource>> queryByUserId(Integer userId) {
                return Result.failed(ResultCode.SYS_FALLBACK, Constant.FALLBACK);
            }

            @Override
            public Result<Integer> uploadResource(ResourceRemoteRequest request) {
                return Result.failed(ResultCode.SYS_FALLBACK, Constant.FALLBACK);
            }

        };
    }

}
