package com.lap.permission.client.fallback;

import com.lap.framework.dto.response.Result;
import com.lap.framework.enums.ResultCode;
import com.lap.permission.client.MsgTemplateClient;
import com.lap.permission.constant.Constant;
import com.lap.permission.dto.response.MsgTemplateDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MsgTemplateClientFallback implements FallbackFactory<MsgTemplateClient> {

    @Override
    public MsgTemplateClient create(Throwable cause) {
        log.error("fallback error:{}", cause.getLocalizedMessage());

        return new MsgTemplateClient() {

            @Override
            public Result<MsgTemplateDto> queryByCode(String code) {
                return Result.failed(ResultCode.SYS_FALLBACK, Constant.FALLBACK);
            }

        };
    }

}
