package com.lap.permission.constant;

/**
 * 常量信息
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
public final class Constant {

    private Constant() {
    }

    public static final String FALLBACK = "permission fallback";

}
