# 限流描述
> replenishRate: 每次补充令牌数量
> burstCapacity: 令牌桶最大容量，突发请求数量
> requestedTokens: 每次请求消耗令牌的数量

需要注意：请求有很多个的情况下，要记得都加起来，feign也算。

## 使用方案说明

每秒1次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 1
      burstCapacity: 1
      requestedTokens: 1
```

每秒10次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 10
      burstCapacity: 10
      requestedTokens: 1

```

每分钟1次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 1
      burstCapacity: 60
      requestedTokens: 60
```

每分钟10次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 1
      burstCapacity: 60
      requestedTokens: 6
```

每小时1次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 1
      burstCapacity: 3600
      requestedTokens: 3600

```

每小时10次
```yaml
- name: RequestRateLimiter #基于redis漏斗限流
  args:
    key-resolver: "#{@myResolver}"
    redis-rate-limiter:
      replenishRate: 1
      burstCapacity: 3600
      requestedTokens: 360
```