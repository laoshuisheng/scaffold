package com.lap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.lap.permission"})
public class GatewayBootstrap {

    public static void main(String[] args) {
        var app = new SpringApplication(GatewayBootstrap.class);
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }

}