package com.lap.gateway.entity;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * 忽略配置
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Data
@ToString
public class Security {

    private List<Ignore> tokenList;

    private List<Ignore> authorizationList;

    public boolean ignoreToken(String uri, String method) {
        return contain(tokenList, uri, method);
    }

    /**
     * 忽略权限的请求，必然保护忽略token的,不然鉴权的时候,忽略token必然没有权限
     *
     * @param uri    请求uri
     * @param method 请求方法
     * @return 是否忽略，true-忽略,false-不忽略
     */
    public boolean ignoreAuthor(String uri, String method) {
        List<Ignore> all = new ArrayList<>();
        if (tokenList != null)
            all.addAll(tokenList);

        if (authorizationList != null)
            all.addAll(authorizationList);

        return contain(all, uri, method);
    }

    private boolean contain(List<Ignore> dataList, String uri, String method) {
        if (dataList == null)
            return false;

        return dataList.stream()
                .anyMatch(u -> u.uri.equalsIgnoreCase(uri) && u.method.equalsIgnoreCase(method));
    }

    public record Ignore(String uri, String method) {

        @Override
        public String toString() {
            return "Ignore{" +
                    "uri='" + uri + '\'' +
                    ", method='" + method + '\'' +
                    '}';
        }

    }

}
