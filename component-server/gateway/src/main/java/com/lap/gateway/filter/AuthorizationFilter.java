package com.lap.gateway.filter;

import com.lap.framework.dto.response.Result;
import com.lap.framework.entity.Resource;
import com.lap.framework.enums.Header;
import com.lap.framework.enums.ResultCode;
import com.lap.gateway.entity.Security;
import com.lap.gateway.tools.Constant;
import com.lap.gateway.tools.ExceptionHolder;
import com.lap.gateway.tools.MonoHolder;
import com.lap.permission.client.ResourceClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <pre>
 * 授权过滤器,提供用户授权校验
 * 必须在鉴权过滤器后面
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Order(-90)
public class AuthorizationFilter implements GlobalFilter {
    public static final Pattern PATTERN = Pattern.compile(Constant.REGEX);

    @jakarta.annotation.Resource
    private Security security;

    /*
    这个lazy可以防止启动循环依赖导致错误
     */
    @Lazy
    @jakarta.annotation.Resource
    private ResourceClient resourceClient;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String uri = request.getURI().getPath();
        String method = request.getMethod().name();
        if (security.ignoreAuthor(uri, method))
            return chain.filter(exchange);

        Result<List<Resource>> resources = getResource(request);
        if (ResultCode.failure(resources.getCode()))
            return MonoHolder.onError(exchange.getResponse(), resources);

        Resource resource = validateUri(resources.getData(), uri, method);
        if (Objects.isNull(resource)) {
            String message = String.format("%s:%s unauthorized", method, uri);
            return MonoHolder.onError(exchange.getResponse(), Result.failed(ResultCode.REQUEST_UNAUTHORIZED, message));
        }

        ServerHttpRequest newRequest = newRequest(exchange.getRequest(), resource);
        return chain.filter(exchange.mutate().request(newRequest).build());
    }

    private Result<List<Resource>> getResource(ServerHttpRequest request) {
        String userParam = request.getHeaders().getFirst(Header.USER_ID.getValue());
        if (StringUtils.isBlank(userParam))
            return Result.ok(Collections.emptyList());

        return CompletableFuture.supplyAsync(() -> {
                    Integer userId = Integer.valueOf(userParam);
                    return resourceClient.queryByUserId(userId);
                }).exceptionally(ExceptionHolder::callException)
                .join();
    }

    private ServerHttpRequest newRequest(ServerHttpRequest request, Resource resource) {
        return request.mutate().headers(header -> {
            header.add(Header.DESENSITIZE_FLAG.getValue(), String.valueOf(resource.getDesensitizeFlag()));
            header.add(Header.DESENSITIZE_JSON.getValue(), resource.getDesensitizeJson());
        }).build();
    }

    /**
     * <pre>
     *  校验请求地址是否存在权限,规则:
     *  1、因为存在占位符,所以需要对占位符的进行替换.
     *  2、判断请求地址和方法是否对应
     * </pre>
     *
     * @param dataList 资源集合
     * @param uri      请求地址
     * @param method   请求方法
     * @return 返回结果, 匹配返回 true,不匹配返回false
     */
    private Resource validateUri(List<Resource> dataList, String uri, String method) {
        if (Objects.isNull(dataList))
            return null;

        return dataList.stream().filter(item -> {
            Matcher matcher = PATTERN.matcher(getUri(item));
            String newUri = matcher.replaceAll(Constant.REPLACE_PATTERN);
            return Pattern.matches(newUri, uri) && method.equalsIgnoreCase(item.getMethod());
        }).findFirst().orElse(null);
    }

    /**
     * 网关跳转需要根据模块+uri,这里需要对跳转地址进行拼接
     *
     * @param resource 资源信息
     * @return uri地址
     */
    private String getUri(Resource resource) {
        StringBuilder sb = new StringBuilder();
        if (Objects.nonNull(resource)) {
            if (StringUtils.isNotEmpty(resource.getModule())) {
                if (!resource.getModule().startsWith(Constant.SLASH)) {
                    sb.append(Constant.SLASH);
                }
                sb.append(resource.getModule());
            }
            if (StringUtils.isNotEmpty(resource.getUri())) {
                if (!resource.getUri().startsWith(Constant.SLASH)) {
                    sb.append(Constant.SLASH);
                }
                sb.append(resource.getUri());
            }
        }
        return sb.toString();
    }

}
