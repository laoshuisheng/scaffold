package com.lap.gateway.tools;

import com.lap.framework.dto.response.Result;
import com.lap.framework.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeoutException;

/**
 * 异常处理，暂时还没有测试有没有用到，现在添加了熔断，可能用不上
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Slf4j
public final class ExceptionHolder {

    private ExceptionHolder() {
    }

    public static <T> Result<T> callException(Throwable e) {
        log.error(e.getLocalizedMessage(), e);
        if (e instanceof TimeoutException)
            return Result.failed(ResultCode.REQUEST_TIMEOUT);

        return Result.failed(ResultCode.SYS_ERROR);
    }

}
