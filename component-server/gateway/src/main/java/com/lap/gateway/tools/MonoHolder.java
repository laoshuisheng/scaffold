package com.lap.gateway.tools;


import com.lap.framework.dto.response.Result;
import com.lap.framework.tools.JsonUtil;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import reactor.core.publisher.Mono;

public final class MonoHolder {

    private MonoHolder() {
    }

    public static Mono<Void> onError(ServerHttpResponse response, Result<?> result) {
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, "application/json;charset=utf-8");
        DataBuffer buffer = response.bufferFactory().wrap(JsonUtil.toBytes(result));
        return response.writeWith(Mono.just(buffer));
    }

}
