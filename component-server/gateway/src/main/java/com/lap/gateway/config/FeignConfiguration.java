package com.lap.gateway.config;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

@Configuration
public class FeignConfiguration {

    /**
     * 解决：
     * feign.codec.DecodeException: No qualifying bean of type
     * 'org.springframework.boot.autoconfigure.http.HttpMessageConverters'
     * available: expected at least 1 bean which qualifies as autowire candidate.
     * Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
     */
    @Bean
    @ConditionalOnMissingBean
    public HttpMessageConverters messageConverters(ObjectProvider<HttpMessageConverter<?>> converters) {
        return new HttpMessageConverters(converters.orderedStream().toList());
    }


}
