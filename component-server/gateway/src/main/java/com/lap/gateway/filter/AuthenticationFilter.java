package com.lap.gateway.filter;

import com.lap.framework.crypto.EncodeUtil;
import com.lap.framework.dto.response.Result;
import com.lap.framework.enums.Header;
import com.lap.framework.enums.ResultCode;
import com.lap.gateway.entity.Security;
import com.lap.gateway.tools.ExceptionHolder;
import com.lap.gateway.tools.MonoHolder;
import com.lap.permission.client.AuthenticationClient;
import com.lap.permission.dto.response.UserDto;
import jakarta.annotation.Resource;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * <pre>
 * 鉴权过滤器,提供用户端/第三方系统TOKEN验证
 * 后面所有的过滤器应该比该过滤器低
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@Order(-100)
public class AuthenticationFilter implements GlobalFilter {
    private static final String AUTHORIZATION = "Authorization";

    public static final String BEARER = "Bearer ";

    /*
    这个lazy可以防止启动循环依赖导致错误
     */
    @Lazy
    @Resource
    private AuthenticationClient authenticationClient;

    @Resource
    private Security security;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        if (security.ignoreToken(request.getURI().getPath(), request.getMethod().name()))
            return chain.filter(exchange);

        Optional<String> optional = Optional.ofNullable(getToken(exchange.getRequest()));
        if (optional.isEmpty()) {
            Result<?> result = Result.failed(ResultCode.REQUEST_UNAUTHORIZED, "Authorization must provide");
            return MonoHolder.onError(exchange.getResponse(), result);
        }

        Result<UserDto> result = getClientUser(optional.get());
        if (ResultCode.failure(result.getCode()) || Objects.isNull(result.getData()))
            return MonoHolder.onError(exchange.getResponse(), result);

        ServerHttpRequest newRequest = newRequest(exchange.getRequest(), result.getData());
        return chain.filter(exchange.mutate().request(newRequest).build());
    }

    private ServerHttpRequest newRequest(ServerHttpRequest request, UserDto userDto) {
        return request.mutate().headers(header -> {
            header.add(Header.USER_ID.getValue(), String.valueOf(userDto.getId()));
            header.add(Header.USER_NAME.getValue(), userDto.getUsername());
            header.add(Header.FULL_NAME.getValue(), EncodeUtil.encode(userDto.getFullName()));
        }).build();
    }

    private Result<UserDto> getClientUser(String token) {
        return CompletableFuture.supplyAsync(() -> authenticationClient.validate(token))
                .exceptionally(ExceptionHolder::callException)
                .join();
    }

    private String getToken(ServerHttpRequest request) {
        String token = Optional.ofNullable(request.getHeaders().getFirst(AUTHORIZATION))
                .orElse(request.getQueryParams().getFirst(AUTHORIZATION));

        return Optional.ofNullable(token)
                .filter(e -> e.contains(BEARER))
                .map(e -> e.substring(BEARER.length()))
                .orElse(token);
    }

}
