package com.lap.gateway.config;

import com.lap.gateway.entity.Security;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class GatewayConfig {

    @Bean
    @ConfigurationProperties(prefix = "security.ignore")
    Security security() {
        return new Security();
    }

}
