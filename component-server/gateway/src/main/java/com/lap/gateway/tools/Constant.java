package com.lap.gateway.tools;

public final class Constant {
    /**
     * 反斜杠
     */
    public static final String SLASH = "/";
    /**
     * PATH匹配表达式
     */
    public static final String REGEX = "\\{\\w+\\}";
    /**
     * 替换成新的表达式
     */
    public static final String REPLACE_PATTERN = "[\\\\w+\\\\.*]+";

}
