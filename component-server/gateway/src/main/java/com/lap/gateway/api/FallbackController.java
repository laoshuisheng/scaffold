package com.lap.gateway.api;

import com.lap.framework.dto.response.Result;
import com.lap.framework.enums.ResultCode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * 网关熔断
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@RestController
@RequestMapping("fallback")
public class FallbackController {

    @GetMapping
    public Mono<Result<String>> fallback() {
        return Mono.just(Result.failed(ResultCode.SYS_FALLBACK, "Request fallback."));
    }

}
