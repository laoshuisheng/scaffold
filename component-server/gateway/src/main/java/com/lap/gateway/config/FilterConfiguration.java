package com.lap.gateway.config;

import com.lap.gateway.filter.AuthenticationFilter;
import com.lap.gateway.filter.AuthorizationFilter;
import com.lap.gateway.filter.LoggingFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class FilterConfiguration {

    @Bean
    AuthenticationFilter authenticationFilter() {
        /*
        初始化鉴权过滤器
         */
        log.info("Initialize authentication");
        return new AuthenticationFilter();
    }

    //@Bean
    AuthorizationFilter authorizationFilter() {
        /*
        初始化授权过滤器
         */
        log.info("Initialize authorization");
        return new AuthorizationFilter();
    }

    //@Bean
    LoggingFilter loggingFilter() {
        /*
        记录日志
         */
        log.info("Initialize loggingFilter");
        return new LoggingFilter();
    }

}
