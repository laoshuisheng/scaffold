# 使用说明

## 1.自我保护
服务注册到eureka中后，默认情况是 30s (默认) 给eureka 发一次心跳，
如果eureka 一段时间 90s(默认)没收到心跳就会把该服务剔除。但是有时候
服务正常，只是由于网络异常抖动没有把心跳发送至eureka ，如果eureka把
服务剔除，当网络恢复正常时，服务也不会重新注册到eureka (服务只有在启动
时候才注册eureka)。服务通过eureka是访问不了。
为了防止这种误杀，eureka提供了自我保护机制: eureka 在15分钟内收到服务端心跳数少于
eureka本应该收到的总心跳数 * 自我保护阀值(默认 0.85)就会触发。该机制默认开启。
等网络恢复后退出自我保护。

### 总体思想就是：宁可保住不健康的，也不盲目注销任务健康的服务

比如: 有10台服务器，正常情况下 15分钟该给eureka发送 10(2*15) = 300 次(15分钟，1次/30秒=2/分钟)
，但此时eureka收到的心跳 300 * 0.85=255，就会触发自我保护。
不发心跳有两种可能：
1.服务没有挂，网络等原因，eureka没收到服务心跳。网络恢复后继续发送心跳。
2.服务挂了，来不及下线。那什么时候从注册列表移除？就是等哪些真的因为网络抖动保护起来的服务重新发送心跳。

什么时候开自我包含？什么时候不开？
1. 思考：如果服务少不开包含。因为服务少，超过 15% 没收到心跳，网络问题可能性更大。但是服务少超过15%没心跳
，服务挂的可能性更大，如果把挂掉服务保护起来，就给客户端返回错误。
2. 为了包含线上系统健壮稳定，可以在任务情况开启自我保护。

自我保护配置
```yaml
eureka:
  server:
    enable-self-preservation: true #自我保护
    renewal-percent-threshold: 0.85 # 阈值，可以适当修改
```

## 2.快速下线

eureka server 在启动时会创建一个定时任务，每隔一段时间（默认60秒），从当前服务清单中把超时服务没续约
（默认90秒）的服务剔除。可以定时任务间隔时间设置短一点，做快速下线，防止客户端拉取不可用服务。
```yaml
eureka:
  server:
    eviction-interval-timer-in-ms: 3000 #单位毫秒，3秒
```

## 3.缓存优化
eureka server 为了避免同时读写内存数据结构造成并发问题，采用了3级缓存机制来进一步提升服务请求的响应速度。
拉取注册表步骤是：
1.首先从 ReadOnlyCacheMap 里查缓存的注册表。
2.若没有，就找 ReadWriteCacheMap 里缓存的注册表。
3.如果还没有，就从内存中获取实际的注册表数据。

当注册表发生变化的时候，先更新注册表数据和ReadWriteCacheMap里缓存的数据，默认30s后把ReadWriteCacheMap里面的数据更新
到ReadOnlyCacheMap.为了提高服务被发现的速度。我们可以做一些设置。

1.拉取服务的时候，不从 ReadOnlyCacheMap 里查，直接从 ReadWriteCacheMap 取
```yaml
eureka:
  server:
    use-read-only-response-cache: false #关闭从 ReadOnlyCacheMap 拉取
```
2.缩短 ReadWriteCacheMap 向 ReadOnlyCacheMap 同步的时间间隔，默认 30s ，可以优化。
```yaml
eureka:
  server:
    response-cache-update-interval-ms: 3000 #单位毫秒
```

## 4. 客户端开发小技巧

我们开发客户端的时候，如果不启动注册中心，会一直报注册中心连接超时错误，我们可以在开发的时候做如下配置，让服务和注册中心脱钩

```yaml
eureka:
  client:
    enabled: false # 不注册，也不拉取，跟注册中心没有关系
```

## 5. client 客户端拉取注册表更实时
api-client 会定时到eureka-server拉取注册表。默认情况是每 30秒拉取一次。

```yaml
eureka:
  client:
    fetch-registry: true
    registry-fetch-interval-seconds: 3
```

## 6.client.serviceUrl.defaultZone 优化
客户端从eureka-server拉取注册表信息是按照 defaultZone 配置顺序依次拉取的，当 第一个不可用的时候才从第二个获取。
所以我们可以考虑不同客户端配置的顺序不一样。

```yaml
eureka:
  client:
    serviceUrl:
      defaultZone: eureka1,eureka2,eureka3
## 不同业务客户端可以调整一下顺序，eureka2,eureka3,eureka1
```

## 7.client 心跳频率
默认情况下，client每隔 30秒就会向服务端发送一次心跳。这个时间可以适当调小一点。
```yaml
eureka:
  instance:
    # 每隔30s，向服务端发送一次心跳，保证自己依然 存活
    lease-renewal-interval-in-seconds: 30
```

## 8.服务端剔除客户端的时间间隔
默认情况下，server在90s之内没有收到client心跳，将我踢出掉。为了让服务快速响应，可以适当的把这个时间改的小一点。
```yaml
eureka:
  instance:
    lease-expiration-duration-in-seconds: 90
```

## 其他问题

### 在哪些地方没有实现一致性？ 也就是 CAP 中的 C。
1.自我保护机制，使网络不好的情况下还会能拉取到注册表进行调用。
2.在缓存同步的时候没实现。上面我们在优化缓存的时候发现。 ReadOnlyCacheMap 和 ReadWriteCacheMap 之间的数据没实现一致性。
3.从其他peer拉取注册表。集群之间的状态是采用异步方式同步，所以不保证节点间的状态一定是一致的，不过基本上能保证最终是一致的。

### 集群同步，集群并没有扩大eureka的承受能力，只是实现了可用性。
在什么情况下会同步数据？
1. 注册：第一个节点注册进来，只同步下一个节点。
2. 续约：有新服务续约，自动同步到其他的eureka-server.
3. 下线：一直同步所有集群。
4. 剔除：不同，每个server都有自己的剔除机制。

### 估算能承受多少服务量
比如有10个业务服务，每个服务部署 5个实例。就是 5*10=50个实例。
一个实例默认 30秒 发一次心跳，30秒拉取一次注册表。那么每分钟请求量： 2*2 *50=200次/s，200*60*24=？ 请求次数

所以合理设置心跳和拉取可以减少eureka-server 请求压力。

### 生产中的问题，当重启服务的时候，还是可以访问，但是返回服务错误
在服务启动时，一定要先停服，再手动触发下线
