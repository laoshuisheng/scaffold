package com.lap.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 注册中心启动入口
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaBootstrap {

    public static void main(String[] args) {
        var app = new SpringApplication(EurekaBootstrap.class);
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }

}