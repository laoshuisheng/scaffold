package com.lap.business.api;

import com.lap.framework.builder.resource.DefaultBuilder;
import com.lap.framework.builder.resource.ResourceDirector;
import com.lap.framework.controller.BaseController;
import com.lap.framework.dto.response.Result;
import com.lap.framework.entity.Resource;
import com.lap.framework.spring.holder.SpringHolder;
import com.lap.framework.watchdog.annotation.Repeat;
import com.lap.permission.client.ResourceClient;
import com.lap.permission.dto.request.ResourceRemoteRequest;
import com.lap.permission.dto.request.ResourceUploadRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "资源")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/resources")
public class ResourceController extends BaseController {

    @jakarta.annotation.Resource
    private ResourceClient resourceClient;

    @Operation(summary = "资源上报")
    @Repeat
    @PostMapping("upload")
    public Result<Integer> uploadResource(@RequestBody ResourceUploadRequest request) {
        List<Object> dataList = SpringHolder.getObject(RestController.class);
        ResourceDirector director = new ResourceDirector(new DefaultBuilder());
        List<Resource> resources = director.construct(request.getModule(), request.getName(), dataList);

        ResourceRemoteRequest remote = new ResourceRemoteRequest();
        remote.setModule(request.getModule());
        remote.setName(request.getName());
        remote.setServiceFlag(request.getServiceFlag());
        remote.setResources(resources);
        return resourceClient.uploadResource(remote);
    }

}
