package com.lap.business.configuration;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * <pre>
 * 写入下面文件:
 * META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
 * com.lap.framework.config.ServiceAutoConfiguration
 *
 * 暂时不扫描包：
 *
 * </pre>
 *
 * @author Shuisheng Lao(劳水生)
 * @version 0.0.1
 */
@AutoConfiguration
@ComponentScan(basePackages = {"com.lap.business.api"})
public class ServiceAutoConfiguration {
}
