# 脚手架

### 介绍
提供快速开发微服务架构，其中包含 系统服务（权限服务，单点认证），组件服务（网关、注册中心），业务服务（您的权限服务）。

### 软件架构

user->gateway  -> permission

### 安装教程


### tomcat 优化
1.线程数
2.超时时间
3.jvm优化

min-spare-threads：最小备用线程数，tomcat启动时的初始化的线程数。默认10
max-threads：Tomcat可创建的最大的线程数，每一个线程处理一个请求，超过这个请求数后，客户端请求只能排队，
    等有线程释放才能处理。（建议这个配置数可以在服务器CUP核心数的200~250倍之间）默认200
accept-count：当调用Web服务的HTTP请求数达到tomcat的最大线程数时，还有新的HTTP请求到来，
    这时tomcat会将该请求放在等待队列中，这个acceptCount就是指能够接受的最大等待数，默认100。
    如果等待队列也被放满了，这个时候再来新的请求就会被tomcat拒绝（connection refused）。
max-connections：这个参数是指在同一时间，tomcat能够接受的最大连接数。一般这个值要大于(max-threads)+(accept-count)。（最大线程数+排队数）
connection-timeout：最长等待时间，如果没有数据进来，等待一段时间后断开连接，释放线程。
